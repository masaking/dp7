require 'autoprefixer-rails'

Encoding.default_external = "utf-8"
http_path 			= "/"
css_dir 			= "../../inc/css"
fonts_dir 			= "../../css/fonts"
sass_dir 			= "scss"
images_dir 			= "../../img"
javascripts_dir 	= "../../inc/js"
line_comments 		= false
relative_assets 	= true
output_style 		= :compressed  #:nested, :expanded, :compact, or :compressed
# sass_options = { :debug_info => true }
cache				= false
asset_cache_buster :none