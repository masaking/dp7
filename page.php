<?php 
// Header
get_header(); 
// GET THE POST TYPE
$postType = get_post_type();
?>
<body <?php body_class(); ?>>
<header id="header_area_paged">
<?php 
include_once(TEMPLATEPATH . "/fixed_menu.php");
dp_banner_contents();
?>
</header>
<section class="dp_topbar_title"><?php dp_breadcrumb(); ?></section><?php
// **********************************
// Container top widget
// **********************************
if (is_active_sidebar('widget-top-container')) {
	if (isset($options_visual['full_wide_container_widget_area_top']) && !empty($options_visual['full_wide_container_widget_area_top'])) {
		ob_start(); ?>
<div id="top-container-widget" class="container-widget-area pos-top liquid clearfix"><?php
		dynamic_sidebar('widget-top-container'); ?>
</div><?php
		$widget_container_top_content = ob_get_contents();
		ob_end_clean();
	} else {
		ob_start(); ?>
<div id="top-container-widget" class="container-widget-area pos-top clearfix"><?php
		dynamic_sidebar('widget-top-container'); ?>
</div><?php
		$widget_container_top_content = ob_get_contents();
		ob_end_clean();
	}
	echo $widget_container_top_content;
}?>
<div id="container" class="dp-container clearfix">
<a class="breadcrumb_arrow aligncenter" href="#post-<?php the_ID(); ?>"><span>Read Article</span></a><?php
/*************
 * Content start
 ************/
if ( $COLUMN_NUM == 1 ) : 
?>
<div id="content-top-1col" class="content one-col">
<?php 
elseif ( $COLUMN_NUM == 3 ) :
?>
<div id="content" class="content three-col">
<?php
else : 
?>
<div id="content" class="content">
<?php 
endif;

if (have_posts()) :

		// Post title
		$post_title =  the_title('', '', false) ? the_title('', '', false) : __('No Title', 'DigiPress');

		// GET THE FLAG TO SHOW SNS ICON 
		$hideSNSIconFlag = get_post_meta(get_the_ID(), 'hide_sns_icon', true);

		// Get the flag to hide title
		$hideTitleFlag 	 = get_post_meta(get_the_ID(), 'dp_hide_title', true);

		// GET THE POST TYPE
		$postType = get_post_type();

		$postFormat = get_post_format();

		// Content widget
		if (($postType === 'page') && is_active_sidebar('widget-top-content')) : ?>
<div id="top-content-widget" class="clearfix">
<?php dynamic_sidebar( 'widget-top-content' ); ?>
</div>
<?php
		endif;
	while (have_posts()) : the_post(); ?>
<?php
// Count Post View
if (function_exists('dp_count_post_views')) {
	dp_count_post_views(get_the_ID(), true);
}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php 
if (!$hideTitleFlag): 
?>
<header>
<h1 class="entry-title posttitle"><span><?php echo $post_title; ?></span></h1>
<?php
	// header meta
	if ( ( isset( $options['show_pubdate_on_meta_page'] ) && !empty( $options['show_pubdate_on_meta_page']  ) ) || ( isset( $options['show_author_on_meta_page'] ) && !empty( $options['show_author_on_meta_page'] ) ) || ( isset( $options['sns_button_under_title'] ) && !empty( $options['sns_button_under_title'] ) && !get_post_meta(get_the_ID(), 'hide_sns_icon', true ) ) ) :

		// Call meta contents
		showPostMetaForSingleTop($postFormat);

	endif;  // End of postmeta_title division ?>
</header>
<?php
endif;	// !$hideTitleFlag

	// Single header widget
	if (($postType === 'page') && is_active_sidebar('widget-post-header') && !post_password_required()) : ?>
<div id="single-header-widget" class="clearfix">
		<?php dynamic_sidebar( 'widget-post-header' ); ?>
</div>
	<?php endif; ?>
<div class="entry entry-content">
		<?php
		// Content
		the_content(__('Read more', 'DigiPress'));
		// Paged navigation
		$link_pages = wp_link_pages(array(
									'before' => '', 
									'after' => '', 
									'next_or_number' => 'number', 
									'echo' => '0'));
		if ( $link_pages != '' ) {
			echo '<nav class="navigation"><div class="dp-pagenavi clearfix"><span class="pages">Pages : </span>';
			if ( preg_match_all("/(<a [^>]*>[\d]+<\/a>|[\d]+)/i", $link_pages, $matched, PREG_SET_ORDER) ) {
				foreach ($matched as $link) {
					if (preg_match("/<a ([^>]*)>([\d]+)<\/a>/i", $link[0], $link_matched)) {
						echo "<a class=\"page-numbers\" {$link_matched[1]}>{$link_matched[2]}</a>";
					} else {
						echo "<span class=\"current\">{$link[0]}</span>";
					}
				}
			}
			echo '</div></nav>';
		}
		//wp_link_pages('before=<nav class="navigation clearfix"><div class="dp-pagenavi al-c">&after=</div></nav>');
		?>
</div>
		<?php // Single footer widget
		if (is_active_sidebar('widget-post-footer') && !post_password_required()) : ?>
<div id="single-footer-widget" class="clearfix entry">
			<?php dynamic_sidebar( 'widget-post-footer' ); ?>
</div>
		<?php
		endif;
		
		// Meta
		showPostMetaForSingleBottom($postFormat);
		?>
</article>
<?php endwhile; ?>
<?php // Content bottom widget
if (is_active_sidebar('widget-top-content-bottom')) : ?>
<div id="top-content-bottom-widget" class="clearfix">
<?php dynamic_sidebar( 'widget-top-content-bottom' ); ?>
</div>
<?php endif; ?>
<?php else : ?>
<article class="post">
<header><h1 class="entry-title posttitle"><?php _e('Not Found.', 'DigiPress'); ?></h1></header>
<div class="entry entry-content">
<p><?php _e('Apologies, but the page you requested could not be found. <br />Perhaps searching will help.', 'DigiPress'); ?></p>
</div>
</article>
	<?php endif; ?>
</div>
<?php
// Sidebar
if ($COLUMN_NUM == 2) {
	get_sidebar();
} else if ($COLUMN_NUM == 3) {
	get_sidebar();
	get_sidebar('2');
}
?>
</div>
<?php get_footer(); ?>
<?php 
//For SNS Buttons
if ( ( isset( $options['sns_button_under_title'] ) && !empty( $options['sns_button_under_title'] ) ) || ( isset( $options['sns_button_on_meta'] ) && !empty( $options['sns_button_on_meta'] ) ) ) {
	if (!$hideSNSIconFlag && ($postType === 'page')) {

		if ( isset( $options[ 'show_hatena_button' ] ) && !empty( $options[ 'show_hatena_button' ] ) ) {
			echo '<script src="//b.hatena.ne.jp/js/bookmark_button.js" async="async"></script>';
		}
		if ( isset( $options['show_mixi_button'] ) && !empty( $options['show_mixi_button'] ) && ( isset( $options['mixi_accept_key'] ) && !empty( $options['mixi_accept_key'] ) ) ) {
			echo '<script>(function(d) {var s = d.createElement(\'script\'); s.type = \'text/javascript\'; s.async = true;s.src = \'//static.mixi.jp/js/plugins.js#lang=ja\';d.getElementsByTagName(\'head\')[0].appendChild(s);})(document);</script>';
		}
		if ( isset( $options[ 'show_tumblr_button' ] ) && !empty( $options[ 'show_tumblr_button' ] ) ) {
			echo '<script id="tumblr-js" async src="https://assets.tumblr.com/share-button.js"></script>';
		}
		if ( isset( $options[ 'show_pocket_button' ] ) && !empty( $options[ 'show_pocket_button' ] ) ) {
			echo '<script>!function(d,i){if(!d.getElementById(i)){var j=d.createElement("script");j.id=i;j.src="https://widgets.getpocket.com/v1/j/btn.js?v=1";var w=d.getElementById(i);d.body.appendChild(j);}}(document,"pocket-btn-js");</script>';
		}
		if ( isset( $options['show_pinterest_button'] ) && !empty( $options['show_pinterest_button'] ) ) {
			echo '<script async defer src="//assets.pinterest.com/js/pinit.js"></script>';
		}
		if ($options['show_facebook_button'] || $options['facebookcomment_page']) {
			// Get Facebook App ID
			$fb_app_id = isset($options['fb_app_id']) ? $options['fb_app_id'] : '';
			if (empty($fb_app_id)) {
				$fb_app_id = $FB_APP_ID;
			}
			echo '<div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/' . $options['fb_api_lang'] . '/sdk.js#xfbml=1&version=v12.0&appId=' . $fb_app_id . '&autoLogAppEvents=1"></script>';
		}
		if ( isset( $options[ 'show_twitter_button' ] ) && !empty( $options[ 'show_twitter_button' ] ) ) {
			echo '<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';
		}
	} else if ($EXIST_FB_LIKE_BOX && ($postType === 'page')) {
		$fb_app_id = isset($options['fb_app_id']) ? $options['fb_app_id'] : '';
		if (empty($fb_app_id)) {
			$fb_app_id = $FB_APP_ID;
		}
		echo '<div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/' . $options['fb_api_lang'] . '/sdk.js#xfbml=1&version=v12.0&appId=' . $fb_app_id . '&autoLogAppEvents=1"></script>';
	}
}
?>
</body>
</html>