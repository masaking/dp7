<?php 
// Header
get_header(); 
?>
<body <?php body_class(); ?>>
<?php if (is_front_page() && !is_paged()) : ?>
	<?php if ( isset( $options_visual['header_area_low_height'] ) && !empty( $options_visual['header_area_low_height'] ) ) : ?>
<header id="header_area_half">
	<?php else: ?>
<header id="header_area">
	<?php endif; ?>
<?php else : ?>
<header id="header_area_paged">
<?php endif; ?>
<?php 
include_once(TEMPLATEPATH . "/fixed_menu.php");
dp_banner_contents();
?>
</header>
<?php 
if (is_home() && !is_paged()) : // Top Page
	// Show headline
	dp_headline();
else :
// Except Top Page 
?>
<section class="dp_topbar_title"><?php dp_breadcrumb(); ?></section>
<?php
endif;
// **********************************
// Container top widget
// **********************************
if (is_active_sidebar('widget-top-container')) {
	if (isset($options_visual['full_wide_container_widget_area_top']) && !empty($options_visual['full_wide_container_widget_area_top'])) {
		ob_start(); ?>
<div id="top-container-widget" class="container-widget-area pos-top liquid clearfix"><?php
		dynamic_sidebar('widget-top-container'); ?>
</div><?php
		$widget_container_top_content = ob_get_contents();
		ob_end_clean();
	} else {
		ob_start(); ?>
<div id="top-container-widget" class="container-widget-area pos-top clearfix"><?php
		dynamic_sidebar('widget-top-container'); ?>
</div><?php
		$widget_container_top_content = ob_get_contents();
		ob_end_clean();
	}
	echo $widget_container_top_content;
}?>
<div id="container" class="dp-container clearfix">
<?php 
if (is_home() && is_paged()) : 
?>
<div class="breadcrumb_arrow aligncenter"><span>Articles</span></div>
<?php 
endif;

/*************
 * Content start
 ************/
if ( $COLUMN_NUM == 1 ) : 
?>
<div id="content-top-1col" class="content one-col">
<?php 
elseif ( $COLUMN_NUM == 3 ) :
?>
<div id="content" class="content three-col">
<?php
else : 
?>
<div id="content" class="content">
<?php 
endif;
 
// Content widget
if (is_active_sidebar('widget-top-content')) : ?>
<div id="top-content-widget" class="clearfix">
<?php dynamic_sidebar( 'widget-top-content' ); ?>
</div>
<?php endif; ?>

<?php 
include_once(TEMPLATEPATH . "/index-top.php");
// show posts
if (have_posts()) :
	include_once(TEMPLATEPATH . "/index-under.php");
else : ?>
<article class="post">
<header><h1 class="posttitle"><?php _e('Not found.','DigiPress'); ?></h1></header>
<div class="entry">
<p><?php _e('Apologies, but the page you requested could not be found. <br />Perhaps searching will help.', 'DigiPress'); ?></p>
</div>
</article>
<?php endif; ?>
</div>
<?php
// Sidebar
if ($COLUMN_NUM == 2) {
	get_sidebar();
} else if ($COLUMN_NUM == 3) {
	get_sidebar();
	get_sidebar('2');
}
?>
</div>
<?php 
get_footer();

if ( $EXIST_FB_LIKE_BOX ) {
		$fb_app_id = isset($options['fb_app_id']) ? $options['fb_app_id'] : '';
		if (empty($fb_app_id)) {
			$fb_app_id = $FB_APP_ID;
		}
		echo '<div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/' . $options['fb_api_lang'] . '/sdk.js#xfbml=1&version=v12.0&appId=' . $fb_app_id . '&autoLogAppEvents=1"></script>';
	}
?>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</body>
</html>