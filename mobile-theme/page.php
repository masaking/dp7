<?php 
/* ------------------------------------------
   This template is for mobile device!!!!!!!
 ------------------------------------------*/
include_once(TEMPLATEPATH . '/' . DP_MOBILE_THEME_DIR . '/header.php');
?>
<body <?php body_class('mb-theme'); ?>>
<div id="wrap" class="mobile">
<?php include_once(TEMPLATEPATH . '/' . DP_MOBILE_THEME_DIR . '/top-menu.php'); ?>
<header id="header_area">
<?php dp_banner_contents_mobile(); ?>
</header>
<section class="dp_topbar_title"><?php dp_breadcrumb(); ?></section>
<div class="breadcrumb_arrow aligncenter"><span>Articles</span></div>
<div id="container" class="dp-container clearfix">
<div id="content" class="content">
<?php 
if (have_posts()) :
	// Widget
	if (is_active_sidebar('widget-top-container-mobile')) : ?>
<div id="top-container-widget"><div id="top-content-widget">
<?php dynamic_sidebar( 'widget-top-container-mobile' ); ?>
</div></div>
<?php 
	endif;

	// GET THE POST TYPE
	$postType = get_post_type();

	// Next post title
	$nextPost = get_next_post();
	$nextPostTitle = $nextPost->post_title;
	// Previous post title
	$prevPost = get_previous_post();
	$prevPostTitle = $prevPost->post_title;

	// Post format
	$postFormat = get_post_format();

	// Get icon class each post format
	$titleIconClass = postFormatIcon($postFormat);

	// Get the flag to hide title
	$hideTitleFlag 	 = get_post_meta(get_the_ID(), 'dp_hide_title', true);

	// Post title
	$post_title =  the_title('', '', false) ? the_title('', '', false) : __('No Title', 'DigiPress');

	// GET THE FLAG TO SHOW SNS ICON 
	$hideSNSIconFlag = get_post_meta(get_the_ID(), 'hide_sns_icon', true);

	while (have_posts()) : the_post();
		// Count Post View
		if (function_exists('dp_count_post_views')) {
			dp_count_post_views(get_the_ID(), true);
		}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php 
	if (!$hideTitleFlag): 
?>
<header>
<h1 class="entry-title posttitle<?php echo $titleIconClass; ?>"><span><?php echo $post_title; ?></span></h1>
<?php
		// header meta
		if ( ( isset( $options['show_pubdate_on_meta_page'] ) && !empty( $options['show_pubdate_on_meta_page']  ) ) || ( isset( $options['show_author_on_meta_page'] ) && !empty( $options['show_author_on_meta_page'] ) ) || ( isset( $options['sns_button_under_title'] ) && !empty( $options['sns_button_under_title'] ) && !get_post_meta(get_the_ID(), 'hide_sns_icon', true ) ) ) :

			// Call meta contents
			showPostMetaForSingleTop($postFormat);
		 endif; 
?>
</header>
<?php 
	endif; //!$hideTitleFlag

	// Single header widget
	if (($postType === 'page') && is_active_sidebar('under-post-title-mobile') && !post_password_required()) : ?>
	<div id="single-header-widget" class="clearfix">
			<?php dynamic_sidebar( 'under-post-title-mobile' ); ?>
	</div>
<?php 
	endif;
?>
<div class="entry entry-content">
<?php
	// Show eyecatch image
	if (has_post_thumbnail() && isset( $options['show_eyecatch_first'] ) && !empty( $options['show_eyecatch_first'] ) && ($postType === 'page')) {
		$width 	= 620;
		$height	= 452;
		if (( isset( $options_visual['dp_column'] ) && $options_visual['dp_column'] == '1') || get_post_meta(get_the_ID(), 'disable_sidebar', true)) {
			$width 	= 930;
			$height	= 686;
		}
		$image_id	= get_post_thumbnail_id();
		$image_data	= wp_get_attachment_image_src($image_id, array($width, $height), true);
		$image_url 	= is_ssl() ? str_replace('http:', 'https:', $image_data[0]) : $image_data[0];
		$img_tag	= '<img src="'.$image_url.'" width="'.$width.'" class="wp-post-image aligncenter" alt="'.get_the_title().'"  />';
		echo '<div class="al-c">' . $img_tag . '</div>';
	}
	// Content
	the_content(__('Read more', 'DigiPress'));
	// Paged navigation
	$link_pages = wp_link_pages(array(
									'before' => '', 
									'after' => '', 
									'next_or_number' => 'number', 
									'echo' => '0'));
	if ( $link_pages != '' ) {
		echo '<nav class="navigation-mb"><div class="dp-pagenavi clearfix">';
		if ( preg_match_all("/(<a [^>]*>[\d]+<\/a>|[\d]+)/i", $link_pages, $matched, PREG_SET_ORDER) ) {
			foreach ($matched as $link) {
				if (preg_match("/<a ([^>]*)>([\d]+)<\/a>/i", $link[0], $link_matched)) {
					echo "<a class=\"page-numbers\" {$link_matched[1]}>{$link_matched[2]}</a>";
				} else {
					echo "<span class=\"current\">{$link[0]}</span>";
				}
			}
		}
		echo '</div></nav>';
	}
?>
</div>
	<?php // Single footer widget
		if (($postType === 'page') && is_active_sidebar('bottom-of-post-mobile') && !post_password_required()) : ?>
<div id="single-footer-widget" class="clearfix">
	<?php dynamic_sidebar( 'bottom-of-post-mobile' ); ?>
</div>
	<?php
		endif;
		// Meta
		showPostMetaForSingleBottom($postFormat);

		?>
</article>
<?php
	endwhile; 
?>
<?php
else : // else (have_posts())
?>
<article class="post">
<header><h1 class="entry-title posttitle"><?php _e('Not Found.', 'DigiPress'); ?></h1></header>
<div class="entry entry-content">
<p><?php _e('Apologies, but the page you requested could not be found. <br />Perhaps searching will help.', 'DigiPress'); ?></p>
</div>
</article>
<?php 
endif; 	// End of have_posts() 
?>
</div><?php // End of content ?>
</div><?php // End of container ?>
</div><?php // End of wrap ?>
<?php 
include_once(TEMPLATEPATH . "/mobile-theme/footer.php");

//For SNS Buttons
if ( ( isset( $options['sns_button_under_title'] ) && !empty( $options['sns_button_under_title'] ) ) || ( isset( $options['sns_button_on_meta'] ) && !empty( $options['sns_button_on_meta'] ) ) ) {
	if (!$hideSNSIconFlag && ($postType === 'page')) {

		if ( isset( $options[ 'show_hatena_button' ] ) && !empty( $options[ 'show_hatena_button' ] ) ) {
			echo '<script src="//b.hatena.ne.jp/js/bookmark_button.js" async="async"></script>';
		}
		if ( isset( $options['show_mixi_button'] ) && !empty( $options['show_mixi_button'] ) && ( isset( $options['mixi_accept_key'] ) && !empty( $options['mixi_accept_key'] ) ) ) {
			echo '<script type="text/javascript">(function(d) {var s = d.createElement(\'script\'); s.type = \'text/javascript\'; s.async = true;s.src = \'//static.mixi.jp/js/plugins.js#lang=ja\';d.getElementsByTagName(\'head\')[0].appendChild(s);})(document);</script>';
		}
		if ( isset( $options[ 'show_tumblr_button' ] ) && !empty( $options[ 'show_tumblr_button' ] ) ) {
			echo '<script id="tumblr-js" async src="https://assets.tumblr.com/share-button.js"></script>';
		}
		if ( isset( $options[ 'show_pocket_button' ] ) && !empty( $options[ 'show_pocket_button' ] ) ) {
			echo '<script type="text/javascript">!function(d,i){if(!d.getElementById(i)){var j=d.createElement("script");j.id=i;j.src="https://widgets.getpocket.com/v1/j/btn.js?v=1";var w=d.getElementById(i);d.body.appendChild(j);}}(document,"pocket-btn-js");</script>';
		}
		if ( isset( $options['show_pinterest_button'] ) && !empty( $options['show_pinterest_button'] ) ) {
			echo '<script async defer src="//assets.pinterest.com/js/pinit.js"></script>';
		}
		if ($options['show_facebook_button'] || $options['facebookcomment']) {
			// Get Facebook App ID
			$fb_app_id = isset($options['fb_app_id']) ? $options['fb_app_id'] : '';
			if (empty($fb_app_id)) {
				$fb_app_id = $FB_APP_ID;
			}
			echo '<div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/' . $options['fb_api_lang'] . '/sdk.js#xfbml=1&version=v12.0&appId=' . $fb_app_id . '&autoLogAppEvents=1"></script>';
		}
		if ( isset( $options[ 'show_twitter_button' ] ) && !empty( $options[ 'show_twitter_button' ] ) ) {
			echo '<script type="text/javascript">!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';
		}
	} else if ($EXIST_FB_LIKE_BOX) {
		$fb_app_id = isset($options['fb_app_id']) ? $options['fb_app_id'] : '';
		if (empty($fb_app_id)) {
			$fb_app_id = $FB_APP_ID;
		}
		echo '<div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/' . $options['fb_api_lang'] . '/sdk.js#xfbml=1&version=v12.0&appId=' . $fb_app_id . '&autoLogAppEvents=1"></script>';
	}
}
?>
</body>
</html>