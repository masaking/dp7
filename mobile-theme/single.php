<?php 
/* ------------------------------------------
   This template is for mobile device!!!!!!!
 ------------------------------------------*/
include_once(TEMPLATEPATH . '/' . DP_MOBILE_THEME_DIR . '/header.php');
?>
<body <?php body_class('mb-theme'); ?>>
<div id="wrap" class="mobile">
<?php include_once(TEMPLATEPATH . '/' . DP_MOBILE_THEME_DIR . '/top-menu.php'); ?>
<header id="header_area">
<?php dp_banner_contents_mobile(); ?>
</header>
<section class="dp_topbar_title"><?php dp_breadcrumb(); ?></section>
<div class="breadcrumb_arrow aligncenter"><span>Articles</span></div>
<div id="container" class="dp-container clearfix">
<div id="content" class="content">
<?php 
if (have_posts()) :
	// Widget
	if (is_active_sidebar('widget-top-container-mobile')) : ?>
<div id="top-container-widget"><div id="top-content-widget">
<?php dynamic_sidebar( 'widget-top-container-mobile' ); ?>
</div></div>
<?php 
	endif;

	// GET THE POST TYPE
	$postType = get_post_type();

	// Post format
	$postFormat = get_post_format();

	// Get icon class each post format
	$titleIconClass = postFormatIcon($postFormat);

	// Post title
	$post_title =  the_title('', '', false) ? the_title('', '', false) : __('No Title', 'DigiPress');

	// GET THE FLAG TO SHOW SNS ICON 
	$hideSNSIconFlag = get_post_meta(get_the_ID(), 'hide_sns_icon', true);

	while (have_posts()) : the_post();
		// Count Post View
		if (function_exists('dp_count_post_views')) {
			dp_count_post_views(get_the_ID(), true);
		}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<?php if ( $postFormat !== 'quote' ) : ?> 
<header>
<h1 class="entry-title posttitle<?php echo $titleIconClass; ?>"><span><?php echo $post_title; ?></span></h1>
<?php
	// header meta
	if ( ( isset( $options['show_pubdate_on_meta'] ) && !empty( $options['show_pubdate_on_meta'] ) ) || ( isset( $options['show_views_on_meta'] ) && !empty( $options['show_views_on_meta'] ) ) || ( isset( $options['show_author_on_meta'] ) && !empty( $options['show_views_on_meta'] ) ) || ( isset( $options['time_for_reading'] ) && !empty( $options['time_for_reading'] ) ) || ( isset( $options['sns_button_under_title'] ) && !empty( $options['sns_button_under_title'] ) && !get_post_meta(get_the_ID(), 'hide_sns_icon', true ) ) ) : 
		// Call meta contents
		showPostMetaForSingleTop($postFormat);

	 endif; ?>
</header>
<?php
	// Single header widget
	if (($postType === 'post') && is_active_sidebar('under-post-title-mobile') && !post_password_required()) : ?>
	<div id="single-header-widget" class="clearfix">
			<?php dynamic_sidebar( 'under-post-title-mobile' ); ?>
	</div>
<?php 
	endif;
?>
<?php endif; ?>
<div class="entry entry-content">
<?php
	// Show eyecatch image
	if (has_post_thumbnail() && isset( $options['show_eyecatch_first'] ) && !empty( $options['show_eyecatch_first'] ) && ($postType === 'post')) {
		$width 	= 620;
		$height	= 452;
		if ( ( isset( $options_visual['dp_column'] ) && $options_visual['dp_column'] == '1' ) || get_post_meta(get_the_ID(), 'disable_sidebar', true)) {
			$width 	= 930;
			$height	= 686;
		}
		$arg_thumb = array('width' => $width, 'height' => $height, "if_img_tag"=> true);
		$image_id	= get_post_thumbnail_id();
		$image_data	= wp_get_attachment_image_src($image_id, array($width, $height), true);
		$image_url 	= is_ssl() ? str_replace('http:', 'https:', $image_data[0]) : $image_data[0];
		$img_tag	= '<img src="'.$image_url.'" width="'.$width.'" class="wp-post-image aligncenter" alt="'.get_the_title().'"  />';
		echo '<div class="al-c">' . $img_tag . '</div>';
	}
	// Content
	the_content(__('Read more', 'DigiPress'));
	// Paged navigation
	$link_pages = wp_link_pages(array(
									'before' => '', 
									'after' => '', 
									'next_or_number' => 'number', 
									'echo' => '0'));
	if ( $link_pages != '' ) {
		echo '<nav class="navigation-mb"><div class="dp-pagenavi clearfix">';
		if ( preg_match_all("/(<a [^>]*>[\d]+<\/a>|[\d]+)/i", $link_pages, $matched, PREG_SET_ORDER) ) {
			foreach ($matched as $link) {
				if (preg_match("/<a ([^>]*)>([\d]+)<\/a>/i", $link[0], $link_matched)) {
					echo "<a class=\"page-numbers\" {$link_matched[1]}>{$link_matched[2]}</a>";
				} else {
					echo "<span class=\"current\">{$link[0]}</span>";
				}
			}
		}
		echo '</div></nav>';
	}
?>
</div>
	<?php // Single footer widget
		if (($postType === 'post') && is_active_sidebar('bottom-of-post-mobile') && !post_password_required()) : ?>
<div id="single-footer-widget" class="clearfix">
	<?php dynamic_sidebar( 'bottom-of-post-mobile' ); ?>
</div>
	<?php
		endif;
		// Meta info
		showPostMetaForSingleBottom($postType);

		include_once(TEMPLATEPATH . '/' . DP_MOBILE_THEME_DIR . '/related_posts.php');
		if (function_exists('similar_posts')) {
			echo '<aside class="entry similar-posts">';
			similar_posts();
			echo '</aside>';
		}
		// Comment
		comments_template();
		?>
</article>
<?php
	endwhile; 
?>
<?php 
// Prev next post navigation link
$in_same_cat = isset( $options['next_prev_in_same_cat'] ) && !empty( $options['next_prev_in_same_cat'] ) ? true : false;
// Next post title
$next_post = get_next_post($in_same_cat);
// Previous post title
$prev_post = get_previous_post($in_same_cat);
if ($prev_post || $next_post) : 
?>
<nav class="navigation-mb singlenav-mb clearfix">
<?php 
	$post_id = 0;
	$img_tag = '';
	if ( isset( $prev_post ) && !empty( $prev_post ) ) {
		if ($postType === 'post') {
			$post_id = $prev_post->ID;
			// Get thumbnail
			$arg_thumb = array('width' => 100, 'height' => 73, "if_img_tag"=> true, "post_id" => $post_id);
			$img_tag = show_post_thumbnail($arg_thumb);
			if ($img_tag) {
				$img_tag = '<span class="nav-prev-thumb">'.$img_tag.'</span>';
			}
		}
		echo '<div class="navialignleft-mb" title="'.$prev_post->post_title.'"><a href="'.get_permalink($prev_post->ID).'">'.$img_tag.'<span>PREV</span></a></div>';
	} else {
		echo '<div class="navialignleft-mb"><span>'.__('Oldest Post', 'DigiPress').'</span></div>';
	}
	if ( isset( $next_post ) && !empty( $next_post ) ) {
		if ($postType === 'post') {
			$post_id = $next_post->ID;
			$arg_thumb = array('width' => $width, 'height' => $height, "if_img_tag"=> true, "post_id" => $post_id);
			// Get thumbnail
			$img_tag = show_post_thumbnail($arg_thumb);
			if ($img_tag) {
				$img_tag = '<span class="nav-next-thumb">'.$img_tag.'</span>';
			}
		}
		echo '<div class="navialignright-mb" title="'.$next_post->post_title.'"><a href="'.get_permalink($next_post->ID).'"><span>NEXT</span>'.$img_tag.'</a></div>';
	} else {
		echo '<div class="navialignright-mb"><span>'.__('Newest Post', 'DigiPress').'</span></div>';
	}
?>
</nav>
<?php endif; ?>
<?php
else : // else (have_posts())
?>
<article class="post">
<header><h1 class="entry-title posttitle"><?php _e('Not Found.', 'DigiPress'); ?></h1></header>
<div class="entry entry-content">
<p><?php _e('Apologies, but the page you requested could not be found. <br />Perhaps searching will help.', 'DigiPress'); ?></p>
</div>
</article>
<?php 
endif; 	// End of have_posts() 
?>
</div><?php // End of content ?>
</div><?php // End of container ?>
</div><?php // End of wrap ?>
<?php 
include_once(TEMPLATEPATH . "/mobile-theme/footer.php");

$fb_flg = false;
//For SNS Buttons
if ( ( isset( $options['sns_button_under_title'] ) && !empty( $options['sns_button_under_title'] ) ) || ( isset( $options['sns_button_on_meta'] ) && !empty( $options['sns_button_on_meta'] ) ) ) {
	if (!$hideSNSIconFlag ) {

		if ( isset( $options[ 'show_hatena_button' ] ) && !empty( $options[ 'show_hatena_button' ] ) ) {
			echo '<script src="//b.hatena.ne.jp/js/bookmark_button.js" async="async"></script>';
		}
		if ( isset( $options['show_mixi_button'] ) && !empty( $options['show_mixi_button'] ) && ( isset( $options['mixi_accept_key'] ) && !empty( $options['mixi_accept_key'] ) ) ) {
			echo '<script type="text/javascript">(function(d) {var s = d.createElement(\'script\'); s.type = \'text/javascript\'; s.async = true;s.src = \'//static.mixi.jp/js/plugins.js#lang=ja\';d.getElementsByTagName(\'head\')[0].appendChild(s);})(document);</script>';
		}
		if ( isset( $options[ 'show_tumblr_button' ] ) && !empty( $options[ 'show_tumblr_button' ] ) ) {
			echo '<script id="tumblr-js" async src="https://assets.tumblr.com/share-button.js"></script>';
		}
		if ( isset( $options[ 'show_pocket_button' ] ) && !empty( $options[ 'show_pocket_button' ] ) ) {
			echo '<script type="text/javascript">!function(d,i){if(!d.getElementById(i)){var j=d.createElement("script");j.id=i;j.src="https://widgets.getpocket.com/v1/j/btn.js?v=1";var w=d.getElementById(i);d.body.appendChild(j);}}(document,"pocket-btn-js");</script>';
		}
		if ( isset( $options['show_pinterest_button'] ) && !empty( $options['show_pinterest_button'] ) ) {
			echo '<script async defer src="//assets.pinterest.com/js/pinit.js"></script>';
		}
		if ( isset( $options[ 'show_facebook_button' ] ) && !empty( $options[ 'show_facebook_button' ] ) ) {
			// Get Facebook App ID
			$fb_app_id =  ($options['fb_app_id'] != '') ? '&appId='.$options['fb_app_id'] : '';
			if ($fb_app_id) {
				$fb_app_id = ($FB_APP_ID) ? '&appId='.$FB_APP_ID : '';
			}

			echo '<div id="fb-root"></div><script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.3' . $fb_app_id . '";fjs.parentNode.insertBefore(js, fjs);}(document, \'script\', \'facebook-jssdk\'));</script>';
			// Set to true
			$fb_flg = true;
		}
		if ( isset( $options[ 'show_twitter_button' ] ) && !empty( $options[ 'show_twitter_button' ] ) ) {
			echo '<script type="text/javascript">!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>';
		}
	} else if ($EXIST_FB_LIKE_BOX) {
		$fb_app_id = isset($options['fb_app_id']) ? $options['fb_app_id'] : '';
		if (empty($fb_app_id)) {
			$fb_app_id = $FB_APP_ID;
		}
		echo '<div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/' . $options['fb_api_lang'] . '/sdk.js#xfbml=1&version=v12.0&appId=' . $fb_app_id . '&autoLogAppEvents=1"></script>';
		// Set to true
		$fb_flg = true;
	}
}

if (!$fb_flg && isset( $options['facebookcomment'] ) && !empty( $options['facebookcomment'] ) ) {
	// Get Facebook App ID
	$fb_app_id = isset($options['fb_app_id']) ? $options['fb_app_id'] : '';
		if (empty($fb_app_id)) {
			$fb_app_id = $FB_APP_ID;
		}
		echo '<div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/' . $options['fb_api_lang'] . '/sdk.js#xfbml=1&version=v12.0&appId=' . $fb_app_id . '&autoLogAppEvents=1"></script>';

		$fb_flg = true;
}?>
</body>
</html>