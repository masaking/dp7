<?php // Container footer widget
if (is_active_sidebar('container-bottom-mobile')) : ?>
<div id="container_footer">
<div id="content_footer">
<?php dynamic_sidebar( 'container-bottom-mobile' ); ?>
</div>
</div>
<?php endif; ?>
<footer id="footer">
<div id="ft-widget-container">
<div id="ft-widget-content">
<?php // show footer widgets
if (is_active_sidebar('footer-mobile')) : 
	dynamic_sidebar( 'footer-mobile' );
endif;
?>
</div>
</div>
<?php include_once(TEMPLATEPATH . '/' . DP_MOBILE_THEME_DIR . '/footer-menu.php'); ?>
<div id="footer-bottom-mb">
<a href="#header_area" id="gototop-mb" class="footer_arrow icon-angle-up"><span>Return Top</span></a>
<div id="ft-btm-content">&copy; <?php 
global $options;
$current_year = (string)date('Y');
if (isset($options['blog_start_year']) && !empty($options['blog_start_year'])) {
	if ( $options['blog_start_year'] !== $current_year ){
		echo $options['blog_start_year'] . ' - ' . $current_year;
	} else {
		echo $current_year;
	}
} else {
	echo $current_year;
} ?> <a href="<?php echo home_url(); ?>/"><small><?php bloginfo('name'); ?></small></a>
</div></div>
</footer>
<a href="#header_area" id="gototop2" class="icon-up-open" title="Return Top"><span>Return Top</span></a>
<?php
// ******************************
// WordPress footer
// ******************************
wp_footer();
// ******************************
// Autopager JS
// ******************************
showScriptForAutopagerMb($wp_query->max_num_pages);
// ******************************
// Headline JS
// ******************************
if ( isset( $options['headline_type'] ) && $options['headline_type'] === '3' && (is_home() && !is_paged()) ) {
	if ( isset( $options['headline_slider_fx'] ) && $options['headline_slider_fx'] === '1') {
		$headlineTime = isset( $options['headline_slider_time'] ) ? $options['headline_slider_time'] : 3000;
		$headlineHoverStop = isset( $options['headline_hover_stop'] ) && !empty( $options['headline_hover_stop'] ) ? 'true' : 'false';
		$headlineArrow = isset( $options['headline_arrow'] ) && !empty( $options['headline_arrow'] ) ? 'true' : 'false';

		$headline_js = <<<_EOD_
<script>j$(function(){j$('.headline_slider').glide({autoplay:$headlineTime,hoverpause:$headlineHoverStop,arrows:false,nav:false,afterInit:(function(){j$('.slides').fadeIn();})});});</script>
_EOD_;

	} else {
		$tickerVelocity = $options['headline_slider_velocity'];
		$headline_js = <<<_EOD_
<script>j$(function(){j$(function(){j$("#headline_ticker").liScroll({travelocity:$tickerVelocity});});j$('.slides').fadeIn();});</script>
_EOD_;
	}
	echo $headline_js;
}
// ******************************
// Google Custom Search
// ******************************
if ($options['gcs_id'] !== '') : 
?>
<script>(function(){var cx='<?php echo $options['gcs_id']; ?>';var gcse=document.createElement('script');gcse.type= 'text/javascript';gcse.async=true;gcse.src=(document.location.protocol=='https:'?'https:':'http:')+'//cse.google.com/cse.js?cx='+cx;var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(gcse,s);})();</script>
<?php 
endif;
// **********************************
// JSON-LD for Structured Data
// **********************************
dp_json_ld();