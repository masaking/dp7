<?php 
/* ------------------------------------------
   This template is for mobile device!!!!!!!
 ------------------------------------------*/
include_once(TEMPLATEPATH . '/' . DP_MOBILE_THEME_DIR . '/header.php');
?>
<body <?php body_class('mb-theme'); ?>>
<div id="wrap" class="mobile">
<?php include_once(TEMPLATEPATH . '/' . DP_MOBILE_THEME_DIR . '/top-menu.php'); ?>
<header id="header_area">
<?php dp_banner_contents_mobile(); ?>
</header>
<section class="dp_topbar_title"><?php dp_breadcrumb(); ?></section>
<div class="breadcrumb_arrow aligncenter"><span>Articles</span></div>
<?php
// Show result
if (is_search() && !isset( $_REQUEST['q'])) {
	if ($wp_query->found_posts !== 0) {
		echo '<p id="found-title"><span>' . $wp_query->found_posts . __(' posts has found.', 'DigiPress') . '</span></p>';
	}
}
?>
<div id="container" class="dp-container clearfix">
<?php 
// Container widget
if (is_active_sidebar('widget-top-container-mobile')) : ?>
<div id="top-container-widget"><div id="top-content-widget">
<?php dynamic_sidebar( 'widget-top-container-mobile' ); ?>
</div></div>
<?php 
endif;
?>
<div id="content" class="content">
<?php
// Google Custom Search
if (isset( $_REQUEST['q'] ) ) : //Google Custom Search ?>
<gcse:searchresults-only></gcse:searchresults-only>
<?php 
else :
//Default Search
?> 
<?php 
// show posts
if (have_posts()) :

	// Excerpt length
	$excerpt_length = 120;

	$mobile_class = is_mobile_dp() ? ' mobile': '';

	//For thumbnail size
	$width = 200;
	$height = 147;
	$arg_thumb = array('width' => $width, 'height' => $height, "if_img_tag"=> true);

	// Settings for infeed ads
	$infeed_ads_flg = false;
	$infeed_ads_code = '';
	$infeed_ads_order = null;
	if ( isset( $options['archive_infeed_ads_code_mb'] ) && !empty( $options['archive_infeed_ads_code_mb'] ) && isset( $options['archive_infeed_ads_order_mb'] ) && !empty($options['archive_infeed_ads_order_mb'] ) ) {
		$infeed_ads_flg = true;
		$infeed_ads_code = $options['archive_infeed_ads_code_mb'];
		$infeed_ads_order = explode(",", $options['archive_infeed_ads_order_mb']);
	}

	if ( isset( $options['archive_post_show_type'] ) && ( $options['archive_post_show_type'] == 'normal' || $options['archive_post_show_type'] == 'gallery' ) ) : 
		echo '<div id="posts-normal">';
	//Loop each post
	while (have_posts()) : the_post();
		// Post format
		$postFormat = get_post_format($post->ID);
		// Get icon class each post format
		$titleIconClass = postFormatIcon($postFormat);
		// Post title
		$post_title =  the_title('', '', false) ? the_title('', '', false) : __('No Title', 'DigiPress');

		// Check the first or last
		$firstPostClass = dp_is_first() ? 'first-post' : '';
		$lastPostClass = dp_is_last() ? 'last-post': '';
		// even of odd
		$evenOddClass = (++$i % 2 === 0) ? 'evenpost' : 'oddpost';

		// ************* SNS sahre number *****************
		// hatebu
		if ( isset( $options['hatebu_number_after_title_archive'] ) && !empty( $options['hatebu_number_after_title_archive'] ) ) {
			$hatebuNumberCode = '<div class="bg-hatebu icon-hatebu ct-hb"><span class="share-num"></span></div>';
		}

		// Count Facebook Like 
		if ( isset( $options['likes_number_after_title_archive'] ) && !empty( $options['likes_number_after_title_archive'] ) ) {
			$fbLikeCountCode = '<div class="bg-likes icon-facebook ct-fb"><span class="share-num"></span></div>';
		}
		// Count tweets
		if ( isset( $options['tweets_number_after_title_archive'] ) && !empty( $options['tweets_number_after_title_archive'] ) ) {
			$tweetCountCode = '<div class="bg-tweets icon-twitter ct-tw"><span class="share-num"></span></div>';
		}
		/***
		 * Filter hook
		 */
		$sns_insert_content = apply_filters( 'dp_archive_insert_sns_content', get_the_ID() );
		if ($sns_insert_content == get_the_ID() || !is_string($sns_insert_content)) {
			$sns_insert_content = '';
		}
		// Whole share code
		$sns_share_code = ( ( isset( $options['hatebu_number_after_title_archive'] ) && !empty( $options['hatebu_number_after_title_archive'] ) ) || ( isset( $options['tweets_number_after_title_archive'] ) && !empty( $options['tweets_number_after_title_archive'] ) ) || ( isset( $options['likes_number_after_title_archive'] ) && !empty( $options['likes_number_after_title_archive'] ) ) || !empty($sns_insert_content) ) ? '<div class="loop-share-num ct-shares" data-url="'.get_permalink().'">'.$hatebuNumberCode.$tweetCountCode.$fbLikeCountCode.$sns_insert_content.'</div>' : '';
		// ************* SNS sahre number *****************

		// Post views
		$postViewsCode = ( isset( $options['show_views_on_meta'] ) && !empty( $options['show_views_on_meta'] ) && function_exists('dp_get_post_views') ) ? '<span class="icon-eye ft10px mg2px-l">'.dp_get_post_views(get_the_ID(), null).'</span>' : '';

		// Category
		$categoryCode = '';
		if ( isset( $options['show_cat_on_meta'] ) && !empty( $options['show_cat_on_meta'] ) ) {
			$cat = get_the_category();
			if ($cat[0]->cat_name) {
				$categoryCode = '<span class="plane-label">' . $cat[0]->cat_name . '</span>';
			}
		}

		/**
		 * Infdeed ads
		 */
		if ($infeed_ads_flg){
			if (is_array($infeed_ads_order)) {
				foreach ($infeed_ads_order as $ads_num) {
					if ($ads_num == dp_get_loop_number()) {
						echo '<div class="loop-article post_excerpt mobile">'.$infeed_ads_code.'</div>';
					}
				}
			} else if ($post_num == $infeed_ads_order) {
				echo '<div class="loop-article post_excerpt mobile">'.$infeed_ads_code.'</div>';
			}
		}
// Post shows ?>
<article id="post-<?php the_ID(); ?>" class="loop-article post_excerpt <?php echo $evenOddClass . ' ' . $firstPostClass . ' ' . $lastPostClass . $mobile_class; ?>"><div class="post_in_box clearfix">
<div class="widget-post-thumb"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php 
	// Get thumbnail
	echo show_post_thumbnail($arg_thumb);
?></a></div>
<div class="excerpt_div">
<header>
<?php 
// meta info
if ( ( isset( $options['show_pubdate_on_meta'] ) && !empty( $options['show_pubdate_on_meta'] ) ) || ( isset( $options['show_cat_on_meta'] ) && !empty( $options['show_cat_on_meta'] ) ) ) : ?>
<div>
<?php
	if ( isset( $options['show_pubdate_on_meta'] ) && !empty( $options['show_pubdate_on_meta'] ) ) : ?>
<time datetime="<?php the_time('c'); ?>" class="icon-calendar updated"><?php echo get_the_date(); ?></time>
<?php endif; ?>
<?php echo $categoryCode; ?>
</div>
<?php
endif;
?>
<div class="excerpt_title_div">
<h1 class="entry-title excerpt_title<?php echo $titleIconClass; ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
<?php
	if ($postFormat === 'quote'): // Check the post format 
		_e('Quote', 'DigiPress');
	else :
		// Display Hatebu number
		echo $post_title;
	endif;
?>
</a></h1>
<?php echo $sns_share_code . $postViewsCode; ?>
</div>
</header>
</div>
</div>
</article>
<?php endwhile; 
	echo '</div>';	// End of <div class="posts-normal">
?>

<?php 
//If top posts show as gallery view
else : 
?>
<section id="post-table-section" class="clearfix">
<div id="gallery-style">
	<?php
	//For thumbnail size
	$width = 600;
	$height = 440;

	//Loop each post
	while (have_posts()) : the_post();

		// Post format
		$postFormat = get_post_format($post->ID);

		// Get icon class each post format
		$titleIconClass = postFormatIcon($postFormat);
		$titleIconClass = $titleIconClass == '' ? ' class="entry-title"' : ' class="entry-title '.$titleIconClass.'"' ;

		// Check the last
		$lastPostClass = dp_is_last() ? 'last-post': '';
		// even of odd
		$evenOddClass = (++$i % 2 === 0) ? 'evenpost gh' : 'oddpost gh';

		// Post title
		$post_title =  the_title('', '', false) ? the_title('', '', false) : __('No Title', 'DigiPress');


		// ************* SNS sahre number *****************
		// hatebu
		if ($options['hatebu_number_after_title_archive']) {
			$hatebuNumberCode = '<div class="bg-hatebu icon-hatebu ct-hb"><span class="share-num"></span></div>';
		}

		// Count Facebook Like 
		if ($options['likes_number_after_title_archive']) {
			$fbLikeCountCode = '<div class="bg-likes icon-facebook ct-fb"><span class="share-num"></span></div>';
		}
		// Count tweets
		if ($options['tweets_number_after_title_archive']) {
			$tweetCountCode = '<div class="bg-tweets icon-twitter ct-tw"><span class="share-num"></span></div>';
		}
		/***
		 * Filter hook
		 */
		$sns_insert_content = apply_filters( 'dp_archive_insert_sns_content', get_the_ID() );
		if ($sns_insert_content == get_the_ID() || !is_string($sns_insert_content)) {
			$sns_insert_content = '';
		}
		// Whole share code
		$sns_share_code = ( ( isset( $options['hatebu_number_after_title_archive'] ) && !empty( $options['hatebu_number_after_title_archive'] ) ) || ( isset( $options['tweets_number_after_title_archive'] ) && !empty( $options['tweets_number_after_title_archive'] ) ) || ( isset( $options['likes_number_after_title_archive'] ) && !empty( $options['likes_number_after_title_archive'] ) ) || !empty($sns_insert_content) ) ? '<div class="loop-share-num ct-shares" data-url="'.get_permalink().'">'.$hatebuNumberCode.$tweetCountCode.$fbLikeCountCode.$sns_insert_content.'</div>' : '';

		/**
		 * Infdeed ads
		 */
		if ($infeed_ads_flg){
			if (is_array($infeed_ads_order)) {
				foreach ($infeed_ads_order as $ads_num) {
					if ($ads_num == dp_get_loop_number()) {
						echo '<div class="loop-article g_item mb post">'.$infeed_ads_code.'</div>';
					}
				}
			} else if ($post_num == $infeed_ads_order) {
				echo '<div class="loop-article g_item mb post">'.$infeed_ads_code.'</div>';
			}
		}
?>
<article id="post-<?php the_ID(); ?>" class="loop-article g_item mb post clearfix <?php echo $evenOddClass  . ' ' . $lastPostClass; ?>">
<div class="post_thumb_gallery"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php echo show_post_thumbnail($arg_thumb); ?></a></div>
<header><h1<?php echo $titleIconClass; ?>><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
<?php
		if ($postFormat === 'quote'): // Check the post format 
			_e('Quote', 'DigiPress');
		else :
			echo $post_title;
		endif;
?>
</a></h1></header>
<footer class="tbl_meta">
	<?php //Posted date
	if ( $options['show_pubdate_on_meta'] ) : ?>
<time datetime="<?php the_time('c'); ?>" class="icon-calendar updated"><?php echo get_the_date(); ?></time>
	<?php endif; 
	if ( comments_open() ) : // If comment is open ?>
<span class="icon-comment"><?php comments_popup_link(
							__('No Comment', 'DigiPress'), 
							__('Comment(1)', 'DigiPress'), 
							__('Comments(%)', 'DigiPress')); ?></span>
	<?php endif; ?>
	<?php if ( isset( $options['show_views_on_meta'] ) && !empty( $options['show_views_on_meta'] ) && function_exists('dp_get_post_views')) : ?>
<span class="icon-eye"><?php echo dp_get_post_views(get_the_ID(), null); ?></span>
	<?php endif; ?>
	<?php if ($options['show_author_on_meta']) : ?>
<span class="icon-user"><?php the_author_posts_link(); ?></span>
	<?php endif; ?>
<?php // Edit Post Link(If logged in.)
	edit_post_link(__('Edit', 'DigiPress'), ' | ');

	echo $sns_share_code; ?>
</footer>
</article>
<?php
	endwhile;
?>
</div>
</section>
<?php
endif; // End of $options['top_post_show_type']
?>
<nav class="navigation-mb clearfix">
<?php 
	// Autopager
	if ( isset( $options['autopager_mb'] ) && !empty( $options['autopager_mb'] ) && isset( $options['navigation_text_to_2page_archive'] ) ) : ?>
<div class="nav_to_paged"><?php next_posts_link( $options['navigation_text_to_2page_archive'] ) ?></div>
<?php 
	else: // Normal pagenation ?>
<?php
		// Maximum page number
		$max_page = (int)$wp_query->max_num_pages;
		// Current page number	
		if ( get_query_var('paged') ) {
			$paged = (int)get_query_var('paged');
		} elseif ( get_query_var('page') ) {
			$paged = (int)get_query_var('page');
		} else {
			$paged = 1;
		}

		if ($max_page === 1) {
			if ($paged === 1) { // Only one page ?>
<div><span><?php echo $paged . '/' . $max_page; ?></span></div>
	<?php
			}
		} else if ($max_page > 1){
			if ($paged === 1) { ?>
<div class="navialignleft-mb"><span><?php _e('First page now', 'DigiPress') ?></span></div>
<div class="navialigncenter-mb"><span><?php echo $paged . '/' . $max_page; ?></span></div>
<div class="navialignright-mb"><?php next_posts_link(__('<span>NEXT</span>', '')) ?></div>
	<?php 
			} else if ($paged < $max_page) { ?>
<div class="navialignleft-mb"><?php previous_posts_link(__('<span>PREV</span>', '')) ?></div>
<div class="navialigncenter-mb"><span><?php echo $paged . '/' . $max_page; ?></span></div>
<div class="navialignright-mb"><?php next_posts_link(__('<span>NEXT</span>', '')) ?></div>
		<?php
			} else if ($paged === $max_page) { ?>
<div class="navialignleft-mb"><?php previous_posts_link(__('<span>PREV</span>', '')) ?></div>
<div class="navialigncenter-mb"><span><?php echo $paged . '/' . $max_page; ?></span></div>
<div class="navialignright-mb"><span><?php _e('No more page', 'DigiPress') ?></span></div>
		<?php
			} 
		}
		?>
<?php
	endif; // End of $options['autopager_mb']
?>
</nav>
<?php 
else : // else (have_posts()) 
?>
<article class="post">
<header><h1 class="posttitle"><?php _e('Not found.','DigiPress'); ?></h1></header>
<div class="entry">
<p><?php _e('Apologies, but the page you requested could not be found. <br />Perhaps searching will help.', 'DigiPress'); ?></p>
</div>
</article>
<?php endif; // if (have_posts()) END ?>
<?php endif; // End of if (isset($_REQUEST['q'])) 
// End of content division ?>
</div><?php // End of content ?>
</div><?php // End of container ?>
</div><?php // End of wrap ?>
<?php include_once(TEMPLATEPATH . "/mobile-theme/footer.php"); ?>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</body>
</html>