<?php 
if ( !isset( $options['show_top_content'] ) || $options['show_top_content'] == false ) return;

$views				= '';
$hatebuImgCode		= '';
//$tweetCountCode		= '';
//$fbLikeCountCode	= '';

if (is_home() && !is_paged()) : ?>
<section class="new-entry">
<h1 class="newentrylist"><?php echo $options['new_post_label'];?></h1>
<div id="scrollentrybox">
<ul>
		<?php
		// For thumbnail size
		$width = 180;
		$height = 132;
		$arg_thumb = array('width' => $width, 'height' => $height, "if_img_tag"=> true);

		$feedUrl = get_bloginfo('rss2_url');
		$latest = array();
		
		// Query
		if ( isset( $options['show_specific_cat_index_top'] ) && $options['show_specific_cat_index_top'] === 'cat') {
			if ( isset( $options['index_top_except_cat'] ) && !empty( $options['index_top_except_cat'] ) && isset( $options['index_top_except_cat_id'] ) && isset( $options['new_post_count'] ) ) {
				// Add nimus each category id
				$cat_ids = preg_replace('/(\d+)/', '-${1}', $options['index_top_except_cat_id'] );
				$latest =  get_posts($query_string . '&numberposts=' . $options['new_post_count'] . '&category=' . $cat_ids);
			} else {
				if( isset( $options['specific_cat_index_top'] ) && !empty( $options['specific_cat_index_top'] ) ) {
					$latest =  get_posts($query_string . '&numberposts=' . $options['new_post_count'] . '&category=' . $options['specific_cat_index_top']);
				}
			}

			if( isset( $options['specific_cat_index_top'] ) && !empty( $options['specific_cat_index_top'] ) ) {
				$feedUrl = get_category_feed_link($options['specific_cat_index_top'], 'rss2');
			}
		} else if ( isset( $options['show_specific_cat_index_top'] ) && $options['show_specific_cat_index_top'] === 'custom' && isset( $options['new_post_count'] ) && isset( $options['specific_post_type_index_top'] ) ) {
			$latest =  get_posts($query_string . '&numberposts=' . $options['new_post_count'] . '&post_type=' . $options['specific_post_type_index_top'] );
			$feedUrl .= '?post_type=' . $options['specific_post_type_index_top'];
		} else {
			if ( isset( $options['new_post_count'] ) ) {
				$latest = get_posts($query_string . '&numberposts=' . $options['new_post_count']);
			}
		}
		
		foreach( $latest as $post ): setup_postdata($post);
			if (!get_post_meta(get_the_ID(), 'hide_in_index', true)) :
			// Count Hatena
			$hatebuImgCode =  isset( $options['show_hatebu_number']) && !empty( $options['show_hatebu_number'] ) ? '<img src="http://b.hatena.ne.jp/entry/image/' . get_permalink() . '" alt="' . __('Bookmark number in hatena', 'DigiPress') . the_title(' - ', '', false) . '" class="hatebunumber" />' : '';?>
<li class="clearfix"><?php
	// Views
	if ( isset( $options['show_views_on_meta'] ) && !empty( $options['show_views_on_meta'] ) && function_exists('dp_get_post_views') && isset( $options['show_specific_cat_index_top'] ) && !$options['show_specific_cat_index_top'] === 'custom') {
		$views = '<span class="icon-eye ft11px">'.dp_get_post_views(get_the_ID(), null).'</span>';
	}
	// Thumbnail
	if ( isset( $options['show_thumbnail'] ) && !empty( $options['show_thumbnail'] ) ) {
		echo '<div class="widget-post-thumb"><a href="'.get_permalink().'" title="'.get_the_title().'">';
		// Get thumbnail
		echo show_post_thumbnail($arg_thumb);
		echo '</a></div>';
	}
	
	// Date
	if ( isset( $options['show_pubdate'] ) && !empty( $options['show_pubdate'] ) ) echo '<span class="entrylist-date">'.get_the_date().'</span>';
	// Category
	if ( ( isset( $options['show_specific_cat_index_top'] ) && $options['show_specific_cat_index_top'] !== 'custom' ) && ( isset( $options['show_cat_entrylist'] ) && !empty( $options['show_cat_entrylist'] ) ) ) :?>
<div class="entrylist-cat"><?php the_category(' '); ?></div><?php
	endif;?>
<a href="<?php the_permalink(); ?>" class="entrylist-title" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a> <?php echo $hatebuImgCode; ?> <?php if ( comments_open() && isset( $options['show_comment_num_index'] ) && !empty( $options['show_comment_num_index'] ) ) : ?><span class="icon-comment ft11px reverse-link"><?php comments_popup_link(
				__('No Comment', 'DigiPress'), 
				__('Comment(1)', 'DigiPress'), 
				__('Comments(%)', 'DigiPress')); ?></span><?php endif; ?> <?php echo $views; ?></li>
		<?php
		endif;
		endforeach;
		// Reset Query
		wp_reset_postdata();
		?>
</ul>
</div>
<a href="<?php echo $feedUrl; ?>" title="RSS of this list" class="show-this-rss icon-rss"><span>RSS</span></a>
</section>
<?php endif; ?>