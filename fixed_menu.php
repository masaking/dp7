<nav id="fixed_menu"><?php
function show_wp_fixed_menu_list() { ?>
<ul id="fixed_menu_ul">
<li <?php if (is_home()) { echo 'class="current_page_item"'; } ?>><a href="<?php echo home_url(); ?>" title="HOME" class="icon-home"><?php _e('HOME','DigiPress'); ?></a></li>
<li><a href="<?php echo get_feed_link(); ?>" target="_blank" title="feed" class="icon-rss">RSS</a></li>
</ul><?php
} //End Function

// Custom Menu
if (function_exists('wp_nav_menu')) {
	wp_nav_menu(array(
		'theme_location'	=> 'Fixed Menu',
		'container'			=> '',
		'menu_id'			=> 'fixed_menu_ul',
		'fallback_cb'		=> 'show_wp_fixed_menu_list',
		'walker'			=> new description_walker()
	));

} else {
	// Fixed Page List
	show_wp_fixed_menu_list();
}

echo '<div id="fixed_sform"><div class="hd_searchform">';
if ( isset( $options['show_fixed_menu_search'] ) && !empty( $options['show_fixed_menu_search'] ) ) {
	if ($options['show_floating_gcs']) {
		// Google Custom Search
		echo '<gcse:searchbox-only></gcse:searchbox-only>';
	} else {
		// Default search form
		get_search_form();
	}
}
if ( isset( $options['show_fixed_menu_sns'] ) && !empty( $options['show_fixed_menu_sns'] ) ) {
	$sns_link_code = isset( $options['fixed_menu_fb_url'] ) && !empty( $options['fixed_menu_fb_url'] ) ? '<li><a href="' . $options['fixed_menu_fb_url'] . '" title="Share on Facebook" target="_blank" class="icon-facebook"><span>Facebook</span></a></li>' : '';

	$sns_link_code .= isset( $options['fixed_menu_twitter_url'] ) && !empty( $options['fixed_menu_twitter_url'] ) ? '<li><a href="' . $options['fixed_menu_twitter_url'] . '" title="Follow on Twitter" target="_blank" class="icon-twitter"><span>Twitter</span></a></li>' : '';


	$sns_link_code .= isset( $options['rss_to_feedly'] ) && !empty( $options['rss_to_feedly'] ) ? '<li><a href="https://feedly.com/i/subscription/feed/'.urlencode(get_feed_link()).'" target="_blank" title="Follow on feedly" class="icon-feedly"><span>Follow on feedly</span></a></li>' : '<li><a href="'. get_feed_link() .'" title="Subscribe Feed" target="_blank" class="icon-rss"><span>RSS</span></a></li>' ;

	if (!empty($sns_link_code)) echo '<div id="fixed_sns"><ul>'.$sns_link_code.'</ul></div>';
}
echo '</div></div>';?>
<div id="expand_float_menu" class="icon-menu"><span>Menu</span></div></nav>