<?php
/**
 * DigiPress functions and definitions
 *
 * @package DigiPress
 */

$dp_theme_upload_dir = wp_upload_dir();
$upload_url = is_ssl() ? str_replace('http:', 'https:', $dp_theme_upload_dir['baseurl']) : $dp_theme_upload_dir['baseurl'];
$theme_url = is_ssl() ? str_replace('http:', 'https:', get_template_directory_uri()) : get_template_directory_uri();

//Version
define ('DP_OPTION_SPT_VERSION', '1.6.0.8');
//Base theme name
define('DP_THEME_NAME', "el plano");
//Base theme key
define('DP_THEME_KEY', "el-plano");
// Theme Slug
define ('DP_THEME_SLUG', 'dp_elplano');
//Theme ID
define('DP_THEME_ID', "DigiPress");
//Theme URI
define('DIGIPRESS_URI', "https://digipress.info/");
//Author URI
define('DP_AUTHOR_URI', "https://www.digistate.co.jp/");
//Theme Directory
define('DP_THEME_DIR', dirname(__FILE__));
//Theme Directory
define('DP_THEME_URI', $theme_url);
//Theme Directory for mobile
define('DP_MOBILE_THEME_DIR', 'mobile-theme');
//Column Type(1, 2, 3)
define('DP_COLUMN', '2');
// Theme Type
define('DP_BUTTON_STYLE', 'flat');
//Original upload dir
define('DP_UPLOAD_DIR', $dp_theme_upload_dir['basedir'].'/digipress/'.DP_THEME_KEY);
//Original upload path
define('DP_UPLOAD_URI', $upload_url.'/digipress/'.DP_THEME_KEY);
// This is for base embeded content width(image, video...)
define('DP_CONTENT_WIDTH', 630);
if ( !isset( $content_width ) ) $content_width = DP_CONTENT_WIDTH;

/**
 * Load text domain
 */
add_action( 'after_setup_theme', 'dp_theme_after_setup_theme' );
function dp_theme_after_setup_theme() {
	global $wp_version;
	if ( version_compare( $wp_version, '6.7', '<' ) ) {
		load_theme_textdomain( 'DigiPress', get_template_directory() . '/languages' );
	} else {
		load_textdomain( 'DigiPress', get_template_directory() . '/languages/' . determine_locale() . '.mo' );
	}
}

/****************************************************************
* Load theme options/global
****************************************************************/
$options = get_option('dp_options');
$options_visual = get_option('dp_options_visual');


/****************************************************************
* Include Main Class
****************************************************************/
include_once(DP_THEME_DIR . "/inc/scr/theme_main_class.php");

/**
 * Load theme updater functions.
 * Action is used so that child themes can easily disable.
 */
function dp_prefix_theme_updater() {
	require( DP_THEME_DIR . '/inc/scr/updater/theme-updater.php' );
}
add_action( 'after_setup_theme', 'dp_prefix_theme_updater' );

/****************************************************************
* Include Function
****************************************************************/
include_once(DP_THEME_DIR . "/inc/admin/visual_params.php");
include_once(DP_THEME_DIR . "/inc/admin/control_params.php");
include_once(DP_THEME_DIR . "/inc/scr/permission_check.php");
include_once(DP_THEME_DIR . "/inc/scr/get_uploaded_images.php");
include_once(DP_THEME_DIR . "/inc/scr/admin_menu_control.php");
include_once(DP_THEME_DIR . "/inc/scr/create_css.php");
include_once(DP_THEME_DIR . "/inc/scr/create_title_h1.php");
include_once(DP_THEME_DIR . "/inc/scr/create_title_h2.php");
include_once(DP_THEME_DIR . "/inc/scr/create_meta.php");
include_once(DP_THEME_DIR . "/inc/scr/show_banner_contents.php");
include_once(DP_THEME_DIR . '/inc/scr/show_post_thumbnail.php');
include_once(DP_THEME_DIR . "/inc/scr/show_sns_icon.php");
include_once(DP_THEME_DIR . "/inc/scr/is_mobile_dp.php");
include_once(DP_THEME_DIR . "/inc/scr/create_desc.php");
include_once(DP_THEME_DIR . "/inc/scr/breadcrumb.php");
include_once(DP_THEME_DIR . "/inc/scr/headline.php");
include_once(DP_THEME_DIR . "/inc/scr/autopager.php");
require_once(DP_THEME_DIR . "/inc/scr/widgets.php");
include_once(DP_THEME_DIR . "/inc/scr/custom_field.php");
include_once(DP_THEME_DIR . "/inc/scr/custom_menu.php");
include_once(DP_THEME_DIR . "/inc/scr/placeholder.php");
include_once(DP_THEME_DIR . "/inc/scr/get_column_num.php");
include_once(DP_THEME_DIR . "/inc/scr/gallery_shortcode.php");
require_once(DP_THEME_DIR . "/inc/scr/shortcodes.php");
include_once(DP_THEME_DIR . "/inc/scr/pagination.php");
include_once(DP_THEME_DIR . "/inc/scr/disable_auto_format.php");
include_once(DP_THEME_DIR . "/inc/scr/custom_post_type.php");
include_once(DP_THEME_DIR . "/inc/scr/meta_info.php");
include_once(DP_THEME_DIR . "/inc/scr/show_ogp.php");
include_once(DP_THEME_DIR . "/inc/scr/footer_widgets.php");
include_once(DP_THEME_DIR . "/inc/scr/post_views.php");
include_once(DP_THEME_DIR . "/inc/scr/widget_categories.php");
include_once(DP_THEME_DIR . "/inc/scr/widget_tag_cloud.php");
include_once(DP_THEME_DIR . "/inc/scr/json_ld.php");


/****************************************************************
* GLOBALS
****************************************************************/
$EXIST_FB_LIKE_BOX 	= false;
$FB_APP_ID 			= '';
$COLUMN_NUM 		= '';
$IS_MOBILE_DP 		= false;

/****************************************************************
* Set globals before the site is about to showing.
****************************************************************/
add_action('after_setup_theme', 'is_mobile_dp');
add_action('wp', 'get_column_num');


/****************************************************************
* Add Theme Option into wp admin interfaces.
****************************************************************/

//Add option menu into admin panel header and insert CSS and scripts to DigiPress panel.
add_action('admin_menu', array('digipress_options', 'add_menu'));
add_action('admin_menu', array('digipress_options', 'update'));
add_action('admin_menu', array('digipress_options', 'update_visual'));
add_action('admin_menu', array('digipress_options', 'dp_run_upload_file'));
add_action('admin_menu', array('digipress_options', 'dp_delete_upload_file'));
add_action('admin_menu', array('digipress_options', 'edit_images'));
add_action('admin_menu', array('digipress_options', 'reset_theme_options'));
add_action('admin_menu', array('digipress_options', 'dp_export_all_settings'));
add_action('admin_menu', array('digipress_options', 'dp_import_all_settings'));

/****************************************************************
* Remove Wordpress theme customizer
****************************************************************/
function dp_theme_customize_remove($wp_customize){
	class DP_Customize_Control extends WP_Customize_Control {
		public $note = '';
		protected function render() {
			$id    = 'customize-control-' . str_replace( array( '[', ']' ), array( '-', '' ), $this->id );
			$hierarchy = isset($this->hierarchy) && !empty($this->hierarchy) ? ' sub-'.esc_attr($this->hierarchy) : '';
			$class = 'customize-control customize-control-' . $this->type.$hierarchy;?>
<li id="<?php echo esc_attr( $id ); ?>" class="<?php echo esc_attr( $class ); ?>"><?php echo $this->note;?></li><?php
		}
	}

	$wp_customize->remove_section('header_image');
	$wp_customize->remove_section('background_image');
	$wp_customize->remove_section('static_front_page');
	$wp_customize->remove_section('colors');
	$wp_customize->remove_section('custom_css');

	$wp_customize->add_section(
		'dp_goto_option_section', array(
		'title' => __('Customize Theme', 'DigiPress'),
		'priority' => 1
	));
	$wp_customize->add_setting(
		'dp_goto_option_section', array(
		'transport' => 'postMessage'
	));
	$wp_customize->add_control( new DP_Customize_Control(
		$wp_customize,
		'dp_goto_option_section', array(
		'note' => '<a href="admin.php?page=digipress" class="button">'.__('Customize this theme','DigiPress').'</a>',
		'section' => 'dp_goto_option_section',
		'type' => 'text'
		)
	));
}
add_action('customize_register','dp_theme_customize_remove');

/****************************************************************
* Insert custom field to editing post window.
* from custom_field.php
****************************************************************/
// Add custom fields
add_action('admin_menu', 'add_custom_field');
add_action('save_post', 'save_custom_field');
/* Add CSS into admin panel */
// function add_css_for_admin() {
//    echo '<link rel="stylesheet" type="text/css" href="'.get_template_directory_uri().'/inc/css/dp-admin.css">';
// }
// add_action('admin_head-post.php' , 'add_css_for_admin');
// add_action('admin_head-post-new.php' , 'add_css_for_admin');


/****************************************************************
* After setup theme
****************************************************************/
function dp_after_setup_theme() {
	global $wp_version;
	// ***
	// * Add theme support
	// **
	// Post thumbnail
	add_theme_support('post-thumbnails');
	// Custom menu
	add_theme_support('menus');
	// Feed links
	add_theme_support( 'automatic-feed-links' );
	// Auto title tag (WP4.1 over)
    if ( version_compare( $wp_version, '4.1', '>=' ) ) {
        add_theme_support('title-tag');
    }

	// Password form
	remove_filter( 'the_password_form', 'custom_password_form' );
	add_filter('the_password_form', 'dp_password_form');

	// Theme customizer
	add_theme_support('custom-background');
	add_theme_support('custom-header');
	add_theme_support( 'align-wide' );
	add_theme_support( 'editor-styles' );

	// Disable Gutenberg Based Widget Screen (Temporary)
	remove_theme_support( 'widgets-block-editor' );

	// Post formats
	add_theme_support( 'post-formats', array(
		'aside',
		'gallery',
		'image',
		'link',
		'quote',
		'status',
		'video',
		'audio',
		'chat'
	) );

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form',
		// 'gallery',
		'caption'
	) );
}
add_action( 'after_setup_theme', 'dp_after_setup_theme' );

/****************************************************************
* Replace title tag
****************************************************************/
function dp_replace_wp_title( $title ) {
    if ( ! class_exists('All_in_One_SEO_Pack') && ! function_exists( 'aioseo' ) && ! function_exists( 'YoastSEO' ) ) {
	    $title = dp_site_title( null, null, false );
	}
    return $title;
}
add_filter( 'pre_get_document_title', 'dp_replace_wp_title' );


/****************************************************************
* Last Modified into HTTP Headers
****************************************************************/
function dp_get_last_modified_date(){
	$date = array(
		get_the_modified_time("Y"),
		get_the_modified_time("m"),
		get_the_modified_time("d")
	);
	$time = array(
		get_the_modified_time("H"),
		get_the_modified_time("i"),
		get_the_modified_time("s"),
	);
	$time_str = implode("-", $date)."T".implode(":", $time);
	return date_i18n( "r", strtotime($time_str) );
}

function dp_http_header_set(){
	if (is_singular()){
		$mod_time = get_the_modified_time('U');
		$last_mod = gmdate("D, d M Y H:i:s T", $mod_time);
		$etag = md5($last_mod.get_permalink());
		header(sprintf("Last-Modified: %s", $last_mod));
		header(sprintf("Etag: %s", $etag));
		// $if_modified_since = filter_input(INPUT_SERVER, 'HTTP_IF_MODIFIED_SINCE');
		// $if_none_match = filter_input(INPUT_SERVER, 'HTTP_IF_NONE_MATCH');
		// if ( $if_modified_since === $last_mod || $if_none_match === $etag ) {
		// 	header( 'HTTP', true, 304 );
		// 	exit;
		// }
	}
}
add_action( "wp", "dp_http_header_set", 1 );


/****************************************************************
* Action hook on wp_head
****************************************************************/
function dp_hook_wp_head(){
	global $options;

	// SNS
	if ( isset($options['disable_dns_prefetch']) && !empty($options['disable_dns_prefetch']) ) {
		remove_action('wp_head','wp_resource_hints', 2);
	} else {
		if ( !(isset($options['disable_sns_share_count']) && !empty($options['disable_sns_share_count'])) ){
			$prefetch = '<link rel="dns-prefetch" href="//connect.facebook.net" />
<link rel="dns-prefetch" href="//secure.gravatar.com" />
<link rel="dns-prefetch" href="//api.pinterest.com" />
<link rel="dns-prefetch" href="//jsoon.digitiminimi.com" />
<link rel="dns-prefetch" href="//b.hatena.ne.jp" />
<link rel="dns-prefetch" href="//platform.twitter.com" />';
			echo str_replace(array("\r\n","\r","\n","\t","/^\s(?=\s)/"), '', $prefetch);
		}
	}

	if (is_singular()){
		// Modified date
		printf( '<meta http-equiv="Last-Modified" content="%s" />', dp_get_last_modified_date() );
		// Ping URL
		if ( pings_open() ) {
			printf( '<link rel="pingback" href="%s" />', get_bloginfo( 'pingback_url' ) );
		}
	}
}
add_action( "wp_head", "dp_hook_wp_head", 1 );

/****************************************************************
* Admin function
****************************************************************/
/* Disable admin bar */
// add_filter('show_admin_bar', '__return_false');
// /* Disable admin notice for editors */
if (!current_user_can('edit_users')) {
	function dp_wphidenag() {
		remove_action( 'admin_notices', 'update_nag');
	}
	add_action('admin_menu','dp_wphidenag');
}


/****************************************************************
* Replace upload content url in SSL
****************************************************************/
function dp_replace_ssl_content($content){
	if(is_ssl()){
		$upload_dir = wp_upload_dir();
		$upload_dir_url = $upload_dir['baseurl'];
		$upload_dir_ssl_url = str_replace('http:', 'https:', $upload_dir_url);
		$content = str_replace($upload_dir_url, $upload_dir_ssl_url, $content);
	}
	return $content;
}
add_filter('the_content', 'dp_replace_ssl_content');

/****************************************************************
* Insert Ads in single post content
****************************************************************/
define('DP_H_TAG_REG', '/<h[1-6].*?>/i');
function dp_get_h_tag_position_in_content( $the_content ){
	if ( preg_match( DP_H_TAG_REG, $the_content, $h_tags )) {
		return $h_tags[0];
	}
}
function dp_insert_widget_before_first_h_tag($the_content) {
	global $IS_MOBILE_DP;
	$middle_content = '';
	if (is_singular()){
		if (!$IS_MOBILE_DP) {
			ob_start();
			dynamic_sidebar('widget-post-middle');
			$middle_content = ob_get_contents();
			ob_end_clean();
		} else {
			ob_start();
			dynamic_sidebar('widget-post-middle-mb');
			$middle_content = ob_get_contents();
			ob_end_clean();
		}
	}
	$h_tag_position = dp_get_h_tag_position_in_content( $the_content );
	if ( $h_tag_position ) {
		$the_content = preg_replace(DP_H_TAG_REG, $middle_content.$h_tag_position, $the_content, 1);
	}
	return $the_content;
}
add_filter('the_content','dp_insert_widget_before_first_h_tag', 12);


/****************************************************************
* Avoid SSL at home url
****************************************************************/
function dp_ssl_home_url($url, $path = '', $orig_scheme = 'http'){
	if(is_ssl() && strpos($path, 'wp-content') === false){
		$url = str_replace('https:', 'http:', $url);
	}
	return $url;
}
// add_filter('home_url', 'dp_ssl_home_url');


/****************************************************************
* Disable self pinback
****************************************************************/
function dp_no_self_ping( &$links ) {
	$home = home_url();
	foreach ( $links as $l => $link )
	if ( 0 === strpos( $link, $home ) )
		unset($links[$l]);
}
add_action( 'pre_ping', 'dp_no_self_ping' );

/* Enable excerpt for single page */
add_post_type_support( 'page', 'excerpt' );


/****************************************************************
* Disable meta canonical
****************************************************************/
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head');
remove_action('wp_head', 'rel_canonical');

/****************************************************************
* Disable oEmbed
****************************************************************/
if($options['disable_oembed']) {
	add_filter('embed_oembed_discover', '__return_false');
	remove_action('wp_head','rest_output_link_wp_head');
	remove_action('wp_head','wp_oembed_add_discovery_links');
	remove_action('wp_head','wp_oembed_add_host_js');
}

/****************************************************************
* For check the order of curret post
****************************************************************/
function dp_is_first(){
	global $wp_query;
	return ((int)$wp_query->current_post === 0) ? true : false;
}
function dp_is_last(){
	global $wp_query;
	return ((int)$wp_query->current_post + 1 === $wp_query->post_count) ? true : false;;
}
function dp_is_odd(){
	global $wp_query;
	return (((int)$wp_query->current_post + 1) % 2 === 1) ? true : false;;
}
function dp_is_even(){
	global $wp_query;
	return (((int)$wp_query->current_post + 1) % 2 === 0) ? true : false;;
}



/****************************************************************
* Apply mobile theme
****************************************************************/
function dp_mobile_template_include( $template ) {
	global $IS_MOBILE_DP;
	// Mobile theme directory name
	if ( $IS_MOBILE_DP ) {
		$template_file = basename($template);
		$template_mb = str_replace( $template_file, DP_MOBILE_THEME_DIR.'/'.$template_file, $template );
		// If exist the mobile template, replace them.
		if ( file_exists( $template_mb ) )
			$template = $template_mb;
	}
	return $template;
}
if (!$options['disable_mobile_fast']) {
	add_filter( 'template_include', 'dp_mobile_template_include' );
}


/****************************************************************
* Enable PHP in widgets
****************************************************************/
add_filter('widget_text','dp_execute_php_in_widget',100);
add_filter('dp_widget_text','dp_execute_php_in_widget',100);
function dp_execute_php_in_widget($html){
	global $options;
	if(strpos($html,"<"."?php")!==false && $options['execute_php_in_widget'] ){
		ob_start();
		eval("?".">".$html);
		$html=ob_get_contents();
		ob_end_clean();
	}
	return $html;
}



/**
 * [dp_get_loop_number description]
 * @return int current post number
 */
function dp_get_loop_number(){
    global $wp_query;
    return $wp_query->current_post + 1;
}

/****************************************************************
* Fix original "the_excerpt" function.
****************************************************************/
remove_filter('the_excerpt', 'wpautop'); 
function dp_del_from_excerpt($str){
	$str = preg_replace("/(\r|\n|\r\n)/m", " ", $str);
	$str = preg_replace("/　/", "", $str); //del multibyte space
	$str = preg_replace("/\t/", "", $str); //del tab
	$str = preg_replace("/(<br>)+/", " ", $str);
	$str = preg_replace("/^(<br \/>)/", "", $str);
	return '<p>' . $str . '</p>';
}
add_filter('the_excerpt', 'dp_del_from_excerpt');

/****************************************************************
* Replace "more [...]" strings.
****************************************************************/
//WordPress version as integer.
function dp_new_excerpt_more($more) {
	$str_more = '...';
		return $str_more;
}
//Replace "more" strings.
add_filter('excerpt_more', 'dp_new_excerpt_more');


/****************************************************************
* Change excerpt length.
****************************************************************/
function dp_new_excerpt_mblength($length) {
	return 220;
}
add_filter('excerpt_mblength', 'dp_new_excerpt_mblength');


/****************************************************************
* Remove more "#" link string.
****************************************************************/
function dp_custom_content_more_link( $output ) {
	$output = preg_replace('/#more-[\d]+/i', '', $output );
	return $output;
}
add_filter( 'the_content_more_link', 'dp_custom_content_more_link' );


/****************************************************************
* Remopve protection text and custome protected form
****************************************************************/
function remove_private($s) {
	return '%s';
}
add_filter('protected_title_format', 'remove_private');

function dp_password_form() {
	$custom_phrase = 
'<p class="need-pass-title label label-orange icon-lock">'.__('Protected','DigiPress').'</p>'.__('Please type the password to read this page.', 'DigiPress').'
<div id="protectedForm"><form action="' .site_url(). '/wp-login.php?action=postpass" method="post"><input name="post_password" type="password" size="24" /><input type="submit" name="Submit" value="' . esc_attr__("Submit") . '" />
</form></div>';

return $custom_phrase;
}



/****************************************************************
* Insert post thumbnail in Feeds.
****************************************************************/
// function post_thumbnail_in_feeds($content) {
// 	global $post;
// 	if(has_post_thumbnail($post->ID)) {
// 		$content = '<div>' . get_the_post_thumbnail($post->ID) . '</div>' . $content;
// 	}
// 	return $content;
// }
// add_filter('the_excerpt_rss', 'post_thumbnail_in_feeds');
// add_filter('the_content_feed', 'post_thumbnail_in_feeds');


/****************************************************************
* Replace post slug when unexpected character.
****************************************************************/
function auto_post_slug( $slug, $post_ID, $post_status, $post_type ) {
	global $options;
	if (!(bool)$options['disable_fix_post_slug']) {
		if ( preg_match( '/(%[0-9a-f]{2})+/', $slug ) ) {
			$slug = utf8_uri_encode( $post_type ) . '-' . $post_ID;
		}
	}
	return $slug;
}
add_filter( 'wp_unique_post_slug', 'auto_post_slug', 10, 4 );

/****************************************************************
* Disable wp emoji
****************************************************************/
function dp_disable_emoji() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );    
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );    
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
}
if ( isset( $options['disable_emoji'] ) && !empty( $options['disable_emoji'] ) ) {
	add_action('init', 'dp_disable_emoji');
	add_filter( 'emoji_svg_url', '__return_false' );
}

/**
 * Disable attachment page and redirect
 */
function dp_redirect_attachment_page() {
	if ( is_attachment() ) {
		global $post;
		if ( $post && $post->post_parent ) {
			wp_redirect( esc_url( get_permalink( $post->post_parent ) ), 301 );
			exit;
		} else {
			wp_redirect( esc_url( home_url( '/' ) ), 301 );
			exit;
		}
	}
}
add_action( 'template_redirect', 'dp_redirect_attachment_page' );
/****************************
 * HEX to RGB
 ***************************/
function dp_hex_to_rgb($color) {
	$color = preg_replace("/^#/", '', $color);
	if (mb_strlen($color) == 3) $color .= $color;
	$rgb = array();
	for($i = 0; $i < 6; $i+=2) {
		$hex = substr($color, $i, 2);
		$rgb[] = hexdec($hex);
	}
	return $rgb;
}
/****************************************************************
* Make darken or lighten color from hex to rgb
****************************************************************/
function dp_darken_color($color, $range = 30) {
	if (!is_numeric($range)) $range = 30;
	if ($range > 255 || $range < 0) $range = 30;
	$color = preg_replace("/^#/", '', $color);
	if (mb_strlen($color) == 3) $color .= $color;
	$rgb = array();
	for($i = 0; $i < 6; $i+=2) {
		$hex = substr($color, $i, 2);
		$hex = hexdec($hex);
		$hex = $hex > $range ? $hex - $range : $hex;
		$rgb[] = $hex;
	}
	return $rgb;
}
function dp_lighten_color($color, $range = 30) {
	if (!is_numeric($range)) $range = 30;
	if ($range > 255 || $range < 0) $range = 30;
	$color = preg_replace("/^#/", '', $color);
	if (mb_strlen($color) == 3) $color .= $color;
	$rgb = array();
	for($i = 0; $i < 6; $i+=2) {
		$hex = substr($color, $i, 2);
		$hex = hexdec($hex);
		$hex = $hex + $range <= 255 ? $hex + $range : $hex;
		$rgb[] = $hex;
	}
	return $rgb;
}
/****************************************************************
* Use search.php if the search word is not set.
****************************************************************/
function enable_empty_query( $search, $query ) {
	global $wpdb, $options;

	if ($query->is_main_query()) {
		// "q" is for Google Custom Search
		if ( (isset( $_REQUEST['s'] ) && empty( $_REQUEST['s'])) || (isset( $_REQUEST['q']) && isset( $options['gcs_id'] ) && !empty($options['gcs_id']) ) ) {
			$term = $_REQUEST['s'];
			$query->is_search = true;
			if ( $term === '' ) {
				$search = ' AND 0';
			} else {
				$search = " AND ( ( $wpdb->posts.post_title LIKE '%{$term}%' ) OR ( $wpdb->posts.post_content LIKE '%{$term}%' ) )";
			}
		}
	}
	return $search;
}
if (!is_admin()) {
	add_action( 'posts_search', 'enable_empty_query', 10, 2);
}

/****************************************************************
* Disable hentry class
****************************************************************/
function dp_remove_hentry( $classes ) {
	$classes = array_diff($classes, array('hentry'));
	return $classes;
}
add_filter('post_class', 'dp_remove_hentry');

/**
 * Converts pre tag contents to HTML entities
 */
function dp_pre_content_filter( $content ) {
	return preg_replace_callback(
		'|<pre(?!.*class="wp-block-.*").*>(.*)<\/pre>|isU',
		function( $matches ) {
			return str_replace( $matches[1], str_replace( array('[', ']', '&amp;lt;', '&amp;gt;', '&lt;br /&gt;'), array('&#91;', '&#93;', '&lt;', '&gt;', '<br />'), htmlentities( $matches[1] ) ), $matches[0] );
		}, $content );
}
add_filter( 'the_content', 'dp_pre_content_filter', 1 );


/****************************************************************
 * Modifies WordPress's built-in comments_popup_link() function to return a string instead of echo comment results
 ***************************************************************/
function get_comments_popup_link( $zero = false, $one = false, $more = false, $css_class = '', $none = false ) {
    global $wpcommentspopupfile, $wpcommentsjavascript;
 
    $id = get_the_ID();
 
    if ( false === $zero ) $zero = __( 'No Comments','DigiPress' );
    if ( false === $one ) $one = __( 'Comment(1)','DigiPress' );
    if ( false === $more ) $more = __( 'Comments(%)','DigiPress' );
    if ( false === $none ) $none = __( 'Comments Off','DigiPress' );
 
    $number = get_comments_number( $id );
 
    $str = '';
 
    if ( 0 == $number && !comments_open() && !pings_open() ) {
        $str = '<span' . ((!empty($css_class)) ? ' class="' . esc_attr( $css_class ) . '"' : '') . '>' . $none . '</span>';
        return $str;
    }
 
    if ( post_password_required() ) {
        $str = __('Enter your password to view comments.','DigiPress');
        return $str;
    }
 
    $str = '<a href="';
    if ( $wpcommentsjavascript ) {
        if ( empty( $wpcommentspopupfile ) )
            $home = home_url();
        else
            $home = home_url();
        $str .= $home . '/' . $wpcommentspopupfile . '?comments_popup=' . $id;
        $str .= '" onclick="wpopen(this.href); return false"';
    } else { // if comments_popup_script() is not in the template, display simple comment link
        if ( 0 == $number )
            $str .= get_permalink() . '#respond';
        else
            $str .= get_comments_link();
        $str .= '"';
    }
 
    if ( !empty( $css_class ) ) {
        $str .= ' class="'.$css_class.'" ';
    }
    $title = the_title_attribute( array('echo' => 0 ) );
 
    $str .= apply_filters( 'comments_popup_link_attributes', '' );
 
    $str .= ' title="' . esc_attr( sprintf( __('Comment on %s','DigiPress'), $title ) ) . '">';
    $str .= get_comments_number_str( $zero, $one, $more );
    $str .= '</a>';
     
    return $str;
}
/**
 * Modifies WordPress's built-in comments_number() function to return string instead of echo
 */
function get_comments_number_str( $zero = false, $one = false, $more = false, $deprecated = '' ) {
    if ( !empty( $deprecated ) )
        _deprecated_argument( __FUNCTION__, '1.3' );
 
    $number = get_comments_number();
 
    if ( $number > 1 )
        $output = str_replace('%', number_format_i18n($number), ( false === $more ) ? __('Comments(%)', 'DigiPress') : $more);
    elseif ( $number == 0 )
        $output = ( false === $zero ) ? __('No Comments', 'DigiPress') : $zero;
    else // must be one
        $output = ( false === $one ) ? __('Comment(1)', 'DigiPress') : $one;
 
    return apply_filters('comments_number', $output, $number);
}

/**
 * Remove unnecessary recent comment inline style
 */
function dp_remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action( 'wp_head', array( $wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style' ) );
}
add_action( 'widgets_init', 'dp_remove_recent_comments_style', 1 );


/**
 * Disable Gutenberg Based Widget Screen
 */
function dp_disable_block_based_widget(){
	global $options;

	if ( isset($options['disable_block_based_widget']) && !empty($options['disable_block_based_widget']) ){
		return false;
	}

	return true;
}
add_filter( 'use_widgets_block_editor', 'dp_disable_block_based_widget' );


/****************************************************************
* Number of post at each archive.
****************************************************************/
function dp_number_posts_per_archive( $query ) {
	if (is_admin()) return;
	global $options, $IS_MOBILE_DP;

	$suffix = '';

	if ( $query->is_main_query() ) {
		// Suffix
		$suffix = $IS_MOBILE_DP ? '_mobile' : '';

		// Get posts
		if ($query->is_home() && $options['number_posts_index'.$suffix]) {
			if ($options['show_specific_cat_index'] === 'cat') {
				$query->set( 'posts_per_page', $options['number_posts_index'.$suffix] );

				// Show specific category's posts
				if ($options['index_bottom_except_cat']) {
					// Add nimus each category id
					$cat_ids = preg_replace('/(\d+)/', '-${1}', $options['index_bottom_except_cat_id']);

					$query->set( 'cat', $cat_ids );

				} else {
					$query->set( 'cat', $options['specific_cat_index'] );
				}

			} else if ($options['show_specific_cat_index'] === 'custom') {
				// Show specific custom post type
				$query->set( 'posts_per_page', $options['number_posts_index'.$suffix] );
				$query->set( 'post_type', $options['specific_post_type_index'] );

			} else {
				$query->set( 'posts_per_page', $options['number_posts_index'.$suffix] );
			}
		}
		else if ($query->is_category() && $options['number_posts_category'.$suffix] ) {
			$query->set( 'posts_per_page', $options['number_posts_category'.$suffix] );
		}
		else if ($query->is_search() && $options['number_posts_search'.$suffix] ) {
			$query->set( 'posts_per_page', $options['number_posts_search'.$suffix] );
		}
		else if ($query->is_tag() && $options['number_posts_tag'.$suffix] ) {
			$query->set( 'posts_per_page', $options['number_posts_tag'.$suffix] );
		}
		else if ($query->is_date() && $options['number_posts_date'.$suffix] ) {
			$query->set( 'posts_per_page', $options['number_posts_date'.$suffix] );
		}
	}
}
add_action( 'pre_get_posts', 'dp_number_posts_per_archive' );


/****************************************************************
* Add functions into outer html for theme.
****************************************************************/
//Remove meta of CMS Version
remove_action('wp_head', 'wp_generator');



/**
* Detects animated GIF from given file pointer resource or filename.
*
* @param resource|string $file File pointer resource or filename
* @return bool
*/
function dp_is_animated_gif($file){
	$fp = null;
	if (is_string($file)) {
		$ct = stream_context_create(
			array(
				'ssl' => array(
					'verify_peer'      => false,
					'verify_peer_name' => false
				)
			)
		);
		$fp = fopen( $file, "rb", false, $ct );
	} else {
		$fp = $file;
		/* Make sure that we are at the beginning of the file */
		fseek($fp, 0);
	}
	if ( $fp === false ) {
		return false;
	} else {
		if ( fread( $fp, 3 ) !== "GIF" ) {
			fclose( $fp );
			return false;
		}
	}
	$frames = 0;
	while (!feof($fp) && $frames < 2) {
		if (fread($fp, 1) === "\x00") {
			/* Some of the animated GIFs do not contain graphic control extension (starts with 21 f9) */
			if (fread($fp, 1) === "\x21" || fread($fp, 2) === "\x21\xf9") {
				$frames++;
			}
		}
	}
	fclose($fp);
	return $frames > 1;
}
function dp_disable_upload_sizes( $sizes, $metadata ) {
	if (ini_get('allow_url_fopen')){
		$uploads = wp_upload_dir();
		$upload_path = $uploads['baseurl'];
		$relative_path = $metadata['file'];
		$file_url = $upload_path . '/' . $relative_path;

		if( dp_is_animated_gif( $file_url ) ) {
			$sizes = array();
		}
	}
	// Return sizes you want to create from image (None if image is gif.)
	return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'dp_disable_upload_sizes', 10, 2);



/****************************************************************
* The time stamp to prevent css and js cache 
****************************************************************/
function ts_ext($filename) {
	$ts = '';
	if (file_exists($filename)) {
		$ts = date('YmdHis', filemtime($filename));
	} else {
		$ts = date('YmdHis'); 
	}
	return $ts;
}


/****************************************************************
* Create Uinique ID
****************************************************************/
function dp_rand($sha1 = false) {
	$str_rand = (bool)$sha1 ? sha1(uniqid(mt_rand())) : uniqid(mt_rand(100,500));
	return $str_rand;
}
/**
 * Get the image width and height
 * @param $iamge_url : image URL
 * @return array(width, height) or false
 */
function dp_get_image_size( $image_url = null, $is_local = true ){
	if ( empty($image_url) ) return;

	if ( preg_match("/^\/\//", $image_url) ){
		$image_url = is_ssl() ? 'https:'.$image_url : 'http:'.$image_url;
	}


	$image_data = null;

	// Local path or URL
	if ( $is_local ) {
		// WP content dir local path
		$wp_content_dir = WP_CONTENT_DIR;
		// WP content dir URL
		$wp_content_url = content_url();

		// Replace to the local path
		$image_url_rep = str_replace( $wp_content_url, $wp_content_dir, $image_url );

		if ( $image_url_rep !== $image_url ) {
			// Get the dimensions
			$image_data = getimagesize( $image_url_rep );
			if ( isset( $image_data ) && is_array( $image_data ) ) {
				// ( width, height )
				$image_data = array( $image_data[0], $image_data[1] );
			}
		}
	}


	// When the image path is URL
	if ( $image_data === null ) {

		$image_data = wp_remote_get( $image_url );

		if ( !is_wp_error( $image_data ) && $image_data['response']['code'] === 200 ) {
			if ( function_exists('getimagesizefromstring') ) {
				$image_data = getimagesizefromstring($image_data['body']);
				if ( isset( $image_data ) && is_array( $image_data ) ) {
					// ( width, height )
					$image_data = array( $image_data[0], $image_data[1] );
				}
			} else {
				$image_data = imagecreatefromstring( $image_data['body'] );
				// ( width, height )
				$image_data = array( imagesx($image_data), imagesy($image_data) );
			}
		}
	}


	if ( $image_data !== null &&  is_array($image_data) ) {
		return $image_data;
	}


	return false;
}


/******************************************************
 For url cache control
******************************************************/
function echo_filedate($filename) {
    if (file_exists($filename)) {
        return date_i18n('YmdHis', filemtime($filename));
    } else {
    	return date_i18n('Ymd');
    }
}

/****************************************************************
* Load jquery
*  -- disable WP default jquery, load Google API minimized script
****************************************************************/
function dp_load_jquery() {
	if (is_admin()) return;

	global $options, $options_visual, $IS_MOBILE_DP;

	// Default CSS
	$css_name = "style.css";
	if ( ( isset( $options['decoration_type'] ) && $options['decoration_type'] === 'bootstrap' ) || ( isset( $options['decoration_type'] ) && !empty( $options['decoration_type'] ) === 'none' ) ) {
		$css_name = "style-bs.css";
	}
	// Bootstrap
	if ( isset( $options['decoration_type'] ) && $options['decoration_type'] === 'bootstrap' ) {
		wp_enqueue_style('bootstrap','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css');
		wp_enqueue_script('bootstrap','https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js',array('jquery'),DP_OPTION_SPT_VERSION,true);
	}

	$css_pc = '/css/'.$css_name;
	$css_mb = '/'.DP_MOBILE_THEME_DIR.'/css/'.$css_name;
	$css_custom = '/css/visual-custom.css';

	if ( $IS_MOBILE_DP ) {
        wp_enqueue_style( 'digipress', DP_THEME_URI.$css_mb, null, echo_filedate(DP_THEME_DIR.$css_mb) );
    } else {
        wp_enqueue_style( 'digipress', DP_THEME_URI.$css_pc, null, echo_filedate(DP_THEME_DIR.$css_pc) );
    }
    // Custom CSS
    if ( file_exists( DP_UPLOAD_DIR.'/css/visual-custom.css') ) {
        wp_enqueue_style( 'dp-visual', DP_UPLOAD_URI.$css_custom, array('digipress'), echo_filedate(DP_UPLOAD_DIR.$css_custom) );
    } else {
        wp_enqueue_style( 'dp-visual', DP_THEME_URI.$css_custom, array('digipress'), echo_filedate(DP_THEME_DIR.$css_custom) );
    }

	wp_deregister_script( 'jquery' );
	if ( isset( $options['use_google_jquery'] ) && !empty( $options['use_google_jquery'] ) ) {
		// Replace to Google API jQuery
		wp_enqueue_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js', array(), false );
	} else {
		wp_enqueue_script( 'jquery', includes_url( '/js/jquery/jquery.min.js' ), array(), NULL );
	}

	// jQuery easing
	wp_enqueue_script('easing', DP_THEME_URI . '/inc/js/jquery/jquery.easing.min.js', array('jquery'),DP_OPTION_SPT_VERSION,true);
	// ImagesLoaded
	wp_enqueue_script('imagesloaded', DP_THEME_URI . '/inc/js/imagesloaded.pkgd.min.js', array('jquery'),DP_OPTION_SPT_VERSION,true);
	// fitVids
	wp_enqueue_script('fitvids', DP_THEME_URI . '/inc/js/jquery/jquery.fitvids.min.js', array('jquery'),DP_OPTION_SPT_VERSION,true);
	// Share count
	wp_enqueue_script('sns-share-count', DP_THEME_URI . '/inc/js/jquery/jquery.sharecount.min.js', array('jquery'),DP_OPTION_SPT_VERSION,true);

	// JS for Portfolio
	if ( $IS_MOBILE_DP ) {
		if ( isset( $options['autopager_mb'] )  && !empty( $options['autopager_mb'] ) && !is_singular()) {
			wp_enqueue_script('autopager', DP_THEME_URI . '/inc/js/jquery/jquery.autopager.min.js', array('jquery'),DP_OPTION_SPT_VERSION,true);
		}

		if (!is_singular()) {
			wp_enqueue_script('dp-masonry', DP_THEME_URI . '/inc/js/jquery/jquery.masonry.min.js', array('jquery', 'imagesloaded'),DP_OPTION_SPT_VERSION,true);
		}
	} else {
		if ( isset( $options['autopager'] ) && !empty( $options['autopager'] ) && !is_singular()) {
			wp_enqueue_script('autopager', DP_THEME_URI . '/inc/js/jquery/jquery.autopager.min.js', array('jquery'),DP_OPTION_SPT_VERSION,true);
		}

		if (!is_singular()) {
			wp_enqueue_script('dp-masonry', DP_THEME_URI . '/inc/js/jquery/jquery.masonry.min.js', array('jquery', 'imagesloaded'),DP_OPTION_SPT_VERSION,true);
		}
	}

	// JS for stretch background header image
	if ( isset( $options_visual['dp_header_content_type'] ) && $options_visual['dp_header_content_type'] == 2 && is_front_page() && !is_paged() ) {
		if ( $IS_MOBILE_DP ) {
			if ( isset( $options['disable_mobile_fast'] ) && !empty( $options['disable_mobile_fast'] ) ) {
				wp_enqueue_script('bgstretcher', DP_THEME_URI . '/inc/js/bgstretcher.min.js', array('jquery'),DP_OPTION_SPT_VERSION,true);
			}
		} else {
			wp_enqueue_script('bgstretcher', DP_THEME_URI . '/inc/js/bgstretcher.min.js', array('jquery'),DP_OPTION_SPT_VERSION,true);
		}
	}

	// Headline
	if ( isset( $options['headline_type'] ) && $options['headline_type'] === '3' && (is_home() && !is_paged()) ) {
		if ($options['headline_slider_fx'] === '1') {
			wp_enqueue_script('glide', DP_THEME_URI . '/inc/js/jquery/jquery.glide.min.js', array('jquery'),DP_OPTION_SPT_VERSION,true);
		} else {
			wp_enqueue_script('liscroll', DP_THEME_URI . '/inc/js/jquery/jquery.liscroll.min.js', array('jquery'),DP_OPTION_SPT_VERSION,true);
		}
	}

	wp_enqueue_script('dp-js', DP_THEME_URI . '/inc/js/theme-import.min.js', array('jquery', 'easing', 'sns-share-count'),DP_OPTION_SPT_VERSION,true);

	// for comment form
	if ( is_singular() && comments_open() && get_option('thread_comments') ) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'dp_load_jquery', 1);
// *************************
// For assyncro scripts
// *************************
function dp_async_scripts($url){
	if ( strpos( $url, '#asyncload') === false ) {
		return $url;
	} else if ( is_admin() ) {
		return str_replace( '#asyncload', '', $url );
	} else {
		return str_replace( '#asyncload', '', $url )."' async='async"; 
	}
}
add_filter('clean_url', 'dp_async_scripts', 11, 1 );


/**
 * Remove type attribute from script tag
 */
add_action( 'template_redirect', function () {
	ob_start(function ($buffer) {
		$buffer = str_replace(array( '<script type="text/javascript"', "<script type='text/javascript'" ), '<script', $buffer);
		return $buffer;
	});
});

/****************************************************************
* Insert css and Javascript to head
****************************************************************/
function dp_add_meta_tags() {
	global $options;
	
	$csspie = '';
	$metaForIE = '';

	//Add elements here that need PIE applied
	if ( isset($options['fast_on_ie']) && !empty( $options['fast_on_ie'] ) ) {
	$csspie = '<!--[if lt IE 9]>
<style media="screen">
body {behavior: url('.trailingslashit(DP_THEME_URI).'/inc/js/csshover.min.htc);}
header#header_area,
header#header_half,
header#header_area_paged,
div#site_title,
div#site_banner_image,
div#site_banner_content,
#container,
.post_thumb,
.post_thumb_portfolio,
.more-link,
.box-c,
.btn {
behavior: url('.trailingslashit(DP_THEME_URI).'/inc/scr/PIE/PIE.php);
}
</style>
<![endif]-->';

$metaForIE = '<script src="' . DP_THEME_URI . '/inc/js/css3-mediaqueries.min.js"></script>';

}
	
	//Remove break line code.
	$meta_for_theme = str_replace(array("\r\n","\r","\n"), '', $csspie);

	//Show into meta
	echo $meta_for_theme;
}
add_action( 'wp_print_scripts', 'dp_add_meta_tags' );


/****************************************************************
* Insert Javascript to the end of html
****************************************************************/
function dp_theme_footer() {
	global $options;

	$textShadowJS = '';
	$trace_code = '';
	
	if ( !$options['fast_on_ie'] ) $textShadowJS = '<script src="'.DP_THEME_URI . '/inc/js/jquery/jquery.textshadow.min.js"></script>';
	
	// Javascript
	$js_footer = 
'<!--[if lt IE 9]>'.
$textShadowJS.
'<script src="'.DP_THEME_URI . '/inc/js/theme-import-ie.min.js"></script>
<![endif]-->';
	//Remove break line code.
	$js_footer = str_replace(array("\r\n","\r","\n"), '', $js_footer);
	
	//Display
	echo $js_footer;
	
	// Access Code
	if ( isset( $options['tracking_code'] ) && !empty( $options['tracking_code'] ) ) {
		$trace_code = "<!-- Tracking Code -->" . $options['tracking_code'] . "<!-- /Tracking Code -->";
	} else {
		return;
	}
	
	//Run only user logged in...
	if ( is_user_logged_in() ) {
		$current_user = wp_get_current_user();
		if ( $current_user->wp_user_level == 10 ) {
			if ($options['no_track_admin']) $trace_code = "<!-- You are logged in as Administrator -->";
		}
	}
	echo $trace_code;
}
add_action('wp_footer', 'dp_theme_footer', 100);
?>