var j$ = jQuery;

j$(function() {
	var wp_inline_edit = inlineEditPost.edit;
 
	inlineEditPost.edit = function( id ) {
		wp_inline_edit.apply( this, arguments );

		var $post_id = 0;
		if ( typeof( id ) == 'object' )
			$post_id = parseInt( this.getId( id ) );

		if ( $post_id > 0 ) {
			var $edit_row = j$( '#edit-' + $post_id );
			var $post_row = j$( '#post-' + $post_id );

			// check box fields
			var $arr_fields = new Array(
				'is_slideshow',
				'is_headline',
				'dp_hide_title',
				'dp_hide_date',
				'dp_hide_author',
				'dp_hide_cat',
				'dp_hide_tag',
				'dp_hide_views',
				'dp_hide_fb_comment',
				'dp_hide_time_for_reading',
				'disable_sidebar',
				'dp_show_eyecatch_force'
				);

			// Each field
			for ( var $prop in $arr_fields) {
				if ($arr_fields.hasOwnProperty($prop)) {
					var $field = $arr_fields[$prop];
					var $val = !! j$('.column-' + $field + '>*', $post_row).attr('checked');
					j$( ':input[name="' + $field + '"]', $edit_row ).attr('checked', $val );
				}
			}
		}
	};
});
