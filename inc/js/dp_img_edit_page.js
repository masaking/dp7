var j$ = jQuery;

// Call every trim image
function onSelectChange(img, selection) {
	if (img.id == 'target_banner_img') {
		j$('#header_x1').val(selection.x1);
		j$('#header_y1').val(selection.y1);
		j$('#header_x2').val(selection.x2);
		j$('#header_y2').val(selection.y2);
		j$('#header_crop_width').val(selection.width);
		j$('#header_crop_height').val(selection.height);
	} else if (img.id == 'target_title_img') {
		j$('#title_x1').val(selection.x1);
		j$('#title_y1').val(selection.y1);
		j$('#title_x2').val(selection.x2);
		j$('#title_y2').val(selection.y2);
		j$('#title_crop_width').val(selection.width);
		j$('#title_crop_height').val(selection.height);
	}
}
function onSelectEnd(img, selection) {
	if (img.id == 'target_banner_img') {
		j$('#header_crop_width').val(selection.width);
		j$('#header_crop_height').val(selection.height);
	} else if (img.id == 'target_title_img') {
		j$('#title_crop_width').val(selection.width);
		j$('#title_crop_height').val(selection.height);
	} 
}

function checkInt(num) {
	return (num.match(/[^0-9]/g) || parseInt(num, 10) + "" != num) ? false : true;
}

j$(document).ready(function () {
	// Image editing
	// Trim image
	j$('img#target_title_img, img#target_banner_img').imgAreaSelect({
		handles: true,
		onSelectChange: onSelectChange,
		onSelectEnd: onSelectEnd
	});

	// Change image
	j$('ul#crop_title_img li, ul#crop_header_img li, ul.thumb li').on('click', function(){

		j$(this).find('input').attr('checked', 'checked');

		// Title image
		if (j$(this).attr('class') == 'list_title_img') {
			// reset
			j$('img#target_title_img').imgAreaSelect({remove:true});
			j$('img#target_title_img').imgAreaSelect({
				handles: true,
				onSelectChange: onSelectChange,
				onSelectEnd: onSelectEnd
			});
			
			img = j$(this).find('img.hiddenImg');
			imgUrl = j$(this).find('img.thumbTitleImg_crop').attr('src');
			fileName = j$(this).find('label').text();

			// If target is for triming image
			j$('img#target_title_img').attr({
				'src': imgUrl,
				'width': img.width(),
				'height': img.height()
			});
			// Change send value
			j$('#crop_title_img').attr({
				'value': imgUrl	// image url
			});
			j$('#crop_title_img_file_name').attr({
				'value': fileName	// image file name
			});
			
			j$("input#title_resize_val_px").val(img.width());
		}
		
		// Header image
		if (j$(this).attr('class') == 'list_header_img') {
			j$('img#target_banner_img').imgAreaSelect({remove:true});
			j$('img#target_banner_img').imgAreaSelect({
				handles: true,
				onSelectChange: onSelectChange,
				onSelectEnd: onSelectEnd
			});
	
			img = j$(this).find('img.hiddenImg');
			imgUrl = j$(this).find('img.thumbBannerImg_crop').attr('src');
			fileName = j$(this).find('label').text();

			// Change visible image
			j$('img#target_banner_img').attr({
				'src': imgUrl,
				'width': img.width(),
				'height': img.height()
			});
			// Change send value
			j$('#crop_banner_img').attr({
				'value': imgUrl	// image url
			});
			j$('#crop_banner_img_file_name').attr({
				'value': fileName	// image file name
			});
			
			j$("input#header_resize_val_px").val(img.width());
		}
	});
});

j$(function(){
	// Ajax Post call to trim image
	j$('#send_trim_banner, #send_trim_title').click(function() {
		var resize_mode;
		var resize_value;
		var target_ul_id;
		var data;

		// Header banner image
		if (j$(this).attr('id') == 'send_trim_banner') {
			// Target check
			if (j$('#header_img_edit_target_trim').is(':checked')) {
				// Trim
				// Parameter check.
				if ( !j$('#header_x1').val() || !j$('#header_y1').val() || !j$('#header_crop_width').val() || !j$('#header_crop_height').val() ) {
					alert('Some parameter is nothing!\nPlease reset the params with corrent value.');
					return;
				} else {
					// Get values
					data = {
							target: 'banner',
							edit_type: 'trim',
							img : j$('#crop_banner_img').val(),
							file_name : j$('#crop_banner_img_file_name').val(),
							x1 : j$('#header_x1').val(),
							y1 : j$('#header_y1').val(),
							width : j$('#header_crop_width').val(),
							height : j$('#header_crop_height').val(),
							template_dir : j$('.template_dir').val(),
							image_dir : j$('#form_trim_header_img .image_dir').val(),
							image_url : j$('#form_trim_header_img .image_url').val()
					};
					target_ul_id = 'ul#crop_header_img';
				}
			} else if (j$('#header_img_edit_target_resize').is(':checked')) {
				// Resize
				if (j$('#header_img_resize_px').is(':checked')) {
					if ( !j$("#header_resize_val_px").val() ) {
						alert("Pixel value is blank!");
						return;
					} else if (!checkInt(j$("#header_resize_val_px").val())) {
						alert("Pixel value is not integer!");
						return;
					}
					resize_mode = 'pixel';
					resize_value = j$("#header_resize_val_px").val();
				} else {
					if ( !j$("#header_resize_val_percent").val() ) {
						alert("Percent value is blank!");
						return;
					} else if (!checkInt(j$("#header_resize_val_percent").val())) {
						alert("Percent value is not integer!");
						return;
					}
					resize_mode = 'percent';
					resize_value = j$("#header_resize_val_percent").val();
				}
				// Get values
				data = {
						target: 'banner',
						edit_type: 'resize',
						resize_mode: resize_mode,
						resize_value: resize_value,
						img : j$('#crop_banner_img').val(),
						file_name : j$('#crop_banner_img_file_name').val(),
						template_dir : j$('.template_dir').val(),
						image_dir : j$('#form_trim_header_img .image_dir').val(),
						image_url : j$('#form_trim_header_img .image_url').val()
				};
				target_ul_id = 'ul#crop_header_img';
			} else {
				alert("Please choose editing type");
				return;
			}
		}
		
		// Title image
		if (j$(this).attr('id') == 'send_trim_title') {
			// Target check
			if (j$('#title_img_edit_target_trim').is(':checked')) {
				// Trim

				// Parameter check.
				if ( !j$('#title_x1').val() || !j$('#title_y1').val() || !j$('#title_crop_width').val() || !j$('#title_crop_height').val() ) {
					alert('Some parameter is nothing!\nPlease reset the params with corrent value.');
					return;
				} else {
					// Get values
					data = {
							target: 'title',
							edit_type: 'trim',
							img : j$('#crop_title_img').val(),
							file_name : j$('#crop_title_img_file_name').val(),
							x1 : j$('#title_x1').val(),
							y1 : j$('#title_y1').val(),
							width : j$('#title_crop_width').val(),
							height : j$('#title_crop_height').val(),
							template_dir : j$('.template_dir').val(),
							image_dir : j$('#form_trim_title_img .image_dir').val(),
							image_url : j$('#form_trim_title_img .image_url').val()
					};
					target_ul_id = 'ul#crop_title_img';
				}
				
			} else if (j$('#title_img_edit_target_resize').is(':checked')) {
				// Resize
				if (j$('#title_img_resize_px').is(':checked')) {
					if ( !j$("#title_resize_val_px").val() ) {
						alert("Pixel value is blank!");
						return;
					} else if (!checkInt(j$("#title_resize_val_px").val())) {
						alert("Pixel value is not integer!");
						return;
					}
					resize_mode = 'pixel';
					resize_value = j$("#title_resize_val_px").val();
				} else {
					if ( !j$("#title_resize_val_percent").val() ) {
						alert("Percent value is blank!");
						return;
					} else if (!checkInt(j$("#title_resize_val_percent").val())) {
						alert("Percent value is not integer!");
						return;
					}
					resize_mode = 'percent';
					resize_value = j$("#title_resize_val_percent").val();
				}
				// Get values
				data = {
						target: 'title',
						edit_type: 'resize',
						resize_mode: resize_mode,
						resize_value: resize_value,
						img : j$('#crop_title_img').val(),
						file_name : j$('#crop_title_img_file_name').val(),
						template_dir : j$('.template_dir').val(),
						image_dir : j$('#form_trim_title_img .image_dir').val(),
						image_url : j$('#form_trim_title_img .image_url').val()
				};
				target_ul_id = 'ul#crop_title_img';
			} else {
				alert("Please choose editing type");
				return;
			}
		}

		// POST
		j$.ajax({
			type: "POST",
			url: j$('.template_url').val() + "/inc/admin/edit_execute.php",
			data: data,
			success: function(data, dataType) {
				j$(target_ul_id).append(data);
				alert("Edited!");
			},
			error: function(XMLHttpRequest, textStatus, errorThrown) {
				this; // Callback option
				alert('Error : ' + errorThrown);
			}
		});
	});
	// Disable reload page for Ajax
	j$('#form_trim_header_img, #form_trim_title_img').submit(function() {
		return false;
	});
	//---------------- Ajax Post Image End
	
	// handle for radio button check
	j$("#title_img_edit_target_trim").on('click', function(){
		j$('img#target_title_img').imgAreaSelect({remove:true});
		j$('img#target_title_img').imgAreaSelect({
			handles: true,
			onSelectChange: onSelectChange,
			onSelectEnd: onSelectEnd
		});
		j$("#label_for_title_img").slideDown('fast');
		j$("#title_img_trim_params").fadeIn('fast');
		j$("#title_img_resize_params").fadeOut('fast');
	});
	j$("#title_img_edit_target_resize").on('click', function(){
		j$('img#target_title_img').imgAreaSelect({remove:true});
		j$("#label_for_title_img").slideUp('fast');
		j$("#title_img_trim_params").fadeOut('fast');
		j$("#title_img_resize_params").fadeIn('fast');
	});
	j$("#header_img_edit_target_trim").on('click', function(){
		j$('img#target_banner_img').imgAreaSelect({remove:true});
		j$('img#target_banner_img').imgAreaSelect({
			handles: true,
			onSelectChange: onSelectChange,
			onSelectEnd: onSelectEnd
		});
		j$("#label_for_header_img").slideDown('fast');
		j$("#header_img_trim_params").fadeIn('fast');
		j$("#header_img_resize_params").fadeOut('fast');
	});
	j$("#header_img_edit_target_resize").on('click', function(){
		j$('img#target_banner_img').imgAreaSelect({remove:true});
		j$("#label_for_header_img").slideUp('fast');
		j$("#header_img_trim_params").fadeOut('fast');
		j$("#header_img_resize_params").fadeIn('fast');
	});
});