var j$ = jQuery,
	masonryContainer,
	mqWidth	= 960,
	thisBody = j$('body'),
	gotop = j$("#gototop2");


//------------------------------------------------------------
// Dropdown menu animation effect in global menu
// -----------------------------------------------------------
j$.fn.dropDown = function(options) {

	options = j$.extend({speed: 200}, options || {});
	this.each(function() {
		var root = this, zIndex = 1000;

		function getSubnav(ele) {
			if (ele.nodeName.toLowerCase() == 'li') {
				var subnav = j$('> ul', ele);
				return subnav.length ? subnav[0] : null;
			} else {
				return ele;
			}
		}

		function getActuator(ele) {
			if (ele.nodeName.toLowerCase() == 'ul') {
				return j$(ele).parents('li')[0];
			} else {
				return ele;
			}
		}

		function hide() {
			var subnav = getSubnav(this);
			if (!subnav) return;
			j$.data(subnav, 'cancelHide', false);
			setTimeout(function() {
				if (!j$.data(subnav, 'cancelHide')) {
					j$(subnav).slideUp(options.speed);
				}
			}, 300);
		}

		function show() {
			// When width is 760pixel, only(for media queries).
			if (j$(window).width() > 900) {

				var subnav = getSubnav(this);
				if (!subnav) return;
				j$.data(subnav, 'cancelHide', true);
				j$(subnav).css({zIndex: zIndex++}).slideDown(options.speed);
				if (this.nodeName.toLowerCase() == 'ul') {
					var li = getActuator(this);
					j$(li).addClass('hover');
					j$('> a', li).addClass('hover');
				}
			}
		}

		j$('ul, li', this).hover(show, hide);
		j$('li', this).hover(
			function() { j$(this).addClass('hover'); j$('> a', this).addClass('hover'); },
			function() { j$(this).removeClass('hover'); j$('> a', this).removeClass('hover');}
		);
	});
};


//------------------------------------------------------------
// Add even and odd class
// -----------------------------------------------------------
j$(function() {
	//Add even odd Class for Table Cell
	j$('.commentlist li:nth-child(even)').addClass('even');
	j$('.commentlist li:nth-child(odd)').addClass('odd');
});

//------------------------------------------------------------
// dp-slide class effect
// -----------------------------------------------------------
j$(function() {
	//User's toggle
	var toggle = j$('.dp-slide');
	toggle.prev().css('cursor', 'pointer');
	toggle.prev().on('click',function(){
		var $this = j$(this).next();

		if ($this.length) {
			$this.slideToggle();
		}
	});
});


/**
 * Count SNS shares
 */
function dp_count_shares(){
    let isCache = thisBody[0].dataset.ctSnsCache || true,
        cacheTime = thisBody[0].dataset.ctSnsCacheTime || 86400000;
    j$(".ct-shares").dpShareCount({
        cache: isCache,
        cacheTime: cacheTime,
        numSelector: '.share-num'
    });
}


//------------------------------------------------------------
// Execute CSS3 animation
function css3Animation(target, aniClass, myEvent) {
	if (myEvent === 'click') {
		j$(target).on('click',function() {
			//j$(this).addClass(aniClass);
		});
	} else if (myEvent === 'hover') {
		j$(target).hover(
			function() {
				j$(this).addClass(aniClass);
			},
			function(){
				j$(this).removeClass(aniClass);
			}
		);
	}
}

//------------------------------------------------------------
// Show Tool Tip text
function showGoToToolTip(e) {
	// Return Top hover
	j$(e).hover(
		function(){
			j$(e).children('span').fadeIn('fast');
		},
		function(){
			j$(e).children('span').fadeOut('fast');
		}
	);
}
function showToolTip(target, tooltip_class, of_x, of_y, arrow) {
	var ele = j$(target);
	ele.each(function() {
		var tooltip 	= null;
		var offset_X	= of_x || 34;
		var offset_Y	= of_y || 24;
		var title 	= j$(this).attr('title');
		var popup 	= '<div class="tooltip-div"><span class="tooltip-msg">' + title + '</span></div>';

		if (arrow) {
			popup 	= '<div class="tooltip-div"><span class="tooltip-arrow"></span><span class="tooltip-msg">' + title + '</span></div>';
		}

		j$(this).mouseover(function(){
			j$(this).removeAttr('title');
			if (j$(this)[0].nodeName === 'IMG') {
				j$(this).after(popup);
			} else {
				j$(this).append(popup);
			}
		}).mousemove(function(e){
			j$(this).children('.tooltip-div').css({
				'top':e.offsetY + offset_Y + 'px',
				'left':e.offsetX + offset_X + 'px'
			});
		}).mouseout(function(){
			j$(this).attr('title', title);
			if (j$(this)[0].nodeName === 'IMG') {
				j$('.tooltip-div').remove();
			} else {
				j$(this).children('.tooltip-div').remove();
			}
		});
	});
}
function showBgStretcherToolTip(e, tooltip) {
	var offset_Y = 18;
	var offset_X = 180;

	j$(e).mouseover(function() {
		j$(tooltip).fadeIn('fast');
	}).mouseout(function() {
		j$(tooltip).fadeOut('fast');
	}).mousemove(function(elem) {
		j$(tooltip).css({
			'top':elem.pageY - offset_Y + 'px',
			'left':elem.pageX - offset_X + 'px'
		});
	});
}
//------------------------------------------------------------
// Get the element absolute point in screen
function getElementScreenPosition(elem) {
	var html = document.documentElement;
	var rect = elem.getBoundingClientRect();
	var left = rect.left - html.clientLeft;
	var top = rect.top - html.clientTop;
	return {left:left, top:top};
}
//------------------------------------------------------------
// Get the element absolute point in page
function getElementPosition(elem) {
	// get scroll position
	var html = document.documentElement;
	var body = document.body;
	var scrollLeft = (body.scrollLeft || html.scrollLeft);
	var scrollTop  = (body.scrollTop || html.scrollTop);

	// get the element position in screen
	var pos = getElementScreenPosition(elem);

	// add scroll height
	var left = pos.left + scrollLeft;
	var top  = pos.top + scrollTop;
	return {left:left, top:top};
}

//-----------------------------------------------------------
// Call when window size changed
function resetGlobalMenu() {
	var menu_ul = j$('#fixed_menu_ul ');
	if (j$(window).width() > mqWidth) {
		// Remove object and class for media queries
		j$('.fl_submenu_li').remove();
		menu_ul.find('.sub-menu').hide();
		menu_ul.find('.sub-menu').removeClass('bd-none');
		menu_ul.find('li .item-menu').removeClass('bd-none');

		// Enable drop down
		menu_ul.dropDown();
		menu_ul.show();
		menu_ul.find('li').removeClass('expand_float_menu_li');
	}
	if (j$(window).width() <= mqWidth) {
		menu_ul.hide();
		if (!menu_ul.find('li').hasClass('expand_float_menu_li')) {
			menu_ul.find('li').addClass('expand_float_menu_li');

			// Floating menu of children expansion
			menu_ul.find('li').find('ul').each(function(){
				j$(this).addClass('bd-none');
				j$('> li .menu-item', this).addClass('bd-none');
				j$(this).parent('li:last').prepend('<span class="fl_submenu_li icon-angle-down"></span>');
			});

			// Slide menu
			j$('.fl_submenu_li').on('click',function(){
				var targetMenu = j$(this).siblings('ul.sub-menu');
				if (targetMenu.is(':animated')) targetMenu.stop();
				if (targetMenu.is(':visible')) {
					j$(this).addClass('icon-angle-down');
					j$(this).removeClass('icon-angle-up');
				} else {
					j$(this).addClass('icon-angle-up');
					j$(this).removeClass('icon-angle-down');
				}
				targetMenu.slideToggle(200);
			});
		}
	}
}

//-----------------------------------------------------------
// jQuery mansonry gallery style
function galleryPosts(gId, gItem) {
	if (!j$(gId)[0]) return;

	// initialize Masonry
	var $masonryContainer = j$(gId).masonry();

	// After all image loaded...
	$masonryContainer.imagesLoaded(function() {
		$masonryContainer.masonry({
			itemSelector:gItem,
			isFitWidth : true
		});
	});
	return $masonryContainer;
}

//------------------------------------------------------------
// Images loaded
// ### To use this, the opacity of source img set to 0.
// -----------------------------------------------------------
function imagesLoadedRun(ele, time) {
	if (!time) time = 500;
	j$(ele).imagesLoaded(function(){
		j$(ele).find('img').animate({
			opacity:1
		}, time);
	});
}


// Navigate anchor link
function getAnchor() {
	j$("ul.recent_entries li, .post_excerpt, .dp_related_posts_vertical li, .dp_related_posts_horizon li").on('click',function(){
		window.location=j$(this).find("a").attr("href");
	});
}

//------------------------------------------------------------
// Tab widget function
// -----------------------------------------------------------
function dpTabWidget() {
	// Switch tab widget
	j$('div.first_tab').show();
	j$('li.dp_tab_title').on('click',function () {
		if (j$(this).hasClass('active_tab')) return;

		var tabTitleObj = j$(this).parent('.dp_tab_widget_ul');
		var my_id = j$(this).attr('id');

		tabTitleObj.children('li').removeClass('active_tab');
		tabTitleObj.children('li').addClass('inactive_tab');
		
		j$(this).removeClass('inactive_tab');
		j$(this).addClass('active_tab');

		tabTitleObj.next('.dp_tab_contents').children('div').hide();

		// Hidden by accordion
		if (tabTitleObj.next('.dp_tab_contents').children('#' + my_id + '_content .widget').children().is(':hidden')) {
			tabTitleObj.next('.dp_tab_contents').children('#' + my_id + '_content .widget').children().show();
		}
		// Display
		tabTitleObj.next('.dp_tab_contents').children('#' + my_id + '_content').fadeIn();
	});
}

//-----------------------------------------------------------
// Vertical Expand menu
//-----------------------------------------------------------
function verticalDropDownWidgetList() {
	// j$('.widget_nav_menu ul.sub-menu, .widget_pages ul.children, .widget_categories ul.children, .widget_mycategoryorder ul.children').prev('a').addClass('icon-down-open');

	j$('.widget_nav_menu.slider_fx ul.sub-menu, .widget_pages.slider_fx ul.children, .widget_categories.slider_fx ul.children, .widget_mycategoryorder.slider_fx ul.children, .slider_fx .tab_category_content ul.children').prev('a').css('padding-right', '34px').after('<span class="v_sub_menu_btn icon-triangle-down"></span>');

	// Click trigger
	j$('span.v_sub_menu_btn').on('click',function(){
		var current = j$(this);
		current.next('ul:animated').stop();
		if (current.next('ul').is(':visible')) {
			current.addClass('icon-triangle-down');
			current.removeClass('icon-triangle-up');
		} else {
			current.addClass('icon-triangle-up');
			current.removeClass('icon-triangle-down');
		}
		current.next('ul:not(:animated)').slideToggle(200);
		return false;
	});
}


//------------------------------------------------------------
// When whole page loaded
// -----------------------------------------------------------
function dp_loaded() {
	showBgStretcherToolTip('#bgstretcher li a', 'span.bgstr-tooltip');

	// Fix widget position
	if (j$("#sidebar, #sidebar2")[0]){
		if (j$("#dp_fix_widget")[0]) {
			var fixWidget 			= j$("#dp_fix_widget");
			var fixSidebarHeight	= 88;
			var fixStopHeight		= 132;
			var stopY				= 0;
			var sidebarElem 		= document.getElementById("sidebar");
			if (!sidebarElem) {
				sidebarElem = document.getElementById("sidebar2");
			}
			var fixWidgetElem 		= document.getElementById("dp_fix_widget");
			var stopElem 			= j$('#container_footer')[0] ? document.getElementById("container_footer") : document.getElementById("footer");

			var contentElem			= document.getElementById("content");
			var containerFooterElem = document.getElementById("container_footer");
			var footerElem 			= document.getElementById("footer");

			var sidebarHeight 		= sidebarElem.offsetHeight + sidebarElem.offsetTop - fixSidebarHeight;
			var visibleHeight		= document.documentElement.clientHeight;
			var bottomPos			= 0;

			var footerHeight		= 0;

			if (containerFooterElem) {
				footerHeight = containerFooterElem.offsetHeight + footerElem.offsetHeight + fixSidebarHeight + fixStopHeight;
			} else {
				footerHeight = footerElem.offsetHeight + fixSidebarHeight + fixStopHeight;
			}

			j$(window).scroll(function(){
				stopY = j$(window).scrollTop() + fixWidgetElem.offsetHeight;

				if (j$(window).scrollTop() > sidebarHeight && j$(window).width() > 980) {
					if (stopY >= stopElem.offsetTop - fixStopHeight) {
						// Bottom position
						bottomPos	= visibleHeight - (stopElem.offsetTop - stopElem.offsetHeight - j$(window).scrollTop() + 44);

						if ( (footerHeight > window.innerHeight) && (sidebarElem.offsetHeight > contentElem.offsetHeight)) {
							fixWidget.css("position", "relative");
							fixWidget.css("top","auto");
							fixWidget.css("bottom", "auto");
						} else {
							fixWidget.css("position", "fixed");
							fixWidget.css("top", "auto");
							fixWidget.css("bottom", bottomPos);
						}
						
					} else {
						fixWidget.css("position", "fixed");
						fixWidget.css("top", "66px");
						fixWidget.css("bottom", "auto");
					}
				}
				if (j$(window).scrollTop() <= sidebarHeight || j$(window).width() <= 980){
					fixWidget.css("position", "relative");
					fixWidget.css("top","auto");
					fixWidget.css("bottom", "auto");
				}
			});
		}
	}
}
// Scroll Event
function dp_scroll_event() {
	var document_y = document.documentElement.scrollHeight || document.body.scrollHeight,
		scroll_y = document.documentElement.scrollTop || document.body.scrollTop;
	if (scroll_y > 160) {
		gotop.fadeIn(300);
	} else if (scroll_y <= 110) {
		gotop.fadeOut(300);
	}
}
//------------------------------------------------------------
// Window events
// -----------------------------------------------------------
j$(window).on({
    'load':function(e){
        dp_loaded();
    },
    'resize':function(e){
    	resetGlobalMenu();
    },
    'scroll':function(e){
        dp_scroll_event();
    }
});
//------------------------------------------------------------
// When document is ready...
// -----------------------------------------------------------
j$(document).ready(function(){
	// Copyright when not activated
	if ( typeof dp_theme_noactivate_copyright !== 'undefined' ){
		dp_theme_noactivate_copyright();
	}

	// Set global menu style
	resetGlobalMenu();

	// Images loaded
	imagesLoadedRun(".post_thumb, .post_thumb_portfolio, .widget-post-thumb", 500);

	// Menu Toggle for mobile
	j$('#mb_header_menu_arrow').on('click',function(){
		j$('#mb_header_menu_list').slideToggle();
	});

	// Header content fade in
	if (j$('#header_left')[0]) {
		var site_title = j$('#site_title');
		var hd_left = j$('#header_left');
		j$("#bgstretcher img").imagesLoaded( function(){
			// Hide loading indicator
			if (j$('#bgstretcher')[0]) {
				site_title.css('background-image', 'none');
			}
			
			hd_left.fadeIn(1800);
			if (hd_left.find('h1')[0]) {
				hd_left.find('h1').animate({
					'left':0
				}, 1800);
			}
			if (hd_left.find('h2')[0]) {
				hd_left.find('h2').animate({
					'right':0
				}, 1800);
			}
		});
	}
	if (j$('#top-right-widget')[0]) {
		j$('#top-right-widget').fadeIn(1800);
	}

	// When floating menu clicked
	j$('#expand_float_menu').on('click',function () {
		var targetMenu = j$('#fixed_menu_ul');
		if (targetMenu.is(':animated')) targetMenu.stop();
		
		if (targetMenu.is(':visible')) {
			targetMenu.hide();
		} else {
			targetMenu.show();
		}
	});

	dp_count_shares();


	// Switch comment trackback
	j$('#commentlist_div').on('click',function () {
		j$(this).addClass('active_tab');
		j$(this).removeClass('inactive_tab');
		j$('#trackbacks_div').addClass('inactive_tab');
		j$('#trackbacks_div').removeClass('active_tab');

		if (!j$('.commentlist_div').is(':visible')) {
			j$('.commentlist_div').fadeIn();
		}
		j$('.trackbacks_div').hide();
	});
	j$('#trackbacks_div').on('click',function () {
		j$(this).addClass('active_tab');
		j$(this).removeClass('inactive_tab');
		j$('#commentlist_div').addClass('inactive_tab');
		j$('#commentlist_div').removeClass('active_tab');

		if (!j$('.trackbacks_div').is(':visible')) {
			j$('.trackbacks_div').fadeIn();
		}
		j$('.commentlist_div').hide();
	});

	// Tabwidget
	dpTabWidget();
	// Vertical Drop Down wdget menu
	verticalDropDownWidgetList();
	// fitVideos
	thisBody.fitVids({ignore:".unfit"});

	// Navigate anchor link
	getAnchor();
	// Animation
	css3Animation('.btn.disabled', 'dp-ani-shake', 'click');

	// Tooltip
	showToolTip('.tooltip', '.tooltip-div', -80, 30, true);
	

	// Masonry Gallery 
	if (j$('#gallery-style')[0]) {
		masonryContainer = galleryPosts('#gallery-style', '.g_item');
	} 
	if (j$('#gallery-style-1col')[0]) {
		masonryContainer = galleryPosts('#gallery-style-1col', '.g_item');
	}
	
	// Return Top hover
	showGoToToolTip('#gototop');
});
