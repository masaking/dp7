var j$ = jQuery;
(function(){
	// Color picker (iris)
	let colorPickerOptions = {
		defaultColor: false,
		hide: true,
		palettes:true
	};
	j$('.dp-color-field').wpColorPicker(colorPickerOptions);

	if (typeof wp.media === 'function'){
		// Media uploader
		let custom_uploader = wp.media({
				title: 'Choose Image',
				library: {
					type: 'image'
				},
				button: {
					text: 'OK'
				},
				multiple: false
			}),
			current_media_url_field = null;

		// Open Media window
		j$('.dp_upload_image_button').on('click', function(e) {
			current_media_url_field = j$(this).prev('.img_url');
			e.preventDefault();
			custom_uploader.open();
		});

		// Set the URL
		custom_uploader.on("select", function () {
			let images = custom_uploader.state().get('selection');

			images.each(function(file) {
				if (current_media_url_field !== null) {
					current_media_url_field.val(file.toJSON().url);
				}
			});
		});
	}
})();