<?php
/*******************************************************
* description and keyword of meta tag.
*******************************************************/
/** ===================================================
* Create meta tag including description and keywords attributes.
* @param	none
* @return	none
*/
function dp_meta_kw_desc() {
	global $options;

	if ( class_exists( 'amt_add_meta_tags' ) || class_exists('All_in_One_SEO_Pack') || function_exists( 'aioseo' ) || function_exists( 'YoastSEO' ) ) return;

	$meta_code = '<meta name="description" content="' . create_meta_desc_tag() . '" />';
	if ( !( isset( $options['disable_meta_keywords'] ) && ! empty( $options['disable_meta_keywords'] ) ) ) {
		$meta_code .= '<meta name="keywords" content="' . get_meta_keywords() . '" />';
	}
	echo $meta_code;
}
/*-------------------------------------------
 meta description tag
--------------------------------------------*/
function create_meta_desc_tag() {
	global $options;

	$meta_desc_tag = "";

	if (is_home()) {
		if ($options['enable_meta_def_desc'] && $options['meta_def_desc']) {
			$meta_desc_tag = str_replace(array("\r\n","\r","\n"), "", strip_tags($options['meta_def_desc']));
		} else {
			$meta_desc_tag = get_bloginfo('description');
		}
		if (is_paged()) {
			$meta_desc_tag .= '(' . get_query_var('paged') . ')';
		}
	} else if (is_category()) {	
		$catDesc = str_replace(array("\r\n","\r","\n"), "", strip_tags(category_description()));
		if (empty($catDesc)) {
			if (is_paged()) {
				$meta_desc_tag = wp_title('[', false) 
						. __(' ]Category page is displayed.', 'DigiPress')
						. '(' . get_query_var('paged') . ')';
			} else {
				$meta_desc_tag = wp_title('[', false) 
						. __(' ]Category page is displayed.', 'DigiPress');
			}
		} else {
			if (is_paged()) {
				$meta_desc_tag = $catDesc . '(' . get_query_var('paged') . ')';
			} else {
				$meta_desc_tag = $catDesc;
			}
		}
	} else if (is_year()) {
		if (is_paged()) {
			$meta_desc_tag =  __('Archive of the ', 'DigiPress') 
					. get_the_time(__('Y', 'DigiPress')) 
					. __(' is displayed.', 'DigiPress') 
					. '(' . get_query_var('paged') . ')';
		} else {
			$meta_desc_tag = __('Archive of the ', 'DigiPress')
					. get_the_time(__('Y', 'DigiPress')) 
					. __(' is displayed.', 'DigiPress');
		}
	} else if (is_month()) {
		if (is_paged()) {
			$meta_desc_tag = __('Archive of the ', 'DigiPress')
					. get_the_time(__('Y/m', 'DigiPress')) 
					. __(' is displayed.', 'DigiPress')
					. '(' . get_query_var('paged') . ')';
		} else {
			$meta_desc_tag = __('Archive of the ', 'DigiPress')
					. get_the_time(__('Y/m', 'DigiPress')) 
					. __(' is displayed.', 'DigiPress');
		}
	} else if (is_day()) {
		if (is_paged()) {
			$meta_desc_tag = __('Archive of the ', 'DigiPress')
					. get_the_time(__('Y/m/d', 'DigiPress')) 
					. __(' is displayed.', 'DigiPress')
					. '(' . get_query_var('paged') . ')';
		} else {
			$meta_desc_tag = __('Archive of the ', 'DigiPress')
					. get_the_time(__('Y/m/d', 'DigiPress')) 
					. __(' is displayed.', 'DigiPress');
		}
	} else if (is_time()) {
		if (is_paged()) {
			$meta_desc_tag = __('Archive of the ', 'DigiPress')
					. get_the_time(__('Y/m/d', 'DigiPress')) 
					. __(' is displayed.', 'DigiPress')
					. '(' . get_query_var('paged') . ')';
		} else {
			$meta_desc_tag = __('Archive of the ', 'DigiPress')
					. get_the_time(__('Y/m/d', 'DigiPress')) 
					. __(' is displayed.', 'DigiPress');
		}
	} else if (is_tag()) {
		$tagDesc = str_replace(array("\r\n","\r","\n"), "", strip_tags(tag_description()));
		if (empty($tagDesc)) {
			if (is_paged()) {
				$meta_desc_tag =  wp_title('',false)
						. __(' Tagged posts is displayed.', 'DigiPress')
						. ' Page(' . get_query_var('paged') . ')';
			} else {
				$meta_desc_tag =  wp_title('',false) 
						. __(' Tagged posts is displayed.', 'DigiPress');
			}
		} else {
			if (is_paged()) {
				$meta_desc_tag = $tagDesc . '(' . get_query_var('paged') . ')';
			} else {
				$meta_desc_tag = $tagDesc;
			}
		}
	} else if (is_search()) {
		if (is_paged()) {
			$meta_desc_tag = wp_title('', false, 'right') . '(' .get_query_var('paged') . ')';
		} else {
			$meta_desc_tag = wp_title('', false, 'right') ;
		}
	} else if (is_singular()) {
		while (have_posts()) {
			the_post();
			$desc = str_replace(array("\r\n","\r","\n"), "", strip_tags(get_the_excerpt()));
			if (mb_strlen($desc) > 300) $desc = mb_substr($desc, 0, 300, 'UTF-8').'...';
			$meta_desc_tag = $desc;
		}
	} else if (is_author()){
		$desc = str_replace(array("\r\n","\r","\n"), "", strip_tags(get_the_author_meta('description')));
		if (empty($desc)) {
			$meta_desc_tag =  wp_title('',false) . __(' is displayed.', 'DigiPress');
			if (is_paged()) {
				$meta_desc_tag .= '(' . get_query_var('paged') . ')';
			}
		} else {
			if (mb_strlen($desc) > 300) $desc = mb_substr($desc, 0, 300, 'UTF-8').'...';
			if (is_paged()) {
				$meta_desc_tag = $desc . '(' . get_query_var('paged') . ')';
			} else {
				$meta_desc_tag = $desc;
			}
		}
	} else {
		$meta_desc_tag =  wp_title('',false) . __(' is displayed.', 'DigiPress');
		if (is_paged()) {
			$meta_desc_tag .= '(' . get_query_var('paged') . ')';
		}
	}
	
	return $meta_desc_tag;
}
/*-------------------------------------------
 meta keyword tag
--------------------------------------------*/
function get_meta_keywords() {
	global $options;
	$meta_kw = "";

	if (is_singular()) {
		// get_post_meta(get_the_ID(), 'dp_meta_keyword', true)

		if (get_post_meta(get_the_ID(), 'dp_meta_keyword', true)) {
				$meta_kw = get_post_meta(get_the_ID(), 'dp_meta_keyword', true);
		} else {
			if (is_single()) {
				while (have_posts()) : the_post();
					$posttags = get_the_tags();
					$strTags = "";
					if ($posttags) {
						foreach($posttags as $tag) {
							$strTags =  $strTags . $tag->name . ',';
						}
					}
					if ( ! $strTags == "") {
						$meta_kw = rtrim($strTags, ",");
					}
				endwhile;
			} else if (is_page()) {
				$meta_kw = '';
			} else {
				$meta_kw = wp_title(',', false, 'right');
			}
		}

	} else if (is_archive()) {
		$arcName = wp_title(',', false, 'right');
		$meta_kw = $arcName . trim($options['meta_def_kw']);

	} else if (is_search()) {
		$arcName = wp_title(',', false, 'right');
		$meta_kw = $arcName . trim($options['meta_def_kw']) . ',search result';

	} else if (is_home()) {
		if ($options['enable_meta_def_kw']) {
			$meta_kw = trim($options['meta_def_kw']);
		}
	} else {
		$meta_kw = wp_title(',', false, 'right');
	}

	if (is_paged()) {
		$meta_kw .= ',Paged'. get_query_var('paged');
	}

	return $meta_kw;
}
/*-------------------------------------------
 meta canonical
--------------------------------------------*/
function dp_show_canonical() {
	global $wp, $page, $paged, $wp_query;
	$canonical_url = '';
	if (!is_404()) {
		if (is_singular()) {
			$canonical_url = get_permalink();
		} else {
			$canonical_url = is_ssl() ? 'https://' : 'http://';
			$canonical_url .= $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		}
	}
	// Show
	if (!empty($canonical_url)) {
		echo '<link rel="canonical" href="'.esc_url($canonical_url).'" />';
	}
	// Paged
	if (!is_404() && !is_singular()) {
		$max_page = (int)$wp_query->max_num_pages;
		if ($max_page > 1) {
			$nextpage = $paged + 1;
			if ( $nextpage <= $max_page ) {
				echo '<link rel="next" href="'.next_posts( $max_page, false ).'" />';
			}
			if( $paged > 1 ){
				echo '<link rel="prev" href="'.previous_posts( false ).'" />'; 
			}
		}
	}
}