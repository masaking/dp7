<?php
/*******************************************************
* Create Navigation link.
*******************************************************/
/** ===================================================
* Create site navigation strings.
* @param	none
* @return	none
*/
function dp_breadcrumb($divOption = array(
										"id" => "", 
										"class" => "dp_breadcrumb clearfix") ) {
	global $options;
	global $post;
	$str ='';

	$blank_flag = true;

	// Not home and admin page
	
	if( !is_admin() ){
		$tagAttribute = '';
		$tagTitleStart = '<span>';
		$tagTitleEnd = '</span>';

		foreach($divOption as $attrName => $attrValue){
			$tagAttribute .= sprintf(' %s="%s"', $attrName, $attrValue);
		}
		$str.= '<nav'. $tagAttribute .'>';
		$str.= '<ul>';

		if (!$options['hide_home_breadcrumb']) {
			$str.= '<li><a href="'. home_url() .'/" class="icon-home">' . $tagTitleStart . 'HOME' . $tagTitleEnd .'</a></li>';

			$blank_flag = false;
		}

		if (is_home() && is_paged()) {							// Home paged
			$tagTitleStart = '<span class="icon-doc">';
			$str.='<li>' . $tagTitleStart . 'Page ' . get_query_var('paged') . $tagTitleEnd . '</li>';

			$blank_flag = false;

		} else if (is_archive()){
			// Category
			if (is_category()) {
				$tagTitleStart = '<span class="icon-folder">';
				$cat = get_queried_object();
				if($cat->parent != 0){
					$ancestors = array_reverse(get_ancestors( $cat->term_id, 'category' ));
					foreach($ancestors as $ancestor){
						$str.='<li><a href="'. get_category_link($ancestor) .'">' . $tagTitleStart . get_cat_name($ancestor) . $tagTitleEnd . '</a></li>';
					}
					$blank_flag = false;
				}
				// Paged
				if (is_paged()) {
					$str.='<li><a href="'. get_category_link($cat->term_id) .'">' . $tagTitleStart . $cat->name . ' ( ' . (int)get_query_var('paged') . ' ) ' . $tagTitleEnd . '</a></li>';
					$blank_flag = false;

				} else {
					$str.='<li><a href="'. get_category_link($cat->term_id) .'">' . $tagTitleStart . $cat->name . $tagTitleEnd . '</a></li>';

					$blank_flag = false;
				}

			} elseif (is_date()){
				// Date Archive
				$tagTitleStart = '<span class="icon-calendar">';

				if (get_query_var('day') != 0){				// Archive of the day
					$str.='<li><a href="'. get_year_link(get_query_var('year')). '">' . $tagTitleStart . get_query_var('year').$tagTitleEnd . '</a></li>';
					$str.='<li><a href="'. get_month_link(get_query_var('year'), get_query_var('monthnum')). '">' . $tagTitleStart . get_query_var('monthnum') . $tagTitleEnd . '</a></li>';
					$str.='<li>' . $tagTitleStart . get_query_var('day'). $tagTitleEnd . '</li>';

					$blank_flag = false;

				} elseif (get_query_var('monthnum') != 0){	// Archive of the month
					$str.='<li><a href="'. get_year_link(get_query_var('year')) .'">' . $tagTitleStart . get_query_var('year') . $tagTitleEnd .'</a></li>';
					// Paged
					if (is_paged()) {
						$str.='<li>' . $tagTitleStart . get_query_var('monthnum').' ( ' . get_query_var('paged') . ' ) ' . $tagTitleEnd . '</li>';
					} else {
						$str.='<li>' . $tagTitleStart . get_query_var('monthnum'). $tagTitleEnd .'</li>';
					}

					$blank_flag = false;
				} else {									// Archive of the year
					// Paged
					if (is_paged()) {
						$str.='<li>' . $tagTitleStart . get_query_var('year') .' ( ' . get_query_var('paged') . ' ) ' . $tagTitleEnd . '</li>';
					} else {
						$str.='<li>' . $tagTitleStart . get_query_var('year') . $tagTitleEnd . '</li>';
					}

					$blank_flag = false;
				}

			} elseif (is_author()){
				// Archive of author
				$tagTitleStart = '<span class="icon-user">';
				// Paged
				if (is_paged()) {
					$str .='<li>' . $tagTitleStart  . get_the_author_meta('display_name', get_query_var('author')) . ' ( ' . get_query_var('paged') . ' ) ' . $tagTitleEnd . '</li>';
				} else {
					$str .='<li>' . $tagTitleStart  . get_the_author_meta('display_name', get_query_var('author')) . $tagTitleEnd . '</li>';
				}

				$blank_flag = false;

			} elseif (is_tag()){
				// Archive of tag
				$tagTitleStart = '<span class="icon-tag">';

				// Paged
				if (is_paged()) {
					$str.='<li>' . $tagTitleStart . single_tag_title( '' , false ) . ' ( ' . get_query_var('paged') . ' ) ' . $tagTitleEnd . '</li>';
				} else {
					$str.='<li>' . $tagTitleStart . single_tag_title( '' , false ) . $tagTitleEnd . '</li>';
				}

				$blank_flag = false;

			} elseif (is_post_type_archive()) {
				$customPostTypeObj = get_post_type_object(get_post_type());
				$customPostTypeTitle = esc_html($customPostTypeObj->labels->name);

				$str.='<li>' . $tagTitleStart . $customPostTypeTitle . $tagTitleEnd . '</li>';

				$blank_flag = false;

			} else {
				// Other
				$tagTitleStart = '<span>';

				// Paged
				if (is_paged()) {
					$str.='<li>' . $tagTitleStart . wp_title('', false) . ' ( ' . get_query_var('paged') . ' ) ' . $tagTitleEnd . '</li>';
				} else {
					$str.='<li>' . $tagTitleStart . wp_title('', false) . $tagTitleEnd . '</li>';
				}

				$blank_flag = false;
			}

		} elseif (is_search()) {							// Search result
			$word = isset( $_REQUEST['q']) ? $_GET['q'] : get_search_query();
			$tagTitleStart = '<span class="icon-search">';

			// Paged
			if (is_paged()) {
				$str.='<li>' . $tagTitleStart . $word . ' ( ' . get_query_var('paged') . ' ) ' . $tagTitleEnd . '</li>';
			} else {
				$str.='<li>' . $tagTitleStart  . $word . $tagTitleEnd . '</li>';
			}

			$blank_flag = false;

		} elseif (is_singular()) {

			$customPostTypeObj = get_post_type_object(get_post_type());
			$customPostTypeTitle = esc_html($customPostTypeObj->labels->name);

			if (is_single()) {
				// Single post
				if ((get_post_type() === 'post')) {
					// Single post
					$tagTitleStart = '<span class="icon-folder">';
					$categories = get_the_category($post->ID);
					$cat = $categories[0];
					if($cat->parent != 0){
						$ancestors = array_reverse(get_ancestors( $cat->term_id, 'category' ));
						foreach($ancestors as $ancestor){
							$str.='<li><a href="'. get_category_link($ancestor).'">'. $tagTitleStart . get_cat_name($ancestor) . $tagTitleEnd . '</a></li>';
						}
					}
					$str.='<li><a href="'. get_category_link($cat->term_id). '">' . $tagTitleStart . $cat-> cat_name . $tagTitleEnd . '</a></li>';

					$blank_flag = false;
				} else {
					// Maybe custom post type
					$str.='<li><a href="' . get_post_type_archive_link($post->post_type) . '">' . $tagTitleStart . $customPostTypeTitle . $tagTitleEnd.'</a></li>';
					$blank_flag = false;
				}

			} elseif (is_page()){
				// Page
				if($post -> post_parent != 0 ){
					$ancestors = array_reverse(get_post_ancestors( $post->ID ));
					foreach($ancestors as $ancestor){
						$str.='<li><a href="'. get_permalink($ancestor).'">' . $tagTitleStart . get_the_title($ancestor) . $tagTitleEnd . '</a></li>';
					}

					$blank_flag = false;
				}
			
			} elseif (is_attachment()){						// Atttachment page
				$tagTitleStart = '<span class="icon-picture">';
				$str.= '<li>' . $tagTitleStart . $post -> post_title . $tagTitleEnd .'</li>';

				$blank_flag = false;
			}
	
		} elseif (is_404()){								// 404 Not Found
			$tagTitleStart = '<span class="icon-404">';
			$str.='<li>' . $tagTitleStart  . 'Not found' . $tagTitleEnd . '</li>';

			$blank_flag = false;

		} else {											// Else
			$str.='<li>' . $tagTitleStart . wp_title('', false) . $tagTitleEnd . '</li>';

			$blank_flag = false;
		}
		$str.='</ul>';
		$str.='</nav>';
	}

	if (!$blank_flag) echo $str;
}
?>
