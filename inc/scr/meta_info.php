<?php
function postFormatIcon($format) {
	$titleIconClass = '';
	switch  ($format) {
		case 'aside':
			$titleIconClass = ' icon-pencil';
			break;
		case 'gallery':
			$titleIconClass = ' icon-pictures';
			break;
		case 'image':
			$titleIconClass = ' icon-picture';
			break;
		case 'quote':
			$titleIconClass = ' icon-quote-left';
			break;
		case 'status':
			$titleIconClass = ' icon-comment';
			break;
		case 'video':
			$titleIconClass = ' icon-video-play';
			break;
		case 'audio':
			$titleIconClass = ' icon-music';
			break;
		case 'chat':
			$titleIconClass = ' icon-comment';
			break;
		default:
			$titleIconClass = '';
			break;
	}

	return $titleIconClass;
}

/*******************************************************
* Published time diff
*******************************************************/
function publishedDiff(){
	$from 	= get_post_time('U',true); 	// Published datetime
	$to 	= time(); 				// Current time
	$diff 	= $to - $from; 			// Diff
	$code 	= '';
	if ( $diff < 0 ) {
		$code = '<span class="icon-clock"><time datetime="'.get_the_date('c').'">'.human_time_diff( $from, $to ) . __(' ago','DigiPress').'</time></span>';
	} elseif ( abs($diff) <= 86400 ) {
		// Posted in less than 24 hours
		// $code = '<span><time datetime="'.get_the_date('c').'">'.__('This article was posted in less than 24 hours.','DigiPress').'</time></span>';
		$code = '<span class="icon-clock"><time datetime="'.get_the_date('c').'">'.human_time_diff( $from, $to ) . __(' ago','DigiPress').'</time></span>';
	} else {
		$code = '<span class="icon-clock"><time datetime="'.get_the_date('c').'">'.human_time_diff( $from, $to ) . __(' ago','DigiPress').'</time></span>';
	}
	return $code;
}

/*******************************************************
* Meta content in excerpt
*******************************************************/
function showPostMetaForArchive() {
	if (post_password_required()) return;

	global $options, $post;
	$postType = get_post_type();

	$html_code	= '';
	$tags_code	= '';
	$cats_code	= '';
	$comment_code 	= '';
	$views_code 	= '';
	$author_code	= '';

	// Post parameters
	$dp_hide_date 			= get_post_meta(get_the_ID(), 'dp_hide_date', true);
	$dp_hide_author 		= get_post_meta(get_the_ID(), 'dp_hide_author', true);
	$dp_hide_cat 			= get_post_meta(get_the_ID(), 'dp_hide_cat', true);
	$dp_hide_tag 			= get_post_meta(get_the_ID(), 'dp_hide_tag', true);
	$dp_hide_views 			= get_post_meta(get_the_ID(), 'dp_hide_views', true);
	$dp_star_rating_enable 	= get_post_meta(get_the_ID(), 'dp_star_rating_enable', true);
	$dp_star_rating 		= get_post_meta(get_the_ID(), 'dp_star_rating', true);

	// Published date
	if ( isset( $options['show_pubdate_on_meta'] ) && !empty( $options['show_pubdate_on_meta'] ) && !$dp_hide_date) {
		$html_code .= '<span class="meta-date icon-calendar"><time datetime="'. get_the_date('c').'" class="published">'.get_the_date().'</time></span>';
	}

	// Category
	if ($postType === 'post' && isset( $options['show_cat_on_meta'] ) && !empty( $options['show_cat_on_meta'] ) && !$dp_hide_cat && !is_mobile_dp()) {
		$cats = get_the_category();
		if ( isset( $cats ) && is_array( $cats ) && isset( $cats[0]->cat_ID ) ) {
			for ($i = 0; $i < 3; ++$i) {
				if ( isset( $cats[$i] ) && !empty( $cats[$i] ) ) {
					$cats_code .= '<a href="'.get_category_link($cats[$i]->cat_ID).'" rel="tag">' .$cats[$i]->cat_name.'</a> ';
				} else {
					break;
				}
			}
		}
		$cats_code = '<span class="meta-cat entrylist-cat icon-folder">' .$cats_code. '</span>';
	}

	// Tags 
	if ( $postType === 'post' && isset( $options['show_tags'] ) && !empty( $options['show_tags'] ) && !is_mobile_dp() && !$dp_hide_tag) {
		$count = 0;
		$tags = get_the_tags();
		if ( isset( $tags ) && is_array( $tags ) ) {
			foreach ($tags as $tag) {
				$count++;
				if ($count < 4) {
					$tags_code .= '<a href="'.get_tag_link($tag->term_id).'" rel="tag" title="'.$tag->count.__(' topics of this tag.', 'DigiPress').'">'.$tag->name.'</a> ';
				} else {
					break;
				}
			}
		}
		$tags_code = $tags_code ? '<span class="entrylist-cat meta-tag icon-tag">'.$tags_code.'</span>': '' ;
	}
	/***
	 * First line
	 */
	// Categories + tags
	$html_code .= $cats_code.$tags_code;


	// Comments
	if ( comments_open() ) {
		$comment_code = '<span class="meta-comment icon-comment">'. get_comments_popup_link(
							'0', '1', '%').'</span>';
	}

	// Views 
	if ( isset( $options['show_views_on_meta'] ) && !empty( $options['show_views_on_meta'] ) && function_exists('dp_get_post_views') && !$dp_hide_views) {
		$views_code = '<span class="meta-views icon-eye">'.dp_get_post_views(get_the_ID(), null).'</span>';
	}

	// Author
	if ( isset( $options['show_author_on_meta'] ) && !empty( $options['show_author_on_meta'] ) && !$dp_hide_author ) {
		$author_code .= '<span class="meta-author icon-user vcard author"><a href="'.get_author_posts_url(get_the_author_meta('ID')).'" rel="author" title="'.__('Show articles of this user.', 'DigiPress').'" class="fn">'.get_the_author_meta('display_name').'</a></span>';
	}

	/***
	 * Second line
	 */
	// Add Comments + Views + Author
	$html_code .= $comment_code.$views_code.$author_code;

	/***
	 * Last Rounded
	 */
	$html_code = '<div class="postmetadata_archive">'.$html_code.'</div>';

	// Display
	echo $html_code;
}



/*******************************************************
* Meta content in single and archive that shows all
*******************************************************/
function showPostMetaForSingleTop(
						$postType = 'post', 
						$arrParams	= array(
							'show_date' 		=> true,
							'show_comments'		=> true,
							'show_views'		=> true,
							'show_cats'			=> true,
							'show_tags'			=> true,
							'show_author'		=> true,
							'show_sns_buttons'	=> true,
							'show_time_for_reding' => true
							)) { 

	if (post_password_required()) return;

	global $options, $post;

	$blank_flg = true;
	$html_code	= '';
	$additional_first_code 	= '';
	$additional_end_code 	= '';

	extract($arrParams);

	$postType = get_post_type();

	/****
	 * filter hook
	 */
	$additional_first_code = apply_filters('dp_single_meta_top_first', get_the_ID());
	if ( isset( $additional_first_code ) && !empty($additional_first_code) && $additional_first_code != get_the_ID()) {
		$html_code .= $additional_first_code;
		$blank_flg = false;
	}

	// Post date
	if ( ( ( is_single() && isset( $options['show_pubdate_on_meta'] ) && !empty( $options['show_pubdate_on_meta'] ) ) || (is_page() && isset( $options['show_pubdate_on_meta_page'] ) && !empty( $options['show_pubdate_on_meta_page'] ) ) ) && !get_post_meta(get_the_ID(), 'dp_hide_date', true) && isset( $options['show_date_under_post_title'] ) && !empty( $options['show_date_under_post_title'] ) && $show_date ) : 

		$blank_flg = false;

		if ( isset( $options['date_reckon_mode'] ) && !empty( $options['date_reckon_mode'] ) ) {
			$html_code .= publishedDiff();
		} else {
			$html_code .= '<span class="icon-calendar"><time datetime="'. get_the_date('c').'" class="published">'.get_the_date().'</time></span>';
		}
		
	endif;

	// Comment
	if (('open' == $post-> comment_status) && $show_comments && !is_page()) : 

		$blank_flg = false;

		$html_code .= '<span class="icon-comment">'. get_comments_popup_link(
							__('No Comments', 'DigiPress'), 
							__('Comment(1)', 'DigiPress'), 
							__('Comments(%)', 'DigiPress')).'</span>';
	endif; 

	// Views
	if (($postType === 'post') && isset( $options['show_views_on_meta'] ) && !empty( $options['show_views_on_meta'] ) && !get_post_meta(get_the_ID(), 'dp_hide_views', true) && isset( $options['show_views_under_post_title'] ) && !empty( $options['show_views_under_post_title'] ) && $show_views) : 

		$blank_flg = false;

		$html_code .= '<span class="icon-eye">'.dp_get_post_views(get_the_ID(), null).'</span>';
	endif;

	// Author
	if ((($postType === 'post' && isset( $options['show_author_on_meta'] ) && !empty( $options['show_author_on_meta'] ) ) || ($postType === 'page' && isset( $options['show_author_on_meta_page'] ) && !empty( $options['show_author_on_meta_page'] ) ) ) && isset( $options['show_author_under_post_title'] ) && !empty( $options['show_author_under_post_title'] ) && !get_post_meta(get_the_ID(), 'dp_hide_author', true) && $show_author) : 

		$blank_flg = false;

		$html_code .= '<span class="icon-user vcard author"><a href="'.get_author_posts_url(get_the_author_meta('ID')).'" rel="author" title="'.__('Show articles of this user.', 'DigiPress').'" class="fn">'.get_the_author_meta('display_name').'</a></span>';
	endif;

	// Edit link
	if (is_user_logged_in() && current_user_can('level_10')) {
		$html_code .= '| <a href="'.get_edit_post_link().'">Edit</a>';
	}

	// Time for reading
	if (($postType === 'post') && isset( $options['time_for_reading'] ) && !empty( $options['time_for_reading'] ) && $show_time_for_reding) {

		$blank_flg = false;

		$minutes = round(mb_strlen(strip_tags(get_the_content())) / 600) + 1;
		$time_for_reading = '<div class="dp_time_for_reading fl-r icon-alarm">' . sprintf( __('It takes about %s minute(s) to read this content.', 'DigiPress'), $minutes ) . '</div>'; 

		$html_code .= $time_for_reading;
	}

	// End of first row
	$html_code = '<div class="first_row">' . $html_code . '</div>';

	// --------------
	// Category
	if ($postType === 'post' && isset( $options['show_cat_on_meta'] ) && !empty( $options['show_cat_on_meta'] ) && isset( $options['show_cat_under_post_title'] ) && !empty( $options['show_cat_under_post_title'] ) && !get_post_meta(get_the_ID(), 'dp_hide_cat', true) && $show_cats && !is_mobile_dp()) : 

		$cats_code = '';

		$cats = get_the_category();
		if ( is_array( $cats ) && isset( $cats[0]->cat_ID ) ) {
			foreach ($cats as $cat) {
				$cats_code .= '<a href="'.get_category_link($cat->cat_ID).'" rel="tag">' .$cat->cat_name.'</a> ';
			}

			$cats_code = '<div class="icon-folder entrylist-cat">' .$cats_code. '</div>';
			$blank_flg = false;
		}
		$html_code .= $cats_code;
	endif;

	// SNS BUttons
	if ( !get_post_meta(get_the_ID(), 'hide_sns_icon', true) && isset( $options['sns_button_under_title'] ) && !empty( $options['sns_button_under_title'] ) && $show_sns_buttons && !is_mobile_dp()) :
		$blank_flg = false;
		$html_code .= dp_show_sns_buttons('top', false);
	endif;

	/****
	 * filter hook
	 */
	$additional_end_code = apply_filters('dp_single_meta_top_end', get_the_ID());
	if (!empty($additional_end_code) && $additional_end_code != get_the_ID()) {
		$html_code .= $additional_end_code;
		$blank_flg = false;
	}

	// Show source
	if (!$blank_flg) {
		$html_code = '<div class="postmeta_title">' . $html_code . '</div>';
		echo $html_code;
	}
}	// End function 


/*******************************************************
* Meta content for post meta
*******************************************************/
function showPostMetaForSingleBottom(
						$postType 	= 'post', 
						$arrParams	= array(
							'show_date' 		=> true,
							'show_comments'		=> true,
							'show_views'		=> true,
							'show_cats'			=> true,
							'show_tags'			=> true,
							'show_author'		=> true,
							'show_sns_buttons'	=> true,
							'show_time_for_reding' => true
							)) {
	if (post_password_required()) return;

	global $options, $post;

	$blank_flg 		= true;
	$html_code		= '';
	$lastUpdate 	= '';
	$additional_first_code = '';
	$additional_end_code = '';

	extract($arrParams);

	// Get post format
	$postFormat = get_post_format($post->ID);
	$postType 	= $postType ? $postType : get_post_type();

	/****
	 * filter hook
	 */
	$additional_first_code = apply_filters('dp_single_meta_bottom_first', get_the_ID());
	if (!empty($additional_first_code) && $additional_first_code != get_the_ID()) {
		$html_code .= $additional_first_code;
		$blank_flg = false;
	}

	if (!is_page()) {
		// if(function_exists('the_ratings')) the_ratings();
		if ($postType === 'post' && isset( $options['show_cat_on_meta'] ) && !empty( $options['show_cat_on_meta'] ) && isset( $options['show_cat_on_post_meta'] ) && !empty( $options['show_cat_on_post_meta'] ) && !get_post_meta(get_the_ID(), 'dp_hide_cat', true) && $show_cats) {

			$cats_code = '';

			$cats = get_the_category();
			if ( is_array( $cats ) && isset( $cats[0]->cat_ID ) ) {
				foreach ($cats as $cat) {
					$cats_code .= '<a href="'.get_category_link($cat->cat_ID).'" rel="tag">' .$cat->cat_name.'</a> ';
				}
				$cats_code = '<div class="icon-folder entrylist-cat">' .$cats_code. '</div>';
				$blank_flg = false;
			}
			$html_code .= $cats_code;
		}
	}

	//Show tags
	if ( isset( $options['show_tags'] ) && !empty( $options['show_tags'] ) && !get_post_meta(get_the_ID(), 'dp_hide_tag', true) && $show_tags ) { 
		$tags_code = '';

		$tags = get_the_tags();
		if ($tags) {
			foreach ($tags as $tag) {
				$tags_code .= '<a href="'.get_tag_link($tag->term_id).'" rel="tag" title="'.$tag->count.__(' topics of this tag.', 'DigiPress').'">'.$tag->name.'</a> ';
			}
			$tags_code = '<div class="icon-tags entrylist-cat">'.$tags_code.'</div>';
			$blank_flg = false;
		}
		$html_code .= $tags_code;
	}

	// Post Date
	if ( ( ( is_single() && isset( $options['show_pubdate_on_meta'] ) && !empty( $options['show_pubdate_on_meta'] ) ) || (is_page() && isset( $options['show_pubdate_on_meta_page'] ) && !empty( $options['show_pubdate_on_meta_page'] ) ) ) && !get_post_meta(get_the_ID(), 'dp_hide_date', true) && isset( $options['show_date_on_post_meta'] ) && !empty( $options['show_date_on_post_meta'] ) && $show_date) {
		$blank_flg = false;
		// Last update
		if ( isset( $options['show_last_update'] ) && !empty( $options['show_last_update'] ) && ( get_the_modified_date() != get_the_date()) ) {
			$lastUpdate =  '　<span content="'.get_the_modified_date('c').'" class="updated icon-update">'.get_the_modified_date().'</span>';
		} else {
			$lastUpdate = '';
		}
		$html_code .= '<span class="icon-calendar"><time datetime="'. get_the_date('c').'" class="published">'.get_the_date().'</time>'.$lastUpdate.'</span>';
	}

	// If comment and trackback is open
	if (('open' == $post-> comment_status) && $show_comments && !is_page()) {
		$blank_flg = false;
		$html_code .= '<span class="icon-edit"><a href="#respond">'.__('Comment', 'DigiPress').'</a></span><span class="icon-comment">'. get_comments_popup_link(
							__('No Comments', 'DigiPress'), 
							__('Comment(1)', 'DigiPress'), 
							__('Comments(%)', 'DigiPress')).'</span>';
	}

	// Views
	if (($postType === 'post' ) && isset( $options['show_views_on_meta'] ) && !empty( $options['show_views_on_meta'] ) && !get_post_meta(get_the_ID(), 'dp_hide_views', true) && isset( $options['show_views_on_post_meta'] ) && !empty( $options['show_views_on_post_meta'] ) && $show_views && function_exists('dp_get_post_views')) {
		$blank_flg = false;
		$html_code .= '<span class="icon-eye">'.dp_get_post_views(get_the_ID(), null).'</span>';
	}

	// Author
	if ( ( ( $postType === 'post' && isset( $options['show_author_on_meta'] ) && !empty( $options['show_author_on_meta'] ) ) || ( $postType === 'page' && isset( $options['show_author_on_meta_page'] ) && !empty( $options['show_author_on_meta_page'] ) ) ) && !get_post_meta(get_the_ID(), 'dp_hide_author', true) && isset( $options['show_author_on_post_meta'] ) && !empty( $options['show_author_on_post_meta'] ) && $show_author) {
		$blank_flg = false;
		$html_code .= '<span class="icon-user vcard author"><a href="'.get_author_posts_url(get_the_author_meta('ID')).'" rel="author" title="'.__('Show articles of this user.', 'DigiPress').'" class="fn">'.get_the_author_meta('display_name').'</a></span>';
	}

	// Edit link
	if (is_user_logged_in() && current_user_can('level_10')) {
		$html_code .= '| <a href="'.get_edit_post_link().'">Edit</a>';
	}

	// SNS BUttons
	if ( !get_post_meta(get_the_ID(), 'hide_sns_icon', true) && isset( $options['sns_button_on_meta'] ) && !empty( $options['sns_button_on_meta'] ) && $show_sns_buttons ) {
		$blank_flg = false;
		$html_code .= dp_show_sns_buttons('bottom', false);
	}

	/****
	 * filter hook
	 */
	$additional_end_code = apply_filters('dp_single_meta_bottom_end', get_the_ID());
	if (!empty($additional_end_code) && $additional_end_code != get_the_ID()) {
		$html_code .= $additional_end_code;
		$blank_flg = false;
	}

	if (!$blank_flg) {
		$html_code = '<footer class="postmeta_bottom">'.$html_code.'</footer>';
		echo $html_code;
	}
}	// End function


/*******************************************************
* Meta content in page at under the title
*******************************************************/
function showPostMetaForPageUnderTitle() { 
	if (post_password_required()) return;
	
	global $options, $post;

	$blank_flg = true;

	if ( get_post_meta(get_the_ID(), 'dp_hide_date', true ) && get_post_meta(get_the_ID(), 'dp_hide_author', true) && ( ( !isset( $options['sns_button_under_title'] ) || empty( $options['sns_button_under_title'] ) ) || get_post_meta(get_the_ID(), 'hide_sns_icon', true) ) ) {
		return;
	}?>
<div class="postmeta_title">
<div>
<?php if ( isset( $options['show_pubdate_on_meta_page'] ) && !empty( $options['show_pubdate_on_meta_page'] )  && !get_post_meta(get_the_ID(), 'dp_hide_date', true) && isset( $options['show_date_under_post_title'] ) && !empty( $options['show_date_under_post_title'] ) ) : ?>
<span class="icon-calendar"><time datetime="<?php the_time('c'); ?>" class="updated"><?php echo get_the_date(); ?></time></span>
<?php endif; ?>
<?php if ('open' == $post-> comment_status) : ?>
<span class="icon-comment"><?php comments_popup_link(
							__('No Comments', 'DigiPress'), 
							__('Comment(1)', 'DigiPress'), 
							__('Comments(%)', 'DigiPress')); ?></span>
<?php endif; ?>
<?php if ( isset( $options['show_author_on_meta_page'] ) && !empty( $options['show_author_on_meta_page'] ) && !get_post_meta(get_the_ID(), 'dp_hide_author', true) && isset( $options['show_author_under_post_title'] ) && !empty( $options['show_author_under_post_title'] ) ) : ?>
<span class="icon-user vcard author"><span class="fn"><?php the_author_posts_link(); ?></span></span>
<?php edit_post_link(__('Edit', 'DigiPress'), ' | '); ?>
<?php endif; ?>
</div>
<?php // SNS BUttons
	if ( ( get_post_type() === 'post' || get_post_type() === 'page') && isset( $options['sns_button_under_title'] ) && !empty( $options['sns_button_under_title'] ) ) :
		dp_show_sns_buttons('title');
	endif; ?>
</div>
<?php }	//End function


/*******************************************************
* Meta content in single and archive for MOBILE
*******************************************************/
function showPostMetaForArchiveMobile() {
	if (post_password_required()) return;

	global $options, $post;
	$postType = get_post_type();
?>
<div class="postmetadata_archive">
<?php 
	// Date
	if ( ($postType === 'post') && isset( $options['show_pubdate_on_meta'] ) && !empty( $options['show_pubdate_on_meta'] ) ) : 
?>
<time datetime="<?php the_time('c'); ?>" class="updated"><?php echo get_the_date(); ?></time>
<?php 
	endif;

	// Category
	if ( $postType === 'post' && isset( $options['show_cat_on_meta'] ) && !empty( $options['show_cat_on_meta'] ) ) {

		$cats_code		= '';
		$cats 			= get_the_category();

		if (isset( $cats ) && is_array( $cats ) ) {
			$cats_code = '<span class="plane-label">' .$cats[0]->cat_name. '</span>';
			echo $cats_code;
		}
	}
?>
</div><?php 
}	//End function 