<?php
// ----------------------------------------------------------------
// Custom init in admin panel 
// ----------------------------------------------------------------
function custom_admin_init() {
	if (!current_user_can('level_10')) {
		// Hide new WordPress version info
		add_filter('pre_site_transient_update_core', '__return_zero');

		// Disable version check connection with API.
		remove_action('wp_version_check', 'wp_version_check');
		remove_action('admin_init', '_maybe_update_core');

		// Remove welcome panel
		remove_action( 'welcome_panel', 'wp_welcome_panel' );
	}

	// Change editor user's role
	$role =& get_role('editor');
	$role->add_cap('edit_themes');
}
// add_action( 'admin_init', 'custom_admin_init');



// ----------------------------------------------------------------
// Add styles in admin panel
// ----------------------------------------------------------------
function dp_admin_print_styles(){
	if (current_user_can('level_10')) return;
	echo '<style type="text/css">
	.versions p,
	#wp-version-message, 
	td.b-posts, td.posts,
	td.b_pages, td.pages, 
	td.b-cats, td.cats,
	td.b-tags, td.tags,
	#contextual-help-link-wrap{display:none;}</style>';
}
// add_action('admin_print_styles', 'dp_admin_print_styles', 21);



// ----------------------------------------------------------------
// Import original CSS and javascript in admin panel
// ----------------------------------------------------------------
function dp_admin_enqueue($hook_suffix) {
	switch ($hook_suffix) {
		case 'post-new.php':
		case 'post.php':
			wp_enqueue_media();
			wp_enqueue_style('wp-color-picker');
			wp_enqueue_script('wp-color-picker');
			wp_enqueue_style('dp-admin-css', DP_THEME_URI . '/inc/css/dp-admin.css', null);
			wp_enqueue_script('dp_post_page', DP_THEME_URI . '/inc/js/dp_post_page.min.js', array('jquery', 'wp-color-picker'),DP_OPTION_SPT_VERSION,true);
			break;
		case 'widgets.php':
			wp_enqueue_style('dp_widgets_page', DP_THEME_URI . '/inc/css/dp_widgets_page.css', null);
			break;
		case 'edit.php':
			wp_enqueue_style('dp_admin_edit', DP_THEME_URI . '/inc/css/dp_admin_edit.css');
			wp_enqueue_script('dp_admin_edit', DP_THEME_URI . '/inc/js/dp_admin_edit.min.js', array('jquery'));
			break;
	}
}
add_action( 'admin_enqueue_scripts', 'dp_admin_enqueue' );


// ----------------------------------------------------------------
// Change footer text
// ----------------------------------------------------------------
function custom_admin_footer_text() {
	return;
}
// add_filter('admin_footer_text', 'custom_admin_footer_text');


// ----------------------------------------------------------------
// Admin bar items
// ----------------------------------------------------------------
function remove_admin_bar_menu() {
	if (current_user_can('level_10')) return;
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('wp-logo'); 		// W ロゴ
	// $wp_admin_bar->remove_menu('site-name');	// サイト名
	$wp_admin_bar->remove_menu('view-site'); 	// サイト名 -> サイトを表示
	$wp_admin_bar->remove_menu('comments'); 	// コメント
	$wp_admin_bar->remove_menu('new-content'); 	// 新規
	$wp_admin_bar->remove_menu('new-post'); 	// 新規 -> 投稿
	$wp_admin_bar->remove_menu('new-media'); 	// 新規 -> メディア
	$wp_admin_bar->remove_menu('new-link'); 	// 新規 -> リンク
	$wp_admin_bar->remove_menu('new-page'); 	// 新規 -> 固定ページ
	$wp_admin_bar->remove_menu('new-user'); 	// 新規 -> ユーザー
	$wp_admin_bar->remove_menu('updates'); 		// 更新
	$wp_admin_bar->remove_menu('my-account'); 	// マイアカウント
	$wp_admin_bar->remove_menu('user-info'); 	// マイアカウント -> プロフィール
	$wp_admin_bar->remove_menu('edit-profile'); // マイアカウント -> プロフィール編集
	$wp_admin_bar->remove_menu('logout'); 		// マイアカウント -> ログアウト
 }
// add_action( 'admin_bar_menu', 'remove_admin_bar_menu', 99 );


// ----------------------------------------------------------------
// Add logout menu
// ----------------------------------------------------------------
function add_new_item_in_admin_bar() {
	if (current_user_can('level_10')) return;
	global $wp_admin_bar;
	$wp_admin_bar->add_menu(array(
		'id' 	=> 'admin_bar_logout',
		'title' => __('Log out','DigiPress'),
		'href' 	=> wp_logout_url()
		)
	);
}
// add_action('wp_before_admin_bar_render', 'add_new_item_in_admin_bar');
 

// ----------------------------------------------------------------
// Remove dashboard widgets
// ----------------------------------------------------------------
function example_remove_dashboard_widgets() {
	if (current_user_can('level_10')) return;
 	global $wp_meta_boxes;
	//unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']); // 現在の状況
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // 最近のコメント
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']); // 被リンク
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']); // プラグイン
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']); // クイック投稿
	//unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']); // 最近の下書き
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']); // WordPressブログ
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']); // WordPressフォーラム
}
// add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets');



// ----------------------------------------------------------------
// Change menu title
// ----------------------------------------------------------------
function change_admin_left_menu_title() {
	global $menu;
	global $submenu;

	// $menu[5][0] = 'Blogs'; 
	$menu[10][0] = __('Upload', 'DigiPress'); // Media
}
// add_action( 'admin_menu', 'change_admin_left_menu_title' );


// ----------------------------------------------------------------
// Change menu order
// ----------------------------------------------------------------
function custom_menu_order($menu_ord) {
	if (!$menu_ord) return true;
	
	return array(
		'index.php', 				// Dashboard
		'separator1', 				// First separator
		'edit.php', 				// Posts
		'upload.php', 				// Media
		'link-manager.php', 		// Links
		'edit.php?post_type=page', 	// Pages
		'edit-comments.php', 		// Comments
		'separator2', 				// Second separator
		'themes.php', 				// Appearance
		'plugins.php', 				// Plugins
		'users.php', 				// Users
		'tools.php', 				// Tools
		'options-general.php', 		// Settings
		'separator-last', 			// Last separator
	);
}
// add_filter('custom_menu_order', 'custom_menu_order'); // Activate custom_menu_order
// add_filter('menu_order', 'custom_menu_order');



// ----------------------------------------------------------------
// Remove sidebar menu
// ----------------------------------------------------------------
function remove_admin_menus () {
	if (current_user_can('level_10')) return;
	// remove_menu_page('wpcf7'); //Contact Form 7
	global $menu;
	//unset($menu[2]); // ダッシュボード
	// unset($menu[4]); // メニューの線1
	// unset($menu[5]); // 投稿
	// unset($menu[10]); // メディア
	unset($menu[15]); // リンク
	// unset($menu[20]); // 固定ページ
	// unset($menu[25]); // コメント
	unset($menu[59]); // メニューの線2
	unset($menu[60]); // テーマ
	unset($menu[65]); // プラグイン
	//unset($menu[70]); // プロフィール
	unset($menu[75]); // ツール
	unset($menu[80]); // 設定
	unset($menu[90]); // メニューの線3
}
// add_action('admin_menu', 'remove_admin_menus');


// ----------------------------------------------------------------
// Visible menus
// ----------------------------------------------------------------
function dp_add_admin_menus () {
	add_theme_page(__('Menus','DigiPress'), __('Menus','DigiPress'), 'editor', 'nav-menus.php');
	add_theme_page(__('Widgets','DigiPress'), __('Widgets','DigiPress'), 'editor', 'widgets.php');
}
// add_action('admin_menu', 'dp_add_admin_menus');


// ----------------------------------------------------------------
// Remove update number in admin bar
// ----------------------------------------------------------------
function remove_before_admin_bar_render() {
	if (current_user_can('level_10')) return;
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu( 'updates' );
}
// add_action( 'wp_before_admin_bar_render', 'remove_before_admin_bar_render' );



// ----------------------------------------------------------------
// Remove post option items
// ----------------------------------------------------------------
// 投稿
function remove_default_post_screen_metaboxes() {
	if (current_user_can('level_10')) return;
	remove_meta_box( 'postexcerpt','post','normal' );       // 抜粋
	//remove_meta_box( 'trackbacksdiv','post','normal' );     // トラックバック送信
	remove_meta_box( 'postcustom','post','normal' );        // カスタムフィールド
	//remove_meta_box( 'commentstatusdiv','post','normal' );  // ディスカッション
	//remove_meta_box( 'commentsdiv','post','normal' );       // コメント
	//remove_meta_box( 'slugdiv','post','normal' );           // スラッグ
	remove_meta_box( 'authordiv','post','normal' );         // 作成者
	remove_meta_box( 'revisionsdiv','post','normal' );      // リビジョン
	//remove_meta_box( 'formatdiv','post','normal' );         // フォーマット
	//remove_meta_box( 'categorydiv','post','normal' );       // カテゴリー
	//remove_meta_box( 'tagsdiv-post_tag','post','normal' );  // タグ
}
// add_action('admin_menu','remove_default_post_screen_metaboxes');

// 固定ページ
function remove_default_page_screen_metaboxes() {
	if (current_user_can('level_10')) return;
	remove_meta_box( 'postcustom','page','normal' );        // カスタムフィールド
	remove_meta_box( 'commentstatusdiv','page','normal' );  // ディスカッション
	remove_meta_box( 'commentsdiv','page','normal' );       // コメント
	//remove_meta_box( 'slugdiv','page','normal' );           // スラッグ
	remove_meta_box( 'authordiv','page','normal' );         // 作成者
	remove_meta_box( 'revisionsdiv','page','normal' );      // リビジョン
}
// add_action('admin_menu','remove_default_page_screen_metaboxes');


// ----------------------------------------------------------------
// Hide profile items
// ----------------------------------------------------------------
// function custom_profile_fields( $contactmethods ) {
// 	if (current_user_can('level_10')) return;
// 	unset($contactmethods['aim']);     // AIM
// 	unset($contactmethods['yim']);     // Yahoo IM
// 	unset($contactmethods['jabber']);  // Jabber / Google Talk
// 	return $contactmethods;
// }
// add_filter('user_contactmethods','custom_profile_fields',10, 1);


// ----------------------------------------------------------------
// Remove screen options
// ----------------------------------------------------------------
function remove_screen_options(){
	if (!current_user_can('level_10')) return false;
}
// add_filter( 'screen_options_show_screen', 'remove_screen_options' );


// ----------------------------------------------------------------
// Add custom post type link in left menu
// ----------------------------------------------------------------
function show_custom_post_dashboard() {
	$dashboard_custom_post_types= Array(
		'news',
		'blog',
		'customers',
		'recommenders'
	);

	foreach($dashboard_custom_post_types as $custom_post_type) {
		global $wp_post_types;
		$num_post_type = wp_count_posts($custom_post_type);
		$num = number_format_i18n($num_post_type->publish);
		$text = _n( $wp_post_types[$custom_post_type]->labels->singular_name, $wp_post_types[$custom_post_type]->labels->name, $num_post_type->publish );
		$capability = $wp_post_types[$custom_post_type]->cap->edit_posts;

		if (current_user_can($capability)) {
			$num = "<a href='edit.php?post_type=" . $custom_post_type . "'>$num</a>";
			$text = "<a href='edit.php?post_type=" . $custom_post_type . "'>$text</a>";
		}

		echo '<tr>';
		echo '<td class="first b b_' . $custom_post_type . '">' . $num . '</td>';
		echo '<td class="t ' . $custom_post_type . '">' . $text . '</td>';
		echo '</tr>';
	}
}
// add_action( 'right_now_content_table_end', 'show_custom_post_dashboard' );


// ----------------------------------------------------------------
// Customize editor panel
// ----------------------------------------------------------------
/**
 * Customize editor panel
 */
// Enable original CSS in editor panel
function dp_theme_add_editor_styles() {
    add_editor_style('inc/css/editor_style.css');
}
add_action('admin_init', 'dp_theme_add_editor_styles');
/**
 * Add inline CSS in Visual editor
 */
function dp_tiny_mce_editor_inline_css($settings){
	// Target selectors
	$selector = array(
		'.mce-content-body.editor-area',	// Clasic Editor
		'div.editor-styles-wrapper .wp-block-freeform.block-library-rich-text__tinymce' // So do gutenberg
	);
	$inline_css = dp_generate_editor_inline_css( $selector );

	$settings['content_style'] = $inline_css;
	return $settings;
}
add_filter('tiny_mce_before_init', 'dp_tiny_mce_editor_inline_css');

/**
 * Enable style sheet for Gutenberg
 */
function dp_theme_enqueue_for_gutenberg() {
	
	$font_color_selector = array(
		'.editor-post-title__block .editor-post-title__input'
	);
	// Inline CSS
	$inline_css = dp_override_gutenberg_font_color( $font_color_selector );


	$wrapper_selector = array(
		'div.editor-styles-wrapper'
	);
	$inline_css .= dp_generate_editor_inline_css( $wrapper_selector );


	// Register inline CSS
	wp_register_style('dp-editor-insert', false);
	wp_enqueue_style('dp-editor-insert');
	wp_add_inline_style('dp-editor-insert', $inline_css);
}
add_action('enqueue_block_editor_assets', 'dp_theme_enqueue_for_gutenberg');

/**
 * Override gutenberg default font color
 */
function dp_override_gutenberg_font_color($arr_selector){
	if ( !(isset($arr_selector) && is_array($arr_selector)) ) return;
	global $options_visual;

	$selector = implode(',', $arr_selector);
	$inline_css = $selector."{color:".$options_visual['base_font_color'].";}";

	return $inline_css;
}

/**
 * Generate inline CSS for editor
 */
function dp_generate_editor_inline_css($arr_selector){
	if ( !(isset($arr_selector) && is_array($arr_selector)) ) return;
	global $options_visual;

	$inline_css = '';

	$rgb_base_font_color = dp_hex_to_rgb($options_visual['base_font_color']);

	foreach ($arr_selector as $selector){
		$inline_css .=
$selector."{
	background-color:".$options_visual['site_bg_color'].";
	color:".$options_visual['base_font_color'].";
	font-size:".$options_visual['base_font_size'].$options_visual['base_font_size_unit'].";
}".
$selector." p{
	font-size:".$options_visual['base_font_size'].$options_visual['base_font_size_unit'].";
}".

$selector." a{
	color:".$options_visual['base_link_color'].";
}".
$selector." a:hover{
	color:".$options_visual['base_link_hover_color'].";
}".
$selector." blockquote{
	background-color:rgba(".$rgb_base_font_color[0].",".$rgb_base_font_color[1].",".$rgb_base_font_color[2].",.04);
	border-color:rgba(".$rgb_base_font_color[0].",".$rgb_base_font_color[1].",".$rgb_base_font_color[2].",.08)!important;
}".
$selector." blockquote::before,".
$selector." blockquote::after{
	color:".$options_visual['base_link_color'].";
}".
$selector." table th,".
$selector." table td,".
$selector." dl,".
$selector." dt,".
$selector." dd{
	border-color:rgba(".$rgb_base_font_color[0].",".$rgb_base_font_color[1].",".$rgb_base_font_color[2].",.18);
}".
$selector." dt,".
$selector." table th{
	background-color:rgba(".$rgb_base_font_color[0].",".$rgb_base_font_color[1].",".$rgb_base_font_color[2].",.04);
}".
$selector." code:not(.rich-text){
	color:".$options_visual['base_font_color']."!important;
	background:rgba(".$rgb_base_font_color[0].",".$rgb_base_font_color[1].",".$rgb_base_font_color[2].",.05)!important;
	border-color:rgba(".$rgb_base_font_color[0].",".$rgb_base_font_color[1].",".$rgb_base_font_color[2].",.48)!important;
}".
$selector.' .btn:not([class*=btn-]),'.
$selector.' .label:not([class*=label-]),'.
$selector." ul:not(.recent_entries) li::before,".
$selector." ol li::before{
	background-color:".$options_visual['base_link_color'].";
}";
	}

	$inline_css = str_replace(array("\r\n","\r","\n","\t"), '', $inline_css);

	return $inline_css;
}

// Add custom button on Tiny MCE
if ( !function_exists( 'dp_tiny_mce_before_init' ) ):
	function dp_tiny_mce_before_init( $init_array ){
		$init_array['body_class'] = 'editor-area';

		// Default block formats
		$init_array['block_formats'] = __('Paragraph', 'DigiPress') . '=p;' . sprintf(__('Heading %s', 'DigiPress'), '1') . '=h1;' . sprintf(__('Heading %s', 'DigiPress'), '2') . '=h2;' . sprintf(__('Heading %s', 'DigiPress'), '3') . '=h3;' . sprintf(__('Heading %s', 'DigiPress'), '4') . '=h4;' . sprintf(__('Heading %s', 'DigiPress'), '5') . '=h5;' . sprintf(__('Heading %s', 'DigiPress'), '6') . '=h6;' . __('Address tag', 'DigiPress') . '=address;' . __('Preformatted text', 'DigiPress') . '=pre;' . __('Source code', 'DigiPress') . '=code';

		// Font size
		$init_array['fontsize_formats'] = '8px 9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px 21px 22px 23px 24px 25px 26px 27px 28px 29px 30px 31px 32px 33px 34px 35px 36px 37px 38px 39px 40px 110% 120% 130% 140% 150% 160% 170% 180% 190% 200% 210% 220% 230% 240% 250% 260% 270% 280% 290% 300%';


		// Overwrite default formats
		$default_formats = array(
			'alignleft' => array(
				'selector' =>'p,h1,h2,h3,h4,h5,h6,table,td,th,div,ul,ol,li,dl,dt,dd',
				'classes' => 'al-l',
				'styles' => ''
			),
			'alignright' => array(
				'selector' =>'p,h1,h2,h3,h4,h5,h6,table,td,th,div,ul,ol,li,dl,dt,dd',
				'classes' => 'al-r',
				'styles' => ''
			),
			'aligncenter' => array(
				'selector' =>'p,h1,h2,h3,h4,h5,h6,table,td,th,div,ul,ol,li,dl,dt,dd',
				'classes' => 'al-c',
				'styles' => ''
			)
		);
		$init_array['formats'] = json_encode($default_formats);


		// Original style code
		$style_formats = array(
			array(
				'title' => __('Bold', 'DigiPress'),
				'inline' => 'span',
				'icon' => 'bold',
				'classes' => 'b'
			),
			array(
				'title' => __('Italic', 'DigiPress'),
				'inline' => 'span',
				'icon' => 'italic',
				'classes' => 'i'
			),
			array(
				'title' => __('Serif style', 'DigiPress'),
				'inline' => 'span',
				'classes' => 'serif'
			),
			array(
				'title' => __('Font sizes', 'DigiPress'),
				'items' => array(
					array(
						'title' => '10px',
						'inline' => 'span',
						'classes' => 'ft10px'
					),
					array(
						'title' => '11px',
						'inline' => 'span',
						'classes' => 'ft11px'
					),
					array(
						'title' => '12px',
						'inline' => 'span',
						'classes' => 'ft12px'
					),
					array(
						'title' => '13px',
						'inline' => 'span',
						'classes' => 'ft13px'
					),
					array(
						'title' => '14px',
						'inline' => 'span',
						'classes' => 'ft14px'
					),
					array(
						'title' => '15px',
						'inline' => 'span',
						'classes' => 'ft15px'
					),
					array(
						'title' => '16px',
						'inline' => 'span',
						'classes' => 'ft16px'
					),
					array(
						'title' => '17px',
						'inline' => 'span',
						'classes' => 'ft17px'
					),
					array(
						'title' => '18px',
						'inline' => 'span',
						'classes' => 'ft18px'
					),
					array(
						'title' => '19px',
						'inline' => 'span',
						'classes' => 'ft19px'
					),
					array(
						'title' => '20px',
						'inline' => 'span',
						'classes' => 'ft20px'
					),
					array(
						'title' => '21px',
						'inline' => 'span',
						'classes' => 'ft21px'
					),
					array(
						'title' => '22px',
						'inline' => 'span',
						'classes' => 'ft22px'
					),
					array(
						'title' => '23px',
						'inline' => 'span',
						'classes' => 'ft23px'
					),
					array(
						'title' => '24px',
						'inline' => 'span',
						'classes' => 'ft24px'
					),
					array(
						'title' => '25px',
						'inline' => 'span',
						'classes' => 'ft25px'
					),
					array(
						'title' => '26px',
						'inline' => 'span',
						'classes' => 'ft26px'
					),
					array(
						'title' => '27px',
						'inline' => 'span',
						'classes' => 'ft27px'
					),
					array(
						'title' => '28px',
						'inline' => 'span',
						'classes' => 'ft28px'
					),
					array(
						'title' => '29px',
						'inline' => 'span',
						'classes' => 'ft29px'
					),
					array(
						'title' => '30px',
						'inline' => 'span',
						'classes' => 'ft30px'
					),
					array(
						'title' => '31px',
						'inline' => 'span',
						'classes' => 'ft31px'
					),
					array(
						'title' => '32px',
						'inline' => 'span',
						'classes' => 'ft32px'
					),
					array(
						'title' => '33px',
						'inline' => 'span',
						'classes' => 'ft33px'
					),
					array(
						'title' => '34px',
						'inline' => 'span',
						'classes' => 'ft34px'
					),
					array(
						'title' => '35px',
						'inline' => 'span',
						'classes' => 'ft35px'
					),
					array(
						'title' => '40px',
						'inline' => 'span',
						'classes' => 'ft40px'
					),
					array(
						'title' => '45px',
						'inline' => 'span',
						'classes' => 'ft45px'
					),
					array(
						'title' => '50px',
						'inline' => 'span',
						'classes' => 'ft50px'
					)
				)
			),
			array(
				'title' => __('Font color', 'DigiPress'),
				'icon' => 'forecolor',
				'items' => array(
					array(
						'title' => __('red', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'red'
					),
					array(
						'title' => __('blue', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'blue'
					),
					array(
						'title' => __('light blue', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'lightblue'
					),
					array(
						'title' => __('green', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'green'
					),
					array(
						'title' => __('yellow', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'yellow'
					),
					array(
						'title' => __('orange', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'orange'
					),
					array(
						'title' => __('pink', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'pink'
					)
				)
			),
			array(
				'title' => sprintf(__('Line marker %s', 'DigiPress'), ''),
				'items' => array(
					array(
						'title' => __('red', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'mk-red'
					),
					array(
						'title' => __('blue', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'mk-blue'
					),
					array(
						'title' => __('light blue', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'mk-lightblue'
					),
					array(
						'title' => __('green', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'mk-green'
					),
					array(
						'title' => __('yellow', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'mk-yellow'
					),
					array(
						'title' => __('orange', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'mk-orange'
					),
					array(
						'title' => __('pink', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'mk-pink'
					)
				)
			),
			array(
				'title' => sprintf(__('Underline %s', 'DigiPress'), ''),
				'icon' => 'underline',
				'items' => array(
					array(
						'title' => __('default', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'bd'
					),
					array(
						'title' => __('red', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'bd-red'
					),
					array(
						'title' => __('blue', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'bd-blue'
					),
					array(
						'title' => __('light blue', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'bd-lightblue'
					),
					array(
						'title' => __('green', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'bd-green'
					),
					array(
						'title' => __('yellow', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'bd-yellow'
					),
					array(
						'title' => __('orange', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'bd-orange'
					),
					array(
						'title' => __('pink', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'bd-pink'
					)
				)
			),
			array(
				'title' => sprintf(__('Box %s', 'DigiPress'), ''),
				'items' => array(
					array(
						'title' => __('default', 'DigiPress'),
						'block' => 'div',
						'classes' => 'box'
					),
					array(
						'title' => __('red', 'DigiPress'),
						'block' => 'div',
						'classes' => 'box-red'
					),
					array(
						'title' => __('blue', 'DigiPress'),
						'block' => 'div',
						'classes' => 'box-blue'
					),
					array(
						'title' => __('green', 'DigiPress'),
						'block' => 'div',
						'classes' => 'box-green'
					),
					array(
						'title' => __('yellow', 'DigiPress'),
						'block' => 'div',
						'classes' => 'box-yellow'
					),
					array(
						'title' => __('orange', 'DigiPress'),
						'block' => 'div',
						'classes' => 'box-orange'
					),
					array(
						'title' => __('pink', 'DigiPress'),
						'block' => 'div',
						'classes' => 'box-pink'
					)
				)
			),
			array(
				'title' => __('Label', 'DigiPress'),
				'items' => array(
					array(
						'title' => __('default', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'label'
					),
					array(
						'title' => __('red', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'label label-red'
					),
					array(
						'title' => __('blue', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'label label-blue'
					),
					array(
						'title' => __('light blue', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'label label-lightblue'
					),
					array(
						'title' => __('green', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'label label-green'
					),
					array(
						'title' => __('yellow', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'label label-yellow'
					),
					array(
						'title' => __('orange', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'label label-orange'
					),
					array(
						'title' => __('pink', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'label label-pink'
					),
					array(
						'title' => __('black', 'DigiPress'),
						'inline' => 'span',
						'classes' => 'label label-black'
					)
				)
			),
			array(
				'title' => __('Button', 'DigiPress'),
				'icon' => 'link',
				'items' => array(
					array(
						'title' => __('default', 'DigiPress'),
						'inline' => 'a',
						'attributes' => array(
							'href' => 'https://'
						),
						'classes' => 'btn',
						'onclick' => 'function(){
							editor.windowManager.open();
						}'
					),
					array(
						'title' => __('red', 'DigiPress'),
						'inline' => 'a',
						'attributes' => array(
							'href' => 'https://'
						),
						'classes' => 'btn btn-red',
						'onclick' => 'function(){
							editor.windowManager.open();
						}'
					),
					array(
						'title' => __('blue', 'DigiPress'),
						'inline' => 'a',
						'attributes' => array(
							'href' => 'https://'
						),
						'classes' => 'btn btn-blue',
						'onclick' => 'function(){
							editor.windowManager.open();
						}'
					),
					array(
						'title' => __('light blue', 'DigiPress'),
						'inline' => 'a',
						'attributes' => array(
							'href' => 'https://'
						),
						'classes' => 'btn btn-lightblue',
						'onclick' => 'function(){
							editor.windowManager.open();
						}'
					),
					array(
						'title' => __('green', 'DigiPress'),
						'inline' => 'a',
						'attributes' => array(
							'href' => 'https://'
						),
						'classes' => 'btn btn-green',
						'onclick' => 'function(){
							editor.windowManager.open();
						}'
					),
					array(
						'title' => __('yellow', 'DigiPress'),
						'inline' => 'a',
						'attributes' => array(
							'href' => 'https://'
						),
						'classes' => 'btn btn-yellow',
						'onclick' => 'function(){
							editor.windowManager.open();
						}'
					),
					array(
						'title' => __('orange', 'DigiPress'),
						'inline' => 'a',
						'attributes' => array(
							'href' => 'https://'
						),
						'classes' => 'btn btn-orange',
						'onclick' => 'function(){
							editor.windowManager.open();
						}'
					),
					array(
						'title' => __('pink', 'DigiPress'),
						'inline' => 'a',
						'attributes' => array(
							'href' => 'https://'
						),
						'classes' => 'btn btn-pink',
						'onclick' => 'function(){
							editor.windowManager.open();
						}'
					),
					array(
						'title' => __('black', 'DigiPress'),
						'inline' => 'a',
						'attributes' => array(
							'href' => 'https://'
						),
						'classes' => 'btn btn-black',
						'onclick' => 'function(){
							editor.windowManager.open();
						}'
					)
				)
			),
			array(
				'title' => __('Background color', 'DigiPress'),
				'icon' => 'backcolor',
				'items' => array(
					array(
						'title' => __('red', 'DigiPress'),
						'block' => 'div',
						'classes' => 'bg-red'
					),
					array(
						'title' => __('blue', 'DigiPress'),
						'block' => 'div',
						'classes' => 'bg-blue'
					),
					array(
						'title' => __('light blue', 'DigiPress'),
						'block' => 'div',
						'classes' => 'bg-lightblue'
					),
					array(
						'title' => __('green', 'DigiPress'),
						'block' => 'div',
						'classes' => 'bg-green'
					),
					array(
						'title' => __('yellow', 'DigiPress'),
						'block' => 'div',
						'classes' => 'bg-yellow'
					),
					array(
						'title' => __('orange', 'DigiPress'),
						'block' => 'div',
						'classes' => 'bg-orange'
					),
					array(
						'title' => __('pink', 'DigiPress'),
						'block' => 'div',
						'classes' => 'bg-pink'
					)
				)
			),
			array(
				'title' => __('Box padding', 'DigiPress'),
				'items' => array(
					array(
						'title' => __('top', 'DigiPress') . ' 10px',
						'block' => 'div',
						'classes' => 'pd10px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 15px',
						'block' => 'div',
						'classes' => 'pd15px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 20px',
						'block' => 'div',
						'classes' => 'pd20px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 25px',
						'block' => 'div',
						'classes' => 'pd25px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 30px',
						'block' => 'div',
						'classes' => 'pd30px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 35px',
						'block' => 'div',
						'classes' => 'pd35px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 40px',
						'block' => 'div',
						'classes' => 'pd40px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 45px',
						'block' => 'div',
						'classes' => 'pd45px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 50px',
						'block' => 'div',
						'classes' => 'pd50px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 10px',
						'block' => 'div',
						'classes' => 'pd10px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 15px',
						'block' => 'div',
						'classes' => 'pd15px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 20px',
						'block' => 'div',
						'classes' => 'pd20px-top'
					),
					array(
						'title' => __('bottom', 'DigiPress') . ' 25px',
						'block' => 'div',
						'classes' => 'pd25px-btm'
					),
					array(
						'title' => __('bottom', 'DigiPress') . ' 30px',
						'block' => 'div',
						'classes' => 'pd30px-btm'
					),
					array(
						'title' => __('bottom', 'DigiPress') . ' 35px',
						'block' => 'div',
						'classes' => 'pd35px-btm'
					),
					array(
						'title' => __('bottom', 'DigiPress') . ' 40px',
						'block' => 'div',
						'classes' => 'pd40px-btm'
					),
					array(
						'title' => __('bottom', 'DigiPress') . ' 45px',
						'block' => 'div',
						'classes' => 'pd45px-btm'
					),
					array(
						'title' => __('bottom', 'DigiPress') . ' 50px',
						'block' => 'div',
						'classes' => 'pd50px-btm'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 10px',
						'block' => 'div',
						'classes' => 'pd10px-l'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 15px',
						'block' => 'div',
						'classes' => 'pd15px-l'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 20px',
						'block' => 'div',
						'classes' => 'pd20px-l'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 25px',
						'block' => 'div',
						'classes' => 'pd25px-l'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 30px',
						'block' => 'div',
						'classes' => 'pd30px-l'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 35px',
						'block' => 'div',
						'classes' => 'pd35px-l'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 40px',
						'block' => 'div',
						'classes' => 'pd40px-l'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 45px',
						'block' => 'div',
						'classes' => 'pd45px-l'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 50px',
						'block' => 'div',
						'classes' => 'pd50px-l'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 10px',
						'block' => 'div',
						'classes' => 'pd10px-r'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 15px',
						'block' => 'div',
						'classes' => 'pd15px-r'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 20px',
						'block' => 'div',
						'classes' => 'pd20px-r'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 25px',
						'block' => 'div',
						'classes' => 'pd25px-r'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 30px',
						'block' => 'div',
						'classes' => 'pd30px-r'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 35px',
						'block' => 'div',
						'classes' => 'pd35px-r'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 40px',
						'block' => 'div',
						'classes' => 'pd40px-r'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 45px',
						'block' => 'div',
						'classes' => 'pd45px-r'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 50px',
						'block' => 'div',
						'classes' => 'pd50px-r'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 10px',
						'block' => 'div',
						'classes' => 'pd10px'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 15px',
						'block' => 'div',
						'classes' => 'pd15px'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 20px',
						'block' => 'div',
						'classes' => 'pd20px'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 25px',
						'block' => 'div',
						'classes' => 'pd25px'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 30px',
						'block' => 'div',
						'classes' => 'pd30px'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 35px',
						'block' => 'div',
						'classes' => 'pd35px'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 40px',
						'block' => 'div',
						'classes' => 'pd40px'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 45px',
						'block' => 'div',
						'classes' => 'pd45px'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 50px',
						'block' => 'div',
						'classes' => 'pd50px'
					)
				)
			),
			array(
				'title' => __('Box margin', 'DigiPress'),
				'items' => array(
					array(
						'title' => __('top', 'DigiPress') . ' 10px',
						'block' => 'div',
						'classes' => 'mg10px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 15px',
						'block' => 'div',
						'classes' => 'mg15px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 20px',
						'block' => 'div',
						'classes' => 'mg20px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 25px',
						'block' => 'div',
						'classes' => 'mg25px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 30px',
						'block' => 'div',
						'classes' => 'mg30px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 35px',
						'block' => 'div',
						'classes' => 'mg35px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 40px',
						'block' => 'div',
						'classes' => 'mg40px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 45px',
						'block' => 'div',
						'classes' => 'mg45px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 50px',
						'block' => 'div',
						'classes' => 'mg50px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 10px',
						'block' => 'div',
						'classes' => 'mg10px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 15px',
						'block' => 'div',
						'classes' => 'mg15px-top'
					),
					array(
						'title' => __('top', 'DigiPress') . ' 20px',
						'block' => 'div',
						'classes' => 'mg20px-top'
					),
					array(
						'title' => __('bottom', 'DigiPress') . ' 25px',
						'block' => 'div',
						'classes' => 'mg25px-btm'
					),
					array(
						'title' => __('bottom', 'DigiPress') . ' 30px',
						'block' => 'div',
						'classes' => 'mg30px-btm'
					),
					array(
						'title' => __('bottom', 'DigiPress') . ' 35px',
						'block' => 'div',
						'classes' => 'mg35px-btm'
					),
					array(
						'title' => __('bottom', 'DigiPress') . ' 40px',
						'block' => 'div',
						'classes' => 'mg40px-btm'
					),
					array(
						'title' => __('bottom', 'DigiPress') . ' 45px',
						'block' => 'div',
						'classes' => 'mg45px-btm'
					),
					array(
						'title' => __('bottom', 'DigiPress') . ' 50px',
						'block' => 'div',
						'classes' => 'mg50px-btm'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 10px',
						'block' => 'div',
						'classes' => 'mg10px-l'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 15px',
						'block' => 'div',
						'classes' => 'mg15px-l'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 20px',
						'block' => 'div',
						'classes' => 'mg20px-l'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 25px',
						'block' => 'div',
						'classes' => 'mg25px-l'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 30px',
						'block' => 'div',
						'classes' => 'mg30px-l'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 35px',
						'block' => 'div',
						'classes' => 'mg35px-l'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 40px',
						'block' => 'div',
						'classes' => 'mg40px-l'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 45px',
						'block' => 'div',
						'classes' => 'mg45px-l'
					),
					array(
						'title' => __('left', 'DigiPress') . ' 50px',
						'block' => 'div',
						'classes' => 'mg50px-l'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 10px',
						'block' => 'div',
						'classes' => 'mg10px-r'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 15px',
						'block' => 'div',
						'classes' => 'mg15px-r'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 20px',
						'block' => 'div',
						'classes' => 'mg20px-r'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 25px',
						'block' => 'div',
						'classes' => 'mg25px-r'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 30px',
						'block' => 'div',
						'classes' => 'mg30px-r'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 35px',
						'block' => 'div',
						'classes' => 'mg35px-r'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 40px',
						'block' => 'div',
						'classes' => 'mg40px-r'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 45px',
						'block' => 'div',
						'classes' => 'mg45px-r'
					),
					array(
						'title' => __('right', 'DigiPress') . ' 50px',
						'block' => 'div',
						'classes' => 'mg50px-r'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 10px',
						'block' => 'div',
						'classes' => 'mg10px'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 15px',
						'block' => 'div',
						'classes' => 'mg15px'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 20px',
						'block' => 'div',
						'classes' => 'mg20px'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 25px',
						'block' => 'div',
						'classes' => 'mg25px'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 30px',
						'block' => 'div',
						'classes' => 'mg30px'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 35px',
						'block' => 'div',
						'classes' => 'mg35px'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 40px',
						'block' => 'div',
						'classes' => 'mg40px'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 45px',
						'block' => 'div',
						'classes' => 'mg45px'
					),
					array(
						'title' => __('all around', 'DigiPress') . ' 50px',
						'block' => 'div',
						'classes' => 'mg50px'
					)
				)
			),
			array(
				'title' => __('Keyboard shortcut', 'DigiPress'),
				'inline' => 'span',
				'classes' => 'keyboard'
			),
			array(
				'title' => __('Formatted text', 'DigiPress'),
				'block' => 'pre',
				'classes' => ''
			),
			array(
				'title' => __('Code tag', 'DigiPress'),
				'block' => 'code',
				'classes' => ''
			)
		);
		//Parse to json
		$init_array['style_formats'] = json_encode($style_formats);
		$init_array['style_formats_merge'] = true;

		return $init_array;
	}
endif;
add_filter( 'tiny_mce_before_init', 'dp_tiny_mce_before_init' );

// Add the quick tags on text editor mode
if ( !function_exists( 'add_dp_custom_quicktags' ) ):
	function add_dp_custom_quicktags() {
		global $options;

		// Disable inserting quick tags
		if ( isset($options['disable_add_quick_tags']) && !empty($options['disable_add_quick_tags']) ) return;

		// Quick tags
		if (wp_script_is('quicktags')){
			$quicktags = "<script>
QTags.addButton('p','" . __('p (Paragraph)', 'DigiPress') . "','<p>','</p>');
QTags.addButton('br','" . __('br (Break)', 'DigiPress') . "','','<br />');
QTags.addButton('qt-dldtdd','" . __('Definition list', 'DigiPress') . "','<dl>\\r\\n<dt></dt>\\r\\n<dd>\\r\\n\\r\\n</dd>\\r\\n</dl>','');
QTags.addButton('qt-dl','" . __('dl', 'DigiPress') . "','<dl>','</dl>');
QTags.addButton('qt-dt','" . __('dt', 'DigiPress') . "','<dt>','</dt>');
QTags.addButton('qt-dd','" . __('dd', 'DigiPress') . "','<dd>','</dd>');
QTags.addButton('qt-table','" . __('table', 'DigiPress') . "','<table>\\r\\n<tbody>\\r\\n\\r\\n</tbody>\\r\\n</table>', '');
QTags.addButton('qt-tr','" . __('tr (Table row)', 'DigiPress') . "','<tr>','</tr>');
QTags.addButton('qt-th','" . __('th (Head cell)', 'DigiPress') . "','<th>','</th>');
QTags.addButton('qt-td','" . __('td  (cell)', 'DigiPress') . "','<td>','</td>');
QTags.addButton('qt-al-l-p','" . sprintf(__('Alignment %s : %s', 'DigiPress'), '(p)', __('left', 'DigiPress') ) . "','<p class=\"al-l\">','</p>');
QTags.addButton('qt-al-c-p','" . sprintf(__('Alignment %s : %s', 'DigiPress'), '(p)', __('center', 'DigiPress') ) . "','<p class=\"al-c\">','</p>');
QTags.addButton('qt-al-r-p','" . sprintf(__('Alignment %s : %s', 'DigiPress'), '(p)', __('right', 'DigiPress') ) . "','<p class=\"al-r\">','</p>');
QTags.addButton('qt-al-l-div','" . sprintf(__('Alignment %s : %s', 'DigiPress'), '(div)', __('left', 'DigiPress') ) . "','<div class=\"al-l\">','</div>');
QTags.addButton('qt-al-c-div','" . sprintf(__('Alignment %s : %s', 'DigiPress'), '(div)', __('center', 'DigiPress') ) . "','<div class=\"al-c\">','</div>');
QTags.addButton('qt-al-r-div','" . sprintf(__('Alignment %s : %s', 'DigiPress'), '(div)', __('right', 'DigiPress') ) . "','<div class=\"al-r\">','</div>');
QTags.addButton('qt-h1','" . __('h1', 'DigiPress') . "','<h1>','</h1>');
QTags.addButton('qt-h2','" . __('h2', 'DigiPress') . "','<h2>','</h2>');
QTags.addButton('qt-h3','" . __('h3', 'DigiPress') . "','<h3>','</h3>');
QTags.addButton('qt-h4','" . __('h4', 'DigiPress') . "','<h4>','</h4>');
QTags.addButton('qt-h5','" . __('h5', 'DigiPress') . "','<h5>','</h5>');
QTags.addButton('qt-h6','" . __('h6', 'DigiPress') . "','<h6>','</h6>');
QTags.addButton('qt-bold','" . __('Bold', 'DigiPress') . "','<span class=\"b\">','</span>');
QTags.addButton('qt-italic','" . __('Italic', 'DigiPress') . "','<span class=\"i\">','</span>');
QTags.addButton('qt-serif','" . __('Serif style', 'DigiPress') . "','<span class=\"serif\">','</span>');
QTags.addButton('qt-red','" . sprintf(__('Font %s', 'DigiPress'), __('red', 'DigiPress')) . "','<span class=\"red\">','</span>');
QTags.addButton('qt-blue','" . sprintf(__('Font %s', 'DigiPress'), __('blue', 'DigiPress')) . "','<span class=\"blue\">','</span>');
QTags.addButton('qt-green','" . sprintf(__('Font %s', 'DigiPress'), __('green', 'DigiPress')) . "','<span class=\"green\">','</span>');
QTags.addButton('qt-yellow','" . sprintf(__('Font %s', 'DigiPress'), __('yellow', 'DigiPress')) . "','<span class=\"yellow\">','</span>');
QTags.addButton('qt-orange','" . sprintf(__('Font %s', 'DigiPress'), __('orange', 'DigiPress')) . "','<span class=\"orange\">','</span>');
QTags.addButton('qt-pink','" . sprintf(__('Font %s', 'DigiPress'), __('pink', 'DigiPress')) . "','<span class=\"pink\">','</span>');
QTags.addButton('qt-mk-red','" . sprintf(__('Line marker %s', 'DigiPress'), __('red', 'DigiPress')) . "','<span class=\"mk-red\">','</span>');
QTags.addButton('qt-mk-blue','" . sprintf(__('Line marker %s', 'DigiPress'), __('blue', 'DigiPress')) . "','<span class=\"mk-blue\">','</span>');
QTags.addButton('qt-mk-green','" . sprintf(__('Line marker %s', 'DigiPress'), __('green', 'DigiPress')) . "','<span class=\"mk-green\">','</span>');
QTags.addButton('qt-mk-yellow','" . sprintf(__('Line marker %s', 'DigiPress'), __('yellow', 'DigiPress')) . "','<span class=\"mk-yellow\">','</span>');
QTags.addButton('qt-mk-orange','" . sprintf(__('Line marker %s', 'DigiPress'), __('orange', 'DigiPress')) . "','<span class=\"mk-orange\">','</span>');
QTags.addButton('qt-mk-pink','" . sprintf(__('Line marker %s', 'DigiPress'), __('pink', 'DigiPress')) . "','<span class=\"mk-pink\">','</span>');
QTags.addButton('qt-bd','" . sprintf(__('Underline %s', 'DigiPress'), '') . "','<span class=\"bd\">','</span>');
QTags.addButton('qt-bd-red','" . sprintf(__('Underline %s', 'DigiPress'), __('red', 'DigiPress')) . "','<span class=\"bd-red\">','</span>');
QTags.addButton('qt-bd-blue','" . sprintf(__('Underline %s', 'DigiPress'), __('blue', 'DigiPress')) . "','<span class=\"bd-blue\">','</span>');
QTags.addButton('qt-bd-green','" . sprintf(__('Underline %s', 'DigiPress'), __('green', 'DigiPress')) . "','<span class=\"bd-green\">','</span>');
QTags.addButton('qt-bd-yellow','" . sprintf(__('Underline %s', 'DigiPress'), __('yellow', 'DigiPress')) . "','<span class=\"bd-yellow\">','</span>');
QTags.addButton('qt-bd-orange','" . sprintf(__('Underline %s', 'DigiPress'), __('orange', 'DigiPress')) . "','<span class=\"bd-orange\">','</span>');
QTags.addButton('qt-bd-pink','" . sprintf(__('Underline %s', 'DigiPress'), __('pink', 'DigiPress')) . "','<span class=\"bd-pink\">','</span>');
QTags.addButton('qt-label','" . __('Label', 'DigiPress') . "','<span class=\"label\">','</span>');
QTags.addButton('qt-label-red','" . __('Label', 'DigiPress') . " " . __('red', 'DigiPress'). "','<span class=\"label label-red\">','</span>');
QTags.addButton('qt-label-blue','" . __('Label', 'DigiPress') . " " . __('blue', 'DigiPress'). "','<span class=\"label label-blue\">','</span>');
QTags.addButton('qt-label-green','" . __('Label', 'DigiPress') . " " . __('green', 'DigiPress'). "','<span class=\"label label-green\">','</span>');
QTags.addButton('qt-label-yellow','" . __('Label', 'DigiPress') . " " . __('yellow', 'DigiPress'). "','<span class=\"label label-yellow\">','</span>');
QTags.addButton('qt-label-orange','" . __('Label', 'DigiPress') . " " . __('orange', 'DigiPress'). "','<span class=\"label label-orange\">','</span>');
QTags.addButton('qt-label-pink','" . __('Label', 'DigiPress') . " " . __('pink', 'DigiPress'). "','<span class=\"label label-pink\">','</span>');
QTags.addButton('qt-button','" . __('Button', 'DigiPress') . "','<a href=\"\" class=\"btn\">','</a>');
QTags.addButton('qt-button-red','" . __('Button', 'DigiPress') . " " . __('red', 'DigiPress'). "','<a href=\"\" class=\"btn btn-red\">','</a>');
QTags.addButton('qt-button-blue','" . __('Button', 'DigiPress') . " " . __('blue', 'DigiPress'). "','<a href=\"\" class=\"btn btn-blue\">','</a>');
QTags.addButton('qt-button-green','" . __('Button', 'DigiPress') . " " . __('green', 'DigiPress'). "','<a href=\"\" class=\"btn btn-green\">','</a>');
QTags.addButton('qt-button-yellow','" . __('Button', 'DigiPress') . " " . __('yellow', 'DigiPress'). "','<a href=\"\" class=\"btn btn-yellow\">','</a>');
QTags.addButton('qt-button-orange','" . __('Button', 'DigiPress') . " " . __('orange', 'DigiPress'). "','<a href=\"\" class=\"btn btn-orange\">','</a>');
QTags.addButton('qt-button-pink','" . __('Button', 'DigiPress') . " " . __('pink', 'DigiPress'). "','<a href=\"\" class=\"btn btn-pink\">','</a>');
QTags.addButton('qt-box','" . sprintf(__('Box %s', 'DigiPress'), '') . "','<div class=\"box\">','</div>');
QTags.addButton('qt-box-red','" . sprintf(__('Box %s', 'DigiPress'), __('red', 'DigiPress')) . "','<div class=\"box-red\">','</div>');
QTags.addButton('qt-box-blue','" . sprintf(__('Box %s', 'DigiPress'), __('blue', 'DigiPress')) . "','<div class=\"box-blue\">','</div>');
QTags.addButton('qt-box-green','" . sprintf(__('Box %s', 'DigiPress'), __('green', 'DigiPress')) . "','<div class=\"box-green\">','</div>');
QTags.addButton('qt-box-yellow','" . sprintf(__('Box %s', 'DigiPress'), __('yellow', 'DigiPress')) . "','<div class=\"box-yellow\">','</div>');
QTags.addButton('qt-box-orange','" . sprintf(__('Box %s', 'DigiPress'), __('orange', 'DigiPress')) . "','<div class=\"box-orange\">','</div>');
QTags.addButton('qt-box-pink','" . sprintf(__('Box %s', 'DigiPress'), __('pink', 'DigiPress')) . "','<div class=\"box-pink\">','</div>');
QTags.addButton('keyboard','" . __('Keyboard shortcut', 'DigiPress') . "','<span class=\"keyboard\">','</span>');
QTags.addButton('pre','" . __('Formatted text', 'DigiPress') . "','<pre>','</pre>');
</script>";
			$quicktags = str_replace(array("\r\n","\r","\n","\t"), '', $quicktags);
			echo $quicktags;
		}
	}
endif;
add_action( 'admin_print_footer_scripts', 'add_dp_custom_quicktags', 99 );

// Add Tiny MCE buttons first row
if ( !function_exists( 'dp_mce_buttons' ) ):
	function dp_mce_buttons($buttons){
		// Remove buttons
		$remove = array('italic');
		// Add buttons
		array_push($buttons, "backcolor", "copy", "cut", "paste", "fontsizeselect", "fontselect", "cleanup");
		return array_diff($buttons,$remove);
	}
endif;
add_filter("mce_buttons", "dp_mce_buttons");

// Add original style box on Tiny MCE second row
if ( !function_exists( 'dp_mce_buttons_2' ) ):
	function dp_mce_buttons_2($buttons) {
		// Get the default select box
		$temp = array_shift($buttons);
		// Push the original styles
		array_unshift($buttons, 'styleselect');
		// Restore the select box
		array_unshift($buttons, $temp);

		return $buttons;
	}
endif;
add_filter('mce_buttons_2','dp_mce_buttons_2');

// Enable AddQuicktag in custom post type
function addquicktag_set_custom_post_type($post_types) {
	global $options;
	array_push($post_types, $options['news_cpt_slug_id']);
	return $post_types;
}
add_filter('addquicktag_post_types', 'addquicktag_set_custom_post_type');


// ----------------------------------------------------------------
// Add Column in posted list page
// ----------------------------------------------------------------
function add_posts_columns($columns) {
	$arr_fields = array(
		'thumbnail',
		'is_slideshow', 
		'is_headline',
		'dp_hide_title',
		'dp_hide_date',
		'dp_hide_author',
		'dp_hide_cat',
		'dp_hide_tag',
		'dp_hide_views',
		'dp_hide_fb_comment',
		'dp_hide_time_for_reading',
		'disable_sidebar',
		'dp_show_eyecatch_force'
	);

	foreach ($arr_fields as $key => $field) {
		$columns[$field] 		= __($field, 'DigiPress');
	}

	return $columns;
}
function show_posts_custom_column($column_name, $post_id) {
	global $options;

	$arr_post_type 	= array('post', 'page', $options['news_cpt_slug_id']);
	$pattern 		= '/'.implode('|', $arr_post_type).'/i';
	$match_type 	= preg_match($pattern, get_post_type($post_id));
	if (!$match_type) return;
	
	// ********************
	// Thumbnail column
	// ********************
	if ($column_name === 'thumbnail') {
		$thumb = get_the_post_thumbnail($post_id, array(90,90), 'thumbnail');
		// Display
		if ( isset($thumb) && $thumb ) {
			echo $thumb;
		} else {
			echo __('No Thumb', 'DigiPress');
		}
	}

	// ********************
	// Hidden columns
	// ********************
	// Check box items
	$arr_fields = array(
		'is_slideshow', 
		'is_headline',
		'dp_hide_title',
		'dp_hide_date',
		'dp_hide_author',
		'dp_hide_cat',
		'dp_hide_tag',
		'dp_hide_views',
		'dp_hide_fb_comment',
		'dp_hide_time_for_reading',
		'disable_sidebar',
		'dp_show_eyecatch_force'
	);

	// Only hidden check box
	foreach ($arr_fields as $key => $field) {
		if ( $column_name === $field ) {
			$val = (bool)get_post_meta( $post_id , $field , true );
			if ( $val ) {
				$checked = 'checked';
			} else {
				$checked = '';
			}
			echo "<input type='checkbox' readonly $checked />";
		}
	}
}
add_filter( 'manage_posts_columns', 'add_posts_columns' );
add_action( 'manage_posts_custom_column', 'show_posts_custom_column', 10, 2 );
add_filter( 'manage_pages_columns', 'add_posts_columns' );
add_action( 'manage_pages_custom_column', 'show_posts_custom_column', 10, 2 );

// ----------------------------------------------------------------
// Add custom field option into quick edit
// ----------------------------------------------------------------
function add_custom_field_into_quick_edit( $column_name, $post_type ) {
	global $options;

	$arr_post_type 	= array('post', 'page', $options['news_cpt_slug_id']);
	$pattern 		= '/'.implode('|', $arr_post_type).'/i';
	$match_type 	= preg_match($pattern, $post_type);
	if (!$match_type) return;

	static $print_nonce = true;
	if ( $print_nonce ) {
		$print_nonce = false;
		wp_nonce_field( 'quick_edit_action', $post_type . '_edit_nonce' ); // For CSRF
	}

	// Check box items
	$arr_fields = array(
		'is_slideshow' 			=> __('Include slideshow', 'DigiPress'),
		'is_headline'			=> __('Include headline', 'DigiPress'),
		'dp_hide_title'			=> __('Hide post title', 'DigiPress'),
		'dp_hide_date'			=> __('Hide post date', 'DigiPress'),
		'dp_hide_author'		=> __('Hide post author', 'DigiPress'),
		'dp_hide_cat'			=> __('Hide post categories', 'DigiPress'),
		'dp_hide_tag'			=> __('Hide post tags', 'DigiPress'),
		'dp_hide_views'			=> __('Hide post views', 'DigiPress'),
		'dp_hide_fb_comment'	=> __('Disable Facebook comments', 'DigiPress'),
		'dp_hide_time_for_reading'=> __('Hide time for reading', 'DigiPress'),
		'disable_sidebar'		=> __('Set to 1 column', 'DigiPress'),
		'dp_show_eyecatch_force'=> __('Show eyecatch under the title', 'DigiPress')
	);

	// HTML
	$field_code = '';
	foreach ($arr_fields as $key => $field) {
		if ( $column_name === $key ) {
			$field_code = '<fieldset class="inline-edit-col-center inline-custom-meta"><div class="inline-edit-col column-'.$key.'"><label class="inline-edit-group">';
			$field_code .= '<input type="checkbox" name="'.$key.'" /><span class="checkbox-title">' . $field . '</span>';
			$field_code .= '</label></div></fieldset>';
		}
	}

	echo $field_code;
}
add_action( 'quick_edit_custom_box', 'add_custom_field_into_quick_edit', 10, 2 );


// ----------------------------------------------------------------
// Save the custom field from quick edit
// ----------------------------------------------------------------
function save_custom_field_from_quick_edit( $post_id ) {
	global $options;

	$arr_post_type 	= array('post', 'page', $options['news_cpt_slug_id']);
	$pattern 		= '/'.implode('|', $arr_post_type).'/i';
	$match_type 	= preg_match($pattern, get_post_type($post_id));
	if (!$match_type) return;

	if ( !current_user_can( 'edit_post', $post_id ) ) return;
	$slug = get_post_type( $post_id ); // Target post type
	$_POST += array("{$slug}_edit_nonce" => '');
	if ( !wp_verify_nonce( $_POST["{$slug}_edit_nonce"], 'quick_edit_action' ) ) return;

	$arr_fields = array(
		'is_slideshow', 
		'is_headline',
		'dp_hide_title',
		'dp_hide_date',
		'dp_hide_author',
		'dp_hide_cat',
		'dp_hide_tag',
		'dp_hide_views',
		'dp_hide_fb_comment',
		'dp_hide_time_for_reading',
		'disable_sidebar',
		'dp_show_eyecatch_force');

	// If check box
	foreach ($arr_fields as $key => $field) {
		if ( isset( $_REQUEST[$field] ) ) {
			update_post_meta($post_id, $field, true);
		} else {
			update_post_meta($post_id, $field, false);
		}
	}
}
add_action( 'save_post', 'save_custom_field_from_quick_edit' );


/**
 * Fired on theme update completed
 */
function dp_upgrader_process_complete( $upgrader_object, $options ) {
	$current_theme = get_template();
	if ( $options['action'] === 'update' && $options['type'] === 'theme' ){

		dp_css_create();

		// foreach( (array)$options['themes'] as $theme ){
		// 	if ( $theme === $current_theme ){
		// 		// re-generate custom CSS
		// 		dp_css_create();
		// 	}
		// }
	}
}
add_action( 'upgrader_process_complete', 'dp_upgrader_process_complete', 10, 2 );

/**
 * Insert img tag to editor with resized image sizes for retina.
 * @param  string $html inserting img tag from media
 * @return string       New img tag with resized half attribute.
 */
function dp_image_send_to_editor_for_retina( $html ) {
	global $options;

	if ( !isset($options['retina_img_tag_to_editor']) || empty($options['retina_img_tag_to_editor'])) return $html;

	$html = preg_replace_callback( '/(width|height)="(\d*)"\s/',
		function($m){
			return $m[1] . '="' . round($m[2] / 2) . '" ';
		},
		$html
	);
	return $html;
}
add_filter( 'image_send_to_editor', 'dp_image_send_to_editor_for_retina' );