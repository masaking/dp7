<?php
/** ===================================================
* Echo slider Javascript
*
* @return	
*/
function make_slider_js($arrImages, $arrTitles, $arrUrls) {
	$options_visual = get_option('dp_options_visual') || [];

	$transitionEffect = $slideDirection = $slideShowSpeed = $nextSlideDelay = '';

	if ( isset( $options_visual['dp_slideshow_effect'] ) && isset( $options_visual['dp_slideshow_direction'] ) && isset( $options_visual['dp_slideshow_transition_time'] ) && isset( $options_visual['dp_slideshow_speed'] ) ) {
		$transitionEffect = "transitionEffect:'".$options_visual['dp_slideshow_effect']."',";
		$slideDirection    = "slideDirection:'".$options_visual['dp_slideshow_direction']."',";
		$slideShowSpeed	   = "slideShowSpeed:".$options_visual['dp_slideshow_transition_time'].",";
		$nextSlideDelay  = "nextSlideDelay:".$options_visual['dp_slideshow_speed'].",";
	}

	$images = $titles = $urls = $order = '';

	if ( isset( $options_visual['dp_slideshow_effect'] ) && $options_visual['dp_slideshow_effect'] == 'fade') {
		$slideDirection = '';
	}

	if ( isset( $options_visual['dp_slideshow_order'] ) && !empty( $options_visual['dp_slideshow_order'] ) ) {
		switch ( $options_visual['dp_slideshow_order']) {
			case 'date':
				$order = "sequenceMode:'normal',";
				break;
			case 'random':
				$order = "sequenceMode:'random',";
				break;
			default:
			 	$order = "sequenceMode:'normal',";
			 	break;
		}
	}

	foreach ($arrImages as $image) {
		$images .= '"' . $image . '",';
	}

	foreach ($arrTitles as $title) {
		$titles .= '"' . $title . '",';
	}

	foreach ($arrUrls as $url) {
		$urls .= '"' . $url . '",';
	}
	
	if ($arrTitles && $arrUrls) {
		$js_code =
"<script>
j$(document).ready(function(){
	j$('#site_title').bgStretcher({
		images:[" . $images . "],
		titles:[" . $titles . "],
		urls:[" . $urls . "],
		imageWidth:980, 
		imageHeight:650, 
		" . $slideDirection . $nextSlideDelay . $slideShowSpeed . $transitionEffect . $order . "
		buttonPrev:'#str_nav_prev',
		buttonNext:'#str_nav_next',
		pagination:'#stretcher_nav',
		callbackfunction: 'showHeaderContents'
	});
})
</script>";
	} else {
	$js_code =
"<script>
j$(document).ready(function(){
	j$('#site_title').bgStretcher({
		images:[" . $images . "],
		imageWidth:980, 
		imageHeight:650, 
		" . $slideDirection . $nextSlideDelay . $slideShowSpeed . $transitionEffect . $order . "
		buttonPrev:'#str_nav_prev',
		buttonNext:'#str_nav_next',
		pagination:'#stretcher_nav',
		callbackfunction: 'showHeaderContents'
	});
})
</script>";
	}

	$js_code = str_replace(array("\r\n","\r","\n","\t"), '', $js_code);

	return $js_code;
}

/** ===================================================
* Create slideshow source
*
*/
function getSlideshowSource($width = 980, $height = 650) {
	//Get options
	$options 		= get_option('dp_options');
	$options_visual = get_option('dp_options_visual');

	$num 		= isset( $options_visual['dp_number_of_slideshow'] ) ? $options_visual['dp_number_of_slideshow'] : 4;
	$orderby 	= isset( $options_visual['dp_slideshow_order'] ) ? $options_visual['dp_slideshow_order'] : '';

	$img_url		= get_post_meta(get_the_ID(), 'slideshow_image_url');
	$post_desc		= get_post_meta(get_the_ID(), 'slideshow_description');

	$arrImages	= array();
	$arrTitles	= array();
	$arrUrls 	= array();

	if ( isset( $options_visual['dp_slideshow_type'] ) && $options_visual['dp_slideshow_type'] === 'post') {
		global $post;

		// Slideshow type is POST
		// Query
		$posts = get_posts(  array(
								'numberposts'	=> $num,
								'meta_key'		=> 'is_slideshow',
								'meta_value'	=> array("true", true),
								'orderby'		=> $orderby // or rand
								)
		);
		// Loop query posts
		foreach( $posts as $post ) : setup_postdata($post);
			$slide_img_url 	= get_post_meta(get_the_ID(), 'slideshow_image_url');

			if ($slide_img_url[0]) {
				// Add image
				array_push($arrImages, $slide_img_url[0]);
			} else {
				if(has_post_thumbnail()) {
					$image_id = get_post_thumbnail_id();
					$image_url = wp_get_attachment_image_src($image_id, array($width, $height), true); 
					// Add image
					array_push($arrImages, $image_url[0]);
				} else {
					preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"]/i', get_post(get_the_ID())->post_content, $imgurl);
					if ($imgurl[1][0]) {
						// Add image
						array_push($arrImages, $imgurl[1][0]);
					} else {
						// Get images
						$images = dp_get_uploaded_images("header");
						$images = $images[0];
						
						if ( count($images) > 0 ) {
							$arrImages = $images;
						}
					}
				}
			}
			//Titile
			array_push($arrTitles, get_the_title());
			//URL
			array_push($arrUrls, get_permalink());
		endforeach;

	} else {
		// Get images
		$images = dp_get_uploaded_images("header");
		$images = $images[0];
		$cnt = count($images);

		if (0 < $cnt && $cnt <= $num) {
			$arrImages = $images;
		} else if ($cnt > $num) {
			for ($i=0; $i < 7; ++$i) { 
				array_push($arrImages, $images[$i]);	
			}
		}
	}

	// SSL
	$arrImages = is_ssl() ? str_replace('http:', 'https:', $arrImages) : $arrImages;

	// Display
	echo make_slider_js($arrImages, $arrTitles, $arrUrls);
}


/** ===================================================
* Show the Banner Contents
*
* @return	none
*/
function dp_banner_contents() {
	//Get options
	$options 		= get_option('dp_options');
	$options_visual = get_option('dp_options_visual');
	$type			= isset( $options_visual['dp_header_content_type'] ) ? $options_visual['dp_header_content_type'] : '' ;
	$headerImgFixed = isset( $options_visual['dp_header_img_fixed'] ) && !empty( $options_visual['dp_header_img_fixed'] ) ? 'background-attachment:fixed;background-position-y:44px;' : '';
	$banner_contents	= '';

	if (is_front_page() && is_home() && !is_paged() && !isset($_REQUEST['q']) && !is_search() ) :
		// Top page
		switch ($type) {
			case 1:	// Header image
				if ( isset( $options_visual['dp_header_img'] ) && !empty( $options_visual['dp_header_img'] ) ) {
					if ($options_visual['dp_header_img'] === 'random') {
						// Get images
						$images = dp_get_uploaded_images("header");
						$images = $images[0];
						$images = is_ssl() ? str_replace('http:', 'https:', $images) : $images;
						$cnt = count($images);

						if ($cnt > 0) {
							//repeat
							$repeat = $options_visual['dp_header_repeat'];
							if ($repeat == 'no-repeat') {
								$repeat = ' '.$repeat;
							} else {
								$repeat = ' left top '.$repeat;
							}
							//show image
							$rnd = rand(0, $cnt - 1);
							$banner_contents = '<div id="site_title" style="background:url('.$images[$rnd].')'.$repeat.';background-size:100% auto;' . $headerImgFixed . '">';
						}

					} else {
						// Normal header image
						$banner_contents = '<div id="site_title">';
					}
					echo $banner_contents;
				}
				break;

			case 2:	// Slideshow
				$banner_contents = '<div id="site_title">';
				echo $banner_contents;
				break;
		}
		?>
<?php 
		if ( isset( $options_visual['header_area_low_height'] ) && !empty( $options_visual['header_area_low_height'] ) ) : ?>
<div id="header_container_half">
<?php 
		else: ?>
<div id="header_container">
<?php 
		endif; ?>
<div id="header_left"><div id="h_area"><div class="hgroup">
<h1><a href="<?php echo home_url(); ?>/" title="<?php bloginfo('name'); ?>">
		<?php
		if ( isset( $options_visual['h1title_as_what'] ) && $options_visual['h1title_as_what'] !== 'image') {
			echo dp_h1_title();
		} else {
			if ( isset( $options_visual['dp_title_img'] ) && !empty( $options_visual['dp_title_img'] ) ) {
				$image_url = is_ssl() ? str_replace('http:', 'https:', $options_visual['dp_title_img']) : $options_visual['dp_title_img'];
				echo '<img src="'.$image_url.'" alt="'.dp_h1_title().'" />';
			}
		}?>
</a></h1>
<?php echo dp_h2_title('<h2>', '</h2>'); ?>
</div>
<?php 
		if ( isset( $options['enable_my_desc'] ) && !empty( $options['enable_my_desc'] ) ) : ?>
<div id="site_banner_desc"><span><?php echo dp_site_desc(); ?></span></div>
<?php 
		endif; ?>
</div>
		<?php // Header left widget
		if (is_active_sidebar('widget-top-left-bottom')) : ?>
<div id="top-left-bottom-widget" class="clearfix">
<?php dynamic_sidebar( 'widget-top-left-bottom' ); ?>
</div>
<?php 
		endif; ?>
</div>
<?php // Header right widget
		if (is_active_sidebar('widget-top-right')) : ?>
<div id="top-right-widget" class="clearfix">
<?php dynamic_sidebar( 'widget-top-right' ); ?>
</div>
<?php 
		endif; ?>
</div>
</div>
<?php 
	else : // is_front_page() && is_home() && !is_paged() && !isset($_REQUEST['q'] 
?>
<div id="header_container_paged" class="clearfix">
<div class="hgroup">
<h1><a href="<?php echo home_url(); ?>/" title="<?php bloginfo('name'); ?>">
		<?php
		if ( isset( $options_visual['h1title_as_what'] ) && $options_visual['h1title_as_what'] !== 'image') {
			echo dp_h1_title();
		} else {
			if ( isset( $options_visual['dp_title_img_paged'] ) && !empty( $options_visual['dp_title_img_paged'] ) ) {
				$image_url = is_ssl() ? str_replace('http:', 'https:', $options_visual['dp_title_img_paged']) : $options_visual['dp_title_img_paged'];
				echo '<img src="'.$options_visual['dp_title_img_paged'].'" alt="'.dp_h1_title().'" />';
			}
		}?>
</a></h1>
<?php 	// Echo H2 title
		if ( isset( $options_visual['h1title_as_what'] ) && $options_visual['h1title_as_what'] != 'image') :
			echo dp_h2_title('<h2>', '</h2>');
		endif;
?>
</div>
<?php 
		// Header right widget
		if (is_active_sidebar('widget-header-right-not-home')) : 
			dynamic_sidebar( 'widget-header-right-not-home' );
		endif;
?>
</div>
<?php 
	endif; // End of "iis_front_page() && is_home() && !is_paged() && !isset($_REQUEST['q']"
}	// End of function ?>
<?php
/** ===================================================
* Show the Banner Contents
*
* @return	none
*/
function dp_banner_contents_mobile() {
	//Get options
	$options_visual = get_option('dp_options_visual');
?>
<div id="header_container_paged">
<div class="hgroup">
<h1><a href="<?php echo home_url(); ?>/" title="<?php bloginfo('name'); ?>">
		<?php
		if ( isset( $options_visual['h1title_as_what'] ) && $options_visual['h1title_as_what'] !== 'image') {
			echo dp_h1_title();
		} else {
			if ( isset( $options_visual['dp_title_img_mobile'] ) && !empty( $options_visual['dp_title_img_mobile'] ) ) {
				$image_url = is_ssl() ? str_replace('http:', 'https:', $options_visual['dp_title_img_mobile']) : $options_visual['dp_title_img_mobile'];
				echo '<img src="'.$options_visual['dp_title_img_mobile'].'" alt="'.dp_h1_title().'" />';
			}
		}?>
</a></h1>
<?php 	// Echo H2 title
		if ( isset( $options_visual['h1title_as_what'] ) && $options_visual['h1title_as_what'] != 'image') :
			echo dp_h2_title('<h2>', '</h2>');
		endif;?>
</div>
</div>
<?php 
} // End of function