<?php
/**
 * Return the current URL
 * @return String Current URL
 */
function dp_get_current_link() {
	return (is_ssl() ? 'https' : 'http') . '://' . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"];
}

/**
 * Post info formatted json-ld
 * @return String single page json-ld
 */
function dp_json_ld(){

	if ( class_exists('All_in_One_SEO_Pack') || function_exists( 'aioseo' ) || function_exists( 'YoastSEO' ) ) return;

	$json_ld_code = '';

	if (is_single() || is_page()) :
		while (have_posts()) : the_post();
			global $options;
			$type = 'BlogPosting';
			$article_section = '';
			if (is_page()){
				$type = 'WebPageElement';
			} else if (is_single()){
				$cats = get_the_category();
				if (isset($cats[0])){
					$article_section = '"articleSection":"'.$cats[0]->cat_name.'",';
				}
			}
			// Get the post thumbnail
			$image_url = '';
			$image_path = '';
			$image_size = null;
			$image_w = 696;
			$image_h = 0;
			$logo_url = '';
			$logo_path = '';
			$logo_w = 600;
			$logo_h = 60;
			$is_ssl = is_ssl();

			// Thumnbnail
			if (has_post_thumbnail()) {
				$image_id = get_post_thumbnail_id();
				$image_data = wp_get_attachment_image_src($image_id, array(1200,800), true);
				if (is_array($image_data)) {
					$image_url = $is_ssl ? str_replace('http:', 'https:', $image_data[0]) : $image_data[0];
					$image_w = ($image_data[1] < $image_w) ? $image_w : $image_data[1];
					$image_h = $image_data[2];
				}
			} else {
				$image_url = DP_THEME_URI . '/img/post_thumbnail/noimage.png';
				$image_path = DP_THEME_DIR . '/img/post_thumbnail/noimage.png';
				$image_size = dp_get_image_size($image_path);
				if ( isset( $image_size[0] ) && isset( $image_size[1] ) ) {
					$image_w = ($image_size[0] < $image_w) ? $image_w : $image_size[0];
					$image_h = $image_size[1];
				}
			}

			// Logo image
			if (isset($options['json_ld_logo_img_url']) && !empty($options['json_ld_logo_img_url'])){
				$logo_url = $options['json_ld_logo_img_url'];
			} else {
				if (!empty($options['dp_title_img'])) {
					$logo_url = $options['dp_title_img'];
					$logo_url = $is_ssl ? str_replace('http:', 'https:', $logo_url) : $logo_url;
				} else {
					if (!empty($options['meta_ogp_img_url'])){
						$logo_url = $options['meta_ogp_img_url'];
					} else {
						$logo_url = DP_THEME_URI.'/img/json-ld/nologo.png';
					}
				}
			}
			$image_size = dp_get_image_size($logo_url);
			if ( isset( $image_size[0] ) && isset( $image_size[1] ) ) {
				$logo_w = ($image_size[0] > $logo_w) ? $logo_w : $image_size[0];
				$logo_h = ($image_size[1] > $logo_h) ? $logo_h : $image_size[1];
			}

			// json ld
			$json_ld_code = 
'<script type="application/ld+json">{
	"@context":"http://schema.org",
	"@type":"'.$type.'",
	"mainEntityOfPage":{
		"@type":"WebPage",
		"@id":"'.get_permalink().'"
	},
	"headline":"' . strip_tags( stripslashes( the_title('','',false) ) ) . '",
	"image":{
		"@type":"ImageObject",
		"url":"'.$image_url.'",
		"width":'.$image_w.',
		"height":'.$image_h.'
	},
	"datePublished":"'.get_the_date('c').'",
	"dateModified":"'.get_the_modified_date('c').'",
	'.$article_section.'
	"author":{
		"@type":"Person",
		"name":"'.get_the_author_meta('display_name').'"
	},
	"publisher":{
		"@type":"Organization",
		"name":"'.get_bloginfo('name').'",
		"logo":{
			"@type":"ImageObject",
				"url":"'.$logo_url.'",
				"width":'.$logo_w.',
				"height":'.$logo_h.'
		}
	},
	"description":"'.mb_substr(esc_attr(strip_tags(get_the_excerpt())), 0, 200, 'utf-8').'"
}</script>';
			$json_ld_code = str_replace(array("\r\n","\r","\n","\t","/^\s(?=\s)/"), '', $json_ld_code);
		endwhile;
	endif;

	// Breadcrumb
	$json_ld_code .= dp_breadcrumb_json_ld();
	// Website
	$json_ld_code .= dp_website_json_ld();

	echo $json_ld_code;

}

/**
 * Create breadcrumb as JSON-LD
 */
function dp_breadcrumb_json_ld() {

	if (is_admin())return;
	if (is_front_page() && !is_paged()) return;

	global $options;

	$position = 1;

	$code = '';
	$arr_json_ld = array();

	if (is_search()) {							// Search result
		$name = isset( $_REQUEST['q']) ? $_GET['q'] : get_search_query();

		if (is_paged()) {
			$name = $name . ' ( ' . get_query_var('paged') . ' )';
		}

		$url = dp_get_current_link();
		$arr_json_ld[] =
'{
	"@type":"ListItem",
	"position":' . $position . ',
	"name":"' . $name . '",
	"item":"' . $url . '"
}';
		$position++;

	} else if (is_archive()){
		// Category
		if (is_category()) {

			// Get the category object
			$cat = get_queried_object();

			// If child category
			if ($cat->parent !== 0){
				// Get the parent object
				$ancestors = array_reverse(get_ancestors( $cat->cat_ID, 'category' ));
				foreach ($ancestors as $ancestor){
					$arr_json_ld[] =
'{
	"@type":"ListItem",
	"position":' . $position . ',
	"name":"' . stripslashes( get_cat_name($ancestor) ) . '",
	"item":"' . get_category_link($ancestor) . '"
}';

					$position++;
				}
			}

			// Current category name
			$name = stripslashes( $cat->name );

			// Paged
			if (is_paged()) {
				$name .= ' ( ' . get_query_var('paged') . ' )';
			}

			$arr_json_ld[] =
'{
	"@type":"ListItem",
	"position":' . $position . ',
	"name":"' . $name . '",
	"item":"' . dp_get_current_link() . '"
}';

			$position++;

		} elseif (is_date()){

			// Date Archive
			if (get_query_var('day') != 0){	// Archive of the day

				// Year
				$arr_json_ld[] =
'{
	"@type":"ListItem",
	"position":' . $position . ',
	"name":"' . get_query_var('year') . '",
	"item":"' . get_year_link(get_query_var('year')) . '"
}';
				$position++;

				// JSON-LD
				$arr_json_ld[] =
'{
	"@type":"ListItem",
	"position":' . $position . ',
	"name":"' . get_query_var('monthnum') . '",
	"item":"' . get_month_link(get_query_var('year'), get_query_var('monthnum')) . '"
}';
				$position++;

				// Day
				$arr_json_ld[] =
'{
	"@type":"ListItem",
	"position":' . $position . ',
	"name":"' . get_query_var('day') . '",
	"item":"' . get_day_link(get_query_var('year'), get_query_var('monthnum'), get_query_var('day')) . '"
}';
				$position++;

			} elseif (get_query_var('monthnum') != 0){	// Archive of the month
				// Year
				$arr_json_ld[] =
'{
	"@type":"ListItem",
	"position":' . $position . ',
	"name":"' . get_query_var('year') . '",
	"item":"' . get_year_link(get_query_var('year')) . '"
}';
				$position++;

				// Current month
				$name = get_query_var('monthnum');
				// Initialize
				$url = '';

				// Paged
				if (is_paged()) {
					$name .= ' ( ' . get_query_var('paged') . ' )';
					$url = dp_get_current_link();
				} else {
					$url = get_month_link( get_query_var('year'), $name );
				}

				// JSON-LD
				$arr_json_ld[] =
'{
	"@type":"ListItem",
	"position":' . $position . ',
	"name":"' . $name . '",
	"item":"' . $url . '"
}';
				$position++;

			} else {	// Archive of the year
				// Current year
				$name = get_query_var('year');
				// Initialize
				$url = '';

				// Paged
				if (is_paged()) {
					$name .= ' ( ' . get_query_var('paged') . ' )';
					$url = dp_get_current_link();
				} else {
					$url = get_year_link( $name );
				}

				// JSON-LD
				$arr_json_ld[] =
'{
	"@type":"ListItem",
	"position":' . $position . ',
	"name":"' . $name . '",
	"item":"' . $url . '"
}';
				$position++;
			}

		} elseif (is_author()){
			// Author name
			$name = get_the_author_meta('display_name', get_query_var('author'));

			// Initialize
			$url = '';

			// Paged
			if (is_paged()) {
				$name .= ' ( ' . get_query_var('paged') . ' )';
				$url = dp_get_current_link();
			} else {
				$url = get_author_posts_url( get_the_author_meta('ID') );
			}

			// JSON-LD
			$arr_json_ld[] =
'{
"@type":"ListItem",
"position":' . $position . ',
"name":"' . $name . '",
"item":"' . $url . '"
}';
			$position++;

		} elseif (is_tag()){
			// Tag archive name
			$name = single_tag_title( '' , false );

			// Initialize
			$url = '';

			// Paged
			if (is_paged()) {
				$name .= ' ( ' . get_query_var('paged') . ' )';
				$url = dp_get_current_link();
			} else {
				$url = get_tag_link( get_queried_object()->term_id );
			}

			// JSON-LD
			$arr_json_ld[] =
'{
"@type":"ListItem",
"position":' . $position . ',
"name":"' . $name . '",
"item":"' . $url . '"
}';
			$position++;

		} elseif (is_post_type_archive()) {
			// custom post type archive
			$post_type_obj = get_post_type_object( get_post_type() );
			$name = strip_tags( stripslashes( $post_type_obj->labels->name ) );
			$url = dp_get_current_link();

			// Paged
			if (is_paged()) {
				$name .= ' ( ' . get_query_var('paged') . ' )';
			}

			// JSON-LD
			$arr_json_ld[] =
'{
"@type":"ListItem",
"position":' . $position . ',
"name":"' . $name . '",
"item":"' . $url . '"
}';
			$position++;

		} else {
			// Other
			$name = wp_title('', false);
			$url = dp_get_current_link();

			// Paged
			if (is_paged()) {
				$name .= ' ( ' . get_query_var('paged') . ' )';
			}

			// JSON-LD
			$arr_json_ld[] =
'{
"@type":"ListItem",
"position":' . $position . ',
"name":"' . $name . '",
"item":"' . $url . '"
}';
			$position++;
		}

	} elseif (is_singular()) {

		global $post;

		$post_type_obj = get_post_type_object( get_post_type() );
		$post_type_name = strip_tags( stripslashes($post_type_obj->labels->name) );

		if (is_single()) {
			// Single post
			if ( (get_post_type() === 'post') ) {
				// Post's category
				$categories = get_the_category( $post->ID );
				$cat = $categories[0];
				if( $cat->parent !== 0 ){
					$ancestors = array_reverse( get_ancestors( $cat->cat_ID, 'category' ) );
					foreach($ancestors as $ancestor){
						// JSON-LD
						$arr_json_ld[] =
'{
	"@type":"ListItem",
	"position":' . $position . ',
	"name":"' . stripslashes( get_cat_name($ancestor) ) . '",
	"item":"' . get_category_link( $ancestor ) . '"
}';

					$position++;
					}
				}

				// JSON-LD
				$arr_json_ld[] =
'{
	"@type":"ListItem",
	"position":' . $position . ',
	"name":"' . stripslashes( $cat->cat_name ) . '",
	"item":"' . get_category_link( $cat->term_id ) . '"
}';

				$position++;

			} else {
				// Maybe custom post type

				// JSON-LD
				$arr_json_ld[] =
'{
	"@type":"ListItem",
	"position":' . $position . ',
	"name":"' . $post_type_name . '",
	"item":"' . get_post_type_archive_link( $post->post_type ) . '"
}';

					$position++;
			}

			// Current single post
			$name =  the_title('', '', false) ? the_title('', '', false) : __('No Title', 'DigiPress');
			$name = strip_tags( stripslashes( $name ) );
			$url = get_permalink();

			// JSON-LD
			$arr_json_ld[] =
'{
"@type":"ListItem",
"position":' . $position . ',
"name":"' . $name . '",
"item":"' . $url . '"
}';
			$position++;

		} elseif (is_page()){
			// Page
			if( $post->post_parent != 0 ){
				// Get the parent page
				$ancestors = array_reverse( get_post_ancestors( $post->ID ) );
				foreach( $ancestors as $ancestor ){

					// JSON-LD
					$arr_json_ld[] =
'{
"@type":"ListItem",
"position":' . $position . ',
"name":"' . strip_tags( stripslashes( get_the_title($ancestor) ) ) . '",
"item":"' . get_permalink($ancestor) . '"
}';
			$position++;

				}
			}

			// Current page
			$name =  the_title('', '', false) ? the_title('', '', false) : __('No Title', 'DigiPress');
			$name = strip_tags( stripslashes( $name ) );
			$url = get_permalink();

			// JSON-LD
			$arr_json_ld[] =
'{
"@type":"ListItem",
"position":' . $position . ',
"name":"' . $name . '",
"item":"' . $url . '"
}';
			$position++;

		} elseif (is_attachment()){	// Atttachment page
			$name = $post->post_title;
			$url = dp_get_current_link();

			// JSON-LD
			$arr_json_ld[] =
'{
"@type":"ListItem",
"position":' . $position . ',
"name":"' . $name . '",
"item":"' . $url . '"
}';
			$position++;
		}

	} elseif (is_404()){	// 404 Not Found
			// JSON-LD
			$arr_json_ld[] =
'{
"@type":"ListItem",
"position":' . $position . ',
"name":"' . __('404 Not Found', 'DigiPress') . '",
"item":"' . dp_get_current_link() . '"
}';
		$position++;

	} else { // Anything all

		ob_start();
		wp_get_document_title();
		$name = ob_get_contents();
		ob_clean();

		if ( empty( $name ) ) {
			$name = dp_h1_title();
		}

		// JSON-LD
		$name = strip_tags( stripslashes( $name ) );
		$url = dp_get_current_link();

		// Paged
		if ( is_paged() ) {
			$name .= ' ( ' . get_query_var('paged') . ' )';
		}

		// JSON-LD
		$arr_json_ld[] =
'{
"@type":"ListItem",
"position":' . $position . ',
"name":"' . $name . '",
"item":"' . $url . '"
}';

		$position++;

	}

	if ( is_array($arr_json_ld) && !empty($arr_json_ld) ){

		$code = implode( ',', $arr_json_ld );
		$code = '<script type="application/ld+json">{"@context":"https://schema.org","@type":"BreadcrumbList","itemListElement":[' . $code . ']}</script>';

		$code = str_replace(array("\r\n","\r","\n","\t"), '', $code);

		return $code;
	}
}

/**
 * Create webite info as JSON-LD
 */
function dp_website_json_ld() {
	if (is_admin())return;

	global $options;

	$name = dp_h1_title();
	$name = stripslashes($name);

	$description = dp_h2_title('','');
	$description = stripslashes($description);
	if (!empty($description)) {
		$description = '"description":"' . $description .'",';
	}

	$image = '';
	// Logo image
	if (isset($options['json_ld_logo_img_url']) && !empty($options['json_ld_logo_img_url'])){
		$image = $options['json_ld_logo_img_url'];
	} else {
		if (!empty($options['dp_title_img'])) {
			$image = $options['dp_title_img'];
			$image = is_ssl() ? str_replace('http:', 'https:', $image) : $image;
		} else {
			if (!empty($options['meta_ogp_img_url'])){
				$image = $options['meta_ogp_img_url'];
			} else {
				$image = DP_THEME_URI.'/img/json-ld/nologo.png';
			}
		}
	}
	if (!empty($image)) {
		$image = '"image":"' . $image .'",';
	}

	$code = '<script type="application/ld+json">
{
	"@context":"http://schema.org",
	"@type":"WebSite",
	"inLanguage":"' . get_locale() . '",
	"name":"' . $name . '",
	"alternateName":"",' . $description . $image . '
	"url":"' . home_url('/') . '"
}
</script>';

	$code = str_replace(array("\r\n","\r","\n","\t"), '', $code);

	return $code;
}