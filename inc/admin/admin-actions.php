<?php
/**
 * Processes all DigiPress actions sent via POST and GET by looking for the 'dp-action'
 * request and running do_action() to call the function
 *
 * @return void
 */
function dp_process_actions() {
	if ( isset( $_POST['dp-action'] ) ) {
		do_action( 'dp_' . $_POST['dp-action'], $_POST );
	}

	if ( isset( $_GET['dp-action'] ) ) {
		do_action( 'dp_' . $_GET['dp-action'], $_GET );
	}
}
add_action( 'admin_init', 'dp_process_actions' );


/**
 * Action list
 */

/**
 * Generates a System Info download file
 */
function dp_tools_sysinfo_download() {

	if( ! current_user_can( 'administrator' ) ) {
		return;
	}

	nocache_headers();

	header( 'Content-Type: text/plain' );
	header( 'Content-Disposition: attachment; filename="dp-system-info.txt"' );

	echo wp_strip_all_tags( $_POST['dp-sysinfo'] );

	// dp_die() is defined in error-tracking.php
	dp_die();
}
add_action( 'dp_download_sysinfo', 'dp_tools_sysinfo_download' );