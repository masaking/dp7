<?php
/**
 * Register die handler for dp_die()
 *
 * @author Sunny Ratilal
 * @return void
 */
function _dp_die_handler() {
	die();
}

/**
 * Wrapper function for wp_die(). This function adds filters for wp_die() which
 * kills execution of the script using wp_die(). This allows us to then to work
 * with functions using dp_die() in the unit tests.
 *
 * @author Sunny Ratilal
 */
function dp_die( $message = '', $title = '', $status = 400 ) {
	add_filter( 'wp_die_ajax_handler', '_dp_die_handler', 10, 3 );
	add_filter( 'wp_die_handler'     , '_dp_die_handler', 10, 3 );
	add_filter( 'wp_die_json_handler', '_dp_die_handler', 10, 3 );

	wp_die( $message, $title, array( 'response' => $status ));
}