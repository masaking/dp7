<?php
/**
 * Require functions
 */
include_once(DP_THEME_DIR . "/inc/admin/sysinfo.php");


/**
 * Display the system info tab
 */
function dp_tools_page() {

	if( ! current_user_can( 'administrator' ) && ! is_admin() ) {
		return;
	}?>
<div class="wrap" id="dp-tools">
<h2 class="dp_h2 icon-hammer-wrench2"><?php _e('Tools', 'DigiPress'); ?></h2>
<p class="ft12px"><?php echo DP_THEME_NAME . ' Ver.' . DP_OPTION_SPT_VERSION; ?></p>
	<div class="dp_tool_item_div">
		<h3 class="icon-list"><?php _e('System Info', 'DigiPress'); ?></h3>
		<p><?php _e('Show your system info for debug and support.<br />Please copy this information, paste it into the form and submit it when you contact us.', 'DigiPress'); ?></p>
		<form action="<?php echo esc_url( admin_url( 'admin.php?page=digipress_tools' ) ); ?>" method="post" dir="ltr">
			<textarea readonly="readonly" onclick="this.focus(); this.select()" id="system-info-textarea" name="dp-sysinfo" style="width:720px;max-width:100%;height:500px;font-family:Menlo,Monaco,monospace;white-space:pre;"><?php echo dp_tools_sysinfo_get(); ?></textarea>
			<p class="submit">
				<input type="hidden" name="dp-action" value="download_sysinfo" /><?php
				submit_button( __('Download System Info File', 'DigiPress'), 'primary', 'dp-download-sysinfo', false ); ?>
			</p>
		</form>
	</div>
</div><?php
}

// Show
dp_tools_page();