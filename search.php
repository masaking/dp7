<?php 
// Header
get_header(); 
?>
<body <?php body_class(); ?>>
<header id="header_area_paged">
<?php 
include_once(TEMPLATEPATH . "/fixed_menu.php");
dp_banner_contents();
?>
</header>
<section class="dp_topbar_title"><?php dp_breadcrumb(); ?></section><?php
// **********************************
// Container top widget
// **********************************
if (is_active_sidebar('widget-top-container')) {
	if (isset($options_visual['full_wide_container_widget_area_top']) && !empty($options_visual['full_wide_container_widget_area_top'])) {
		ob_start(); ?>
<div id="top-container-widget" class="container-widget-area pos-top liquid clearfix"><?php
		dynamic_sidebar('widget-top-container'); ?>
</div><?php
		$widget_container_top_content = ob_get_contents();
		ob_end_clean();
	} else {
		ob_start(); ?>
<div id="top-container-widget" class="container-widget-area pos-top clearfix"><?php
		dynamic_sidebar('widget-top-container'); ?>
</div><?php
		$widget_container_top_content = ob_get_contents();
		ob_end_clean();
	}
	echo $widget_container_top_content;
}?>
<div id="container" class="dp-container clearfix">
<div class="breadcrumb_arrow aligncenter"><span>Search Result</span></div>
<?php
// Show result
if (is_search() && !isset( $_REQUEST['q'])) {
	if ($wp_query->found_posts !== 0) {
		echo '<p id="found-title"><span>' . $wp_query->found_posts . __(' posts has found.', 'DigiPress') . '</span></p>';
	}
}

/*************
 * Content start
 ************/
if ( $COLUMN_NUM == 1 ) : 
?>
<div id="content-top-1col" class="content one-col">
<?php 
elseif ( $COLUMN_NUM == 3 ) :
?>
<div id="content" class="content three-col">
<?php
else : 
?>
<div id="content" class="content">
<?php 
endif;


// Google Custom Search
if (isset( $_REQUEST['q'] ) ) : //Google Custom Search ?>
<gcse:searchresults-only></gcse:searchresults-only>
<?php 
else :
//Default Search
?> 
 <?php
 	if (have_posts()) :
		// Content widget
		if ((get_post_type() === 'post') && is_active_sidebar('widget-top-content')) : ?>
<div id="top-content-widget" class="clearfix">
<?php dynamic_sidebar( 'widget-top-content' ); ?>
</div>
<?php 
		endif;
?>
<?php
// Excerpt length
$excerpt_length = 0;

//For thumbnail size
$width = 600;
$height = 440;
$arg_thumb = array('width' => $width, 'height' => $height, "if_img_tag"=> true);

$hatebuNumberCode 	= '';
$tweetCountCode		= '';
$fbLikeCountCode	= '';

// Settings for infeed ads
$infeed_ads_flg = false;
$infeed_ads_code = '';
$infeed_ads_order = null;
if (!empty($options['archive_infeed_ads_code']) && !empty($options['archive_infeed_ads_order']) ) {
	$infeed_ads_flg = true;
	$infeed_ads_code = $options['archive_infeed_ads_code'];
	$infeed_ads_order = explode(",", $options['archive_infeed_ads_order']);
}

//If top posts show as normal view
if ($options['archive_post_show_type'] == 'normal') : 
	$excerpt_length = $options['archive_normal_excerpt_length'];
?>
<div id="entry-pager-div">
<?php
	//Loop each post
	while (have_posts()) : the_post(); ?>
<?php //All or Excerpt
		// Post format
		$postFormat = get_post_format($post->ID);

		// Get icon class each post format
		$titleIconClass = postFormatIcon($postFormat);

		// Post title
		$post_title =  the_title('', '', false) ? the_title('', '', false) : __('No Title', 'DigiPress');

		// Check the first or last
		$firstPostClass = dp_is_first() ? 'first-post': '';
		$lastPostClass = dp_is_last() ? 'last-post': '';
		// even of odd
		$i = 1;
		$evenOddClass = (++$i % 2 === 0) ? 'evenpost' : 'oddpost';


		// ************* SNS sahre number *****************
		// hatebu
		if ( isset( $options['hatebu_number_after_title_archive'] ) && !empty( $options['hatebu_number_after_title_archive'] ) ) {
			$hatebuNumberCode = '<div class="bg-hatebu icon-hatebu ct-hb"><span class="share-num"></span></div>';
		}

		// Count Facebook Like 
		if ( isset( $options['likes_number_after_title_archive'] ) && !empty( $options['likes_number_after_title_archive'] ) ) {
			$fbLikeCountCode = '<div class="bg-likes icon-facebook ct-fb"><span class="share-num"></span></div>';
		}
		// Count tweets
		if ( isset( $options['tweets_number_after_title_archive'] ) && !empty( $options['tweets_number_after_title_archive'] ) ) {
			$tweetCountCode = '<div class="bg-tweets icon-twitter ct-tw"><span class="share-num"></span></div>';
		}
		/***
		 * Filter hook
		 */
		$sns_insert_content = apply_filters( 'dp_archive_insert_sns_content', get_the_ID() );
		if ($sns_insert_content == get_the_ID() || !is_string($sns_insert_content)) {
			$sns_insert_content = '';
		}
		// Whole share code
		$sns_share_code = ( ( isset( $options['hatebu_number_after_title_archive'] ) && !empty( $options['hatebu_number_after_title_archive'] ) ) || ( isset( $options['tweets_number_after_title_archive'] ) && !empty( $options['tweets_number_after_title_archive'] ) ) || ( isset( $options['likes_number_after_title_archive'] ) && !empty( $options['likes_number_after_title_archive'] ) ) || !empty($sns_insert_content) ) ? '<div class="loop-share-num ct-shares" data-url="'.get_permalink().'">'.$hatebuNumberCode.$tweetCountCode.$fbLikeCountCode.$sns_insert_content.'</div>' : '';
		// ************* SNS sahre number *****************

		if ( isset( $options['archive_excerpt_type'] ) && $options['archive_excerpt_type'] == 'all' ) : 	
// All shows ?>
<article id="post-<?php the_ID(); ?>" class="loop-article post <?php echo $evenOddClass . ' ' . $firstPostClass . ' ' . $lastPostClass; ?>">
<?php 
			if ($postFormat === 'quote'): // Check the post format
?>
<header><h1 class="entry-title posttitle<?php echo $titleIconClass; ?>"><?php _e('Quote', 'DigiPress'); ?></h1></header>
<?php 
			elseif ($postFormat === 'status'):
?>
<div class="clearfix"><header class="inline-bl"><h1 class="mg8px-btm ft12px mg6px-top"><a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ) ); ?>" class="ft12px" title="<?php _e('Articles of this user', 'DigiPress'); ?>"><?php the_author_meta( 'display_name' ); ?></a></h1><h2 class="ft14px"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php echo get_the_date(); ?></h2></a></header>
<div class="fl-l"><?php echo get_avatar($comment,$size='50'); ?></div></div>
<?php
			else :	// else $postFormat 
?>
<header><h1 class="entry-title posttitle"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="title-link <?php echo $titleIconClass; ?>"><?php echo $post_title; ?></a></h1><?php echo $sns_share_code; ?></header>
<?php 
			endif;	// End of if ($postFormat) 
?>
<div class="entry-summary entry">
<?php the_content(__('Read more', 'DigiPress')); ?>
<footer><?php showPostMetaForArchive(); ?></footer>
</div>
</article>
<?php
		else : // $options['archive_excerpt_type'] === 'excerpt'

			/**
			 * Infdeed ads
			 */
			if ($infeed_ads_flg){
				if (is_array($infeed_ads_order)) {
					foreach ($infeed_ads_order as $ads_num) {
						if ($ads_num == dp_get_loop_number()) {
							echo '<div class="loop-article post_excerpt">'.$infeed_ads_code.'</div>';
						}
					}
				} else if ($post_num == $infeed_ads_order) {
					echo '<div class="loop-article post_excerpt">'.$infeed_ads_code.'</div>';
				}
			}
// Show excerpt view ?>
<article id="post-<?php the_ID(); ?>" class="loop-article post_excerpt <?php echo $evenOddClass . ' ' . $firstPostClass . ' ' . $lastPostClass; ?>"><div class="clearfix pd20px-btm">
<div class="post_thumb"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
<?php // Get thumbnail
echo show_post_thumbnail($arg_thumb);
?>
</a></div>
<div class="excerpt_div">
<header><h1 class="entry-title excerpt_title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="title-link <?php echo $titleIconClass; ?>">
<?php
			if ($postFormat === 'quote'): // Check the post format 
				_e('Quote', 'DigiPress');
			else :
				echo $post_title;
			endif;
?>
</a></h1><?php echo $sns_share_code; ?></header>
<div class="entry_excerpt">
<?php
		if ( $excerpt_length != 0 ) {
			//Post excerpt
			$desc = strip_tags(get_the_excerpt());
			if (mb_strlen($desc,'utf-8') > $excerpt_length) $desc = mb_substr($desc, 0, $excerpt_length,'utf-8').'...';
			echo '<p class="entry-summary">'.$desc.'</p>';
		}
?>
</div></div></div>
<footer><?php showPostMetaForArchive(); ?></footer>
</article>
	<?php 
	endif; // End of if $options['archive_excerpt_type'] 
	endwhile;
?>
</div><?php // End of "entry-pager-div" class ?>
<?php 
	//If top posts show as table view
	elseif ($options['archive_post_show_type'] == 'table') : ?>
<section id="post-table-section" class="clearfix">
	<?php
	if ($COLUMN_NUM == 1) {
		echo '<ul class="top-posts-ul one-col">';
	} else if ($COLUMN_NUM == 2) {
		echo '<ul class="top-posts-ul two-col">';
	} else {
		echo '<ul class="top-posts-ul three-col">';
	}

	//For thumbnail size
	$width = 600;
	$height = 440;

	$titleStrCount = 184;

	//Loop each post
	while (have_posts()) : the_post();
		// Post format
		$postFormat = get_post_format($post->ID);

		// Get icon class each post format
		$titleIconClass = postFormatIcon($postFormat);

		// Post title
		$post_title =  the_title('', '', false) ? the_title('', '', false) : __('No Title', 'DigiPress');
		
		
		// ************* SNS sahre number *****************
		// hatebu
		if ( isset( $options['hatebu_number_after_title_archive'] ) && !empty( $options['hatebu_number_after_title_archive'] ) ) {
			$hatebuNumberCode = '<div class="bg-hatebu icon-hatebu ct-hb"><span class="share-num"></span></div>';
		}

		// Count Facebook Like 
		if ( isset( $options['likes_number_after_title_archive'] ) && !empty( $options['likes_number_after_title_archive'] ) ) {
			$fbLikeCountCode = '<div class="bg-likes icon-facebook ct-fb"><span class="share-num"></span></div>';
		}
		// Count tweets
		if ( isset( $options['tweets_number_after_title_archive'] ) && !empty( $options['tweets_number_after_title_archive'] ) ) {
			$tweetCountCode = '<div class="bg-tweets icon-twitter ct-tw"><span class="share-num"></span></div>';
		}
		/***
		 * Filter hook
		 */
		$sns_insert_content = apply_filters( 'dp_archive_insert_sns_content', get_the_ID() );
		if ($sns_insert_content == get_the_ID() || !is_string($sns_insert_content)) {
			$sns_insert_content = '';
		}
		// Whole share code
		$sns_share_code = ( ( isset( $options['hatebu_number_after_title_archive'] ) && !empty( $options['hatebu_number_after_title_archive'] ) ) || ( isset( $options['tweets_number_after_title_archive'] ) && !empty( $options['tweets_number_after_title_archive'] ) ) || ( isset( $options['likes_number_after_title_archive'] ) && !empty( $options['likes_number_after_title_archive'] ) ) || !empty($sns_insert_content) ) ? '<div class="loop-share-num ct-shares" data-url="'.get_permalink().'">'.$hatebuNumberCode.$tweetCountCode.$fbLikeCountCode.$sns_insert_content.'</div>' : '';
		// ************* SNS sahre number *****************
		

		if (mb_strlen($post_title, 'UTF-8') > $titleStrCount) $post_title = mb_substr($post_title, 0, $titleStrCount, 'UTF-8') . '...';

		/**
		 * Infdeed ads
		 */
		if ($infeed_ads_flg){
			if (is_array($infeed_ads_order)) {
				foreach ($infeed_ads_order as $ads_num) {
					if ($ads_num == dp_get_loop_number()) {
						echo '<li><div class="loop-article post">'.$infeed_ads_code.'</div></li>';
					}
				}
			} else if ($post_num == $infeed_ads_order) {
				echo '<li><div class="loop-article post">'.$infeed_ads_code.'</div></li>';
			}
		}
		?>
<li class="clearfix">
<article class="loop-article post <?php echo (++$i % 2 == 0) ? 'evenpost' : 'oddpost'; ?>">
<div class="post_thumb_portfolio"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php echo show_post_thumbnail($arg_thumb); ?></a></div>
<div class="post_tbl_div">
<div class="post_info_portfolio clearfix">
<a href="<?php the_permalink() ?>" rel="bookmark">
<header><h1 class="entry-title top-tbl-title<?php echo $titleIconClass; ?>"><?php
			if ($postFormat === 'quote'): // Check the post format 
				_e('Quote', 'DigiPress');
			else :
				echo $post_title;
			endif;
?></h1></header>
</a>
</div>
</div>
<footer class="tbl_meta">
<?php 
		//Posted date
		if ( isset( $options['show_pubdate_on_meta'] ) && !empty( $options['show_pubdate_on_meta'] ) ) : 
?>
<span class="icon-calendar">
<time datetime="<?php the_time('c'); ?>" class="date updated"><?php echo get_the_date(); ?></time>
</span>
<?php 
		endif; 

		// Comments
		if ( comments_open() ) : // If comment is open 
?>
<span class="icon-comment"><?php comments_popup_link(
							__('No Comment', 'DigiPress'), 
							__('Comment(1)', 'DigiPress'), 
							__('Comments(%)', 'DigiPress')); ?></span>
<?php 
		endif;

		// Views
		if ( ( isset( $options['show_views_on_meta'] ) && !empty( $options['show_views_on_meta'] ) ) && function_exists('dp_get_post_views') ) : 
?>
<span class="icon-eye"><?php echo dp_get_post_views(get_the_ID(), null); ?></span>
<?php 
		endif; 

		// Shares
		echo $sns_share_code;

		// Author
		if ( isset( $options['show_author_on_meta'] ) && !empty( $options['show_author_on_meta'] ) ) : 
			echo '<span class="icon-user vcard author"><a href="'.get_author_posts_url(get_the_author_meta('ID')).'" rel="author" title="'.__('Show articles of this user.', 'DigiPress').'" class="fn">'.get_the_author_meta('display_name').'</a></span>';
		 endif;

		// Edit Post Link(If logged in.)
		edit_post_link(__('Edit', 'DigiPress'), ' | ');
?>
</footer>
</article>
</li>
<?php endwhile; ?>
</ul>
</section>
<?php // If posts show as gallery style
	elseif ( isset( $options['archive_post_show_type'] ) && $options['archive_post_show_type'] == 'gallery') : 

		$excerpt_length = isset( $options['archive_magazine_excerpt_length'] ) ? $options['archive_magazine_excerpt_length'] : 80;
		$width = 600;
		$height = 440;
		$desc = '';
?>
<section id="post-table-section" class="clearfix">
	<?php
	if ( $COLUMN_NUM == 1 ) {
		echo '<div id="gallery-style-1col">';
	} else {
		echo '<div id="gallery-style">';
	}

	//Loop each post
	while (have_posts()) : the_post();

		// Post format
		$postFormat = get_post_format($post->ID);

		// Get icon class each post format
		$titleIconClass = postFormatIcon($postFormat);

		// Check the last
		$lastPostClass = dp_is_last() ? 'last-post': '';
		// even of odd
		$evenOddClass = (++$i % 2 === 0) ? 'evenpost' : 'oddpost';

		// Post title
		$post_title =  the_title('', '', false) ? the_title('', '', false) : __('No Title', 'DigiPress');

		//$itemHeight = rand(1, 5);
		
		// ************* SNS sahre number *****************
		// hatebu
		if ( isset( $options['hatebu_number_after_title_archive'] ) && !empty( $options['hatebu_number_after_title_archive'] ) ) {
			$hatebuNumberCode = '<div class="bg-hatebu icon-hatebu ct-hb"><span class="share-num"></span></div>';
		}

		// Count Facebook Like 
		if ( isset( $options['likes_number_after_title_archive'] ) && !empty( $options['likes_number_after_title_archive'] ) ) {
			$fbLikeCountCode = '<div class="bg-likes icon-facebook ct-fb"><span class="share-num"></span></div>';
		}
		// Count tweets
		if ( isset( $options['tweets_number_after_title_archive'] ) && !empty( $options['tweets_number_after_title_archive'] ) ) {
			$tweetCountCode = '<div class="bg-tweets icon-twitter ct-tw"><span class="share-num"></span></div>';
		}
		/***
		 * Filter hook
		 */
		$sns_insert_content = apply_filters( 'dp_archive_insert_sns_content', get_the_ID() );
		if ($sns_insert_content == get_the_ID() || !is_string($sns_insert_content)) {
			$sns_insert_content = '';
		}
		// Whole share code
		$sns_share_code = ( ( isset( $options['hatebu_number_after_title_archive'] ) && !empty( $options['hatebu_number_after_title_archive'] ) ) || ( isset( $options['tweets_number_after_title_archive'] ) && !empty( $options['tweets_number_after_title_archive'] ) ) || ( isset( $options['likes_number_after_title_archive'] ) && !empty( $options['likes_number_after_title_archive'] ) ) || !empty($sns_insert_content) ) ? '<div class="loop-share-num ct-shares" data-url="'.get_permalink().'">'.$hatebuNumberCode.$tweetCountCode.$fbLikeCountCode.$sns_insert_content.'</div>' : '';
		// ************* SNS sahre number *****************
		
		if ( $excerpt_length != 0 ) {
			$desc = strip_tags(get_the_excerpt());
			if (mb_strlen($desc,'utf-8') > $excerpt_length) $desc = mb_substr($desc, 0, $excerpt_length,'utf-8').'…';
		}?>
<article class="loop-article g_item post clearfix <?php echo $evenOddClass . ' ' . $lastPostClass; ?>">
<div class="post_thumb_gallery"><a href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php echo show_post_thumbnail($arg_thumb); ?></a></div>
<header><h1 class="entry-title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>" class="title-link <?php echo $titleIconClass; ?>"><?php
			if ($postFormat === 'quote'): // Check the post format 
				_e('Quote', 'DigiPress');
			else :
				echo $post_title;
			endif;
?></a></h1></header> 
<?php 
			if ( $excerpt_length != 0 ) :
?>
<div class="entry-summary g_item_desc"><?php echo $desc; ?></div>
<?php
			endif;
?>
<footer class="tbl_meta">
<?php //Posted date
		if ( isset( $options['show_pubdate_on_meta'] ) && !empty( $options['show_pubdate_on_meta'] ) ) : 
?>
<span class="icon-calendar">
<time datetime="<?php the_time('c'); ?>" class="updated"><?php echo get_the_date(); ?></time>
</span>
<?php 
		endif; 
		
		// Comments
		if ( comments_open() ) : // If comment is open 
?>
<span class="icon-comment"><?php comments_popup_link(
							__('No Comment', 'DigiPress'), 
							__('Comment(1)', 'DigiPress'), 
							__('Comments(%)', 'DigiPress')); ?></span>
<?php 
		endif;

		// Views
		if ( ( isset( $options['show_views_on_meta'] ) && !empty( $options['show_views_on_meta'] ) ) && function_exists('dp_get_post_views') ) : ?>
<span class="icon-eye"><?php echo dp_get_post_views(get_the_ID(), null); ?></span>
<?php 
		endif;

		// Shares
		echo $sns_share_code;

		// Author
		if ( isset( $options['show_author_on_meta'] ) && !empty( $options['show_author_on_meta'] ) ) : 
			echo '<span class="icon-user vcard author"><a href="'.get_author_posts_url(get_the_author_meta('ID')).'" rel="author" title="'.__('Show articles of this user.', 'DigiPress').'" class="fn">'.get_the_author_meta('display_name').'</a></span>';
		endif;

		 // Edit Post Link(If logged in.)
		edit_post_link(__('Edit', 'DigiPress'), ' | ');
?>
</footer>
</article>
	<?php endwhile; ?>
</div>
</section>
	<?php endif; // End of $options['archive_post_show_type'] ?>

<?php // Content bottom widget
if (is_active_sidebar('widget-top-content-bottom')) : ?>
<div id="top-content-bottom-widget" class="clearfix">
<?php dynamic_sidebar( 'widget-top-content-bottom' ); ?>
</div>
<?php endif; // end of if (is_active_sidebar) ?>
<?php // Page navigation
	if ( ( isset( $options['autopager'] ) && !empty( $options['autopager'] ) ) || !is_paged() && isset( $options['navigation_text_to_2page_archive'] ) ) : 
		$next_page_link = is_ssl() ? str_replace('http:', 'https:', get_next_posts_link($options['navigation_text_to_2page_archive'])) : get_next_posts_link($options['navigation_text_to_2page_archive']);
?>
<nav class="navigation clearfix"><div class="nav_to_paged"><?php echo $next_page_link; ?></div></nav>
<?php else: // Paged ?>
	<?php
		if (function_exists('wp_pagenavi')) : ?>
<nav class="navigation clearfix"><?php wp_pagenavi(); ?></nav>
	<?php
		else :
	?>
<nav class="navigation clearfix">
		<?php
			if ( isset( $options['pagenation'] ) && !empty( $options['pagenation'] ) ) :
				dp_pagenavi();
			else : ?>
<div class="navialignleft"><?php previous_posts_link(__('<span> PREV</span>', '')) ?></div>
<div class="navialignright"><?php next_posts_link(__('<span>NEXT</span>', '')) ?></div>
		<?php
			endif; // $options['pagenation'] ?>
</nav>
	<?php
		endif; // function_exists('wp_pagenavi') ?>
<?php
	endif; // $options['autopager'] || !is_paged() ?>
<?php else :  // if have_posts() ?>
<article class="post">
<header><h1 class="posttitle"><?php _e('Not found.','DigiPress'); ?></h1></header>
<div class="entry">
<p class="ooops icon-attention"><span>Oppps!</span></p>
<p><?php _e('Apologies, but the page you requested could not be found. <br />Perhaps searching will help.', 'DigiPress'); ?></p>
</div>
</article>
<?php endif; // End of if have_posts() ?>
<?php endif; // End of if (isset($_REQUEST['q'])) 
// End of content division ?>
</div>
<?php
// Sidebar
if ($COLUMN_NUM == 2) {
	get_sidebar();
} else if ($COLUMN_NUM == 3) {
	get_sidebar();
	get_sidebar('2');
}
?>
</div>
<?php 
get_footer();

if ( $EXIST_FB_LIKE_BOX ) {
		$fb_app_id = isset($options['fb_app_id']) ? $options['fb_app_id'] : '';
		if (empty($fb_app_id)) {
			$fb_app_id = $FB_APP_ID;
		}
		echo '<div id="fb-root"></div><script async defer crossorigin="anonymous" src="https://connect.facebook.net/' . $options['fb_api_lang'] . '/sdk.js#xfbml=1&version=v12.0&appId=' . $fb_app_id . '&autoLogAppEvents=1"></script>';
	}
?>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</body>
</html>