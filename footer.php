<?php 
global $options,$options_visual;
// **********************************
// Container footer widget
// **********************************
 if (is_active_sidebar('widget-container-footer') && !is_404()) { ?>
<div id="container_footer" class="container_footer"><?php
	if (isset($options_visual['full_wide_container_widget_area_bottom']) && !empty($options_visual['full_wide_container_widget_area_bottom'])) {
		ob_start(); ?>
<div id="content_footer" class="content_footer liquid"><?php
		dynamic_sidebar('widget-container-footer'); ?>
</div><?php
		$widget_container_bottom_content = ob_get_contents();
		ob_end_clean();
	} else {
		ob_start(); ?>
<div id="content_footer" class="content_footer"><?php
		dynamic_sidebar('widget-container-footer'); ?>
</div><?php
		$widget_container_bottom_content = ob_get_contents();
		ob_end_clean();
	}
	echo $widget_container_bottom_content; ?>
</div><?php
}?>
<footer id="footer"><?php 
// show footer widgets
dp_get_footer();?>
<div id="footer-bottom"><div id="ft-btm-content">&copy; <?php 
$current_year = (string)date('Y');
if (isset($options['blog_start_year']) && !empty($options['blog_start_year'])) {
	if ( $options['blog_start_year'] !== $current_year ){
		echo $options['blog_start_year'] . ' - ' . $current_year;
	} else {
		echo $current_year;
	}
} else {
	echo $current_year;
} ?> <a href="<?php echo home_url(); ?>/"><small><?php bloginfo('name'); ?></small></a>
</div></div></footer>
<?php 
if (is_home() && !is_paged()) :
?>
<a href="#site_title" id="gototop2" class="icon-up-open" title="Return Top"><span>Return Top</span></a>
<?php 
else :
?>
<a href="#header_area_paged" id="gototop2" class="icon-up-open" title="Return Top"><span>Return Top</span></a>
<?php
endif;
// ******************************
// WordPress footer
// ******************************
wp_footer();
// ******************************
// Slideshow JS
// ******************************
if ($options_visual['dp_header_content_type'] === "2" && is_home() && !is_paged()) {
	getSlideshowSource();
}
// ******************************
// Autopager JS
// ******************************
showScriptForAutopager($wp_query->max_num_pages);
// ******************************
// Headline JS
// ******************************
if ($options['headline_type'] === '3' && (is_home() && !is_paged()) ) {
	if ($options['headline_slider_fx'] === '1') {
		$headlineTime = $options['headline_slider_time'];
		$headlineHoverStop = $options['headline_hover_stop'] ? 'true' : 'false';
		$headlineArrow = $options['headline_arrow'] ? 'true' : 'false';

		$headline_js = <<<_EOD_
<script>j$(function(){j$('.headline_slider').glide({autoplay:$headlineTime,hoverpause:$headlineHoverStop,arrows:$headlineArrow,arrowRightClass:'arrow_r icon-right-open',arrowLeftClass:'arrow_l icon-left-open',arrowRightText:'',arrowLeftText:'',nav:false,afterInit:(function(){j$('ul.slides').fadeIn();})});});</script>
_EOD_;

	} else {
		$tickerVelocity = $options['headline_ticker_velocity'] ? $options['headline_ticker_velocity'] : '0.07';
		$tickerHoverStop = $options['headline_ticker_hover_stop'] ? 1 : 0;
		$headline_js = <<<_EOD_
<script>j$(function(){j$(function(){j$("ul#headline_ticker").liScroll({travelocity:$tickerVelocity,hoverstop:$tickerHoverStop});});j$('ul#headline_ticker').fadeIn();});</script>
_EOD_;
	}
	echo $headline_js;
}
// ******************************
// Google Custom Search
// ******************************
if ( isset( $options['gcs_id'] ) && !empty( $options['gcs_id'] ) ) :  ?>
<script>(function(){var cx='<?php echo $options['gcs_id']; ?>';var gcse=document.createElement('script');gcse.type= 'text/javascript';gcse.async=true;gcse.src=(document.location.protocol=='https:'?'https:':'http:')+'//cse.google.com/cse.js?cx='+cx;var s=document.getElementsByTagName('script')[0];s.parentNode.insertBefore(gcse,s);})();</script>
<?php 
endif;
// **********************************
// JSON-LD for Structured Data
// **********************************
dp_json_ld();